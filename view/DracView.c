////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// DracView.c: the DracView ADT implementation
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "DracView.h"
#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "Custom.h"
#include "Player.h"
#include "Map.h"

struct dracView {
    GameView gv;
};


/*
 * newDracView
 * Creates a new DracView to summarise the current state of the game
 */
DracView newDracView (char *pastPlays, PlayerMessage messages[]) {
    DracView new = malloc (sizeof *new);
    if (new == NULL) {
        fprintf(stderr, "%s: Couldn't allocate Dracview\n", __func__);
        abort();
    }
    new->gv = newGameView(pastPlays, messages);
    return new;
}

/*
 * disposeDracView
 * Frees all memory previously allocated for the DracView toBeDeleted
 */
void disposeDracView (DracView toBeDeleted) {
    if(toBeDeleted == NULL) {
        return;
    }
    disposeGameView(toBeDeleted->gv);
    free (toBeDeleted);
}

/*
 * giveMeTheRound
 * Get the current round
 */
Round giveMeTheRound (DracView dv) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return getRound(dv->gv);
}

/*
 * giveMeTheScore
 * Get the current score
 */
int giveMeTheScore (DracView dv) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return getScore(dv->gv);
}

/*
 * howHealthyIs
 * Get the current health points for a given player
 */
int howHealthyIs (DracView dv, PlayerID player) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    return getHealth(dv->gv, player);
}

/*
 * whereIs
 * Get the current location of a given player
 */
LocationID whereIs (DracView dv, PlayerID player) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    return getPastLocation(dv->gv, player, 0);
}

/*
 * lastMove
 * Get the most recent move of a given player
 */
void lastMove (DracView dv, PlayerID player, LocationID *start, LocationID *end) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    LocationID *trail = getTrail(dv->gv, player);
    *start = trail[1];
    *end = trail[0];
}

/*
 * whatsThere
 * Find out what minions are placed at a specified location
 */
void whatsThere (DracView dv, LocationID where, int *numTraps, int *numVamps) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!validPlace(where)){
        fprintf(stderr, "%s: Invalid Location Id\n", __func__);
        abort();
    }
    *numTraps = getTrapsFromGV(dv->gv, where);
    *numVamps = getVampsFromGV(dv->gv, where);
}

/*
 * giveMeTheTrail
 * Fills the trail array with the location ids of the last 6 turns
 */
void giveMeTheTrail (DracView dv, PlayerID player,
    LocationID trail[TRAIL_SIZE])
{
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    LocationID *t = getTrail(dv->gv, player);
    for(int i = 0; i<TRAIL_SIZE; i++){
        int moveDist = moveBackDistance(t[i]);
        //if its an invalid location, try and resolve it (Hides, Backtracks, TP)
        if(!validPlace(t[i])){
            trail[i] = getPastLocation(dv->gv,player, moveDist+i);
        }else{
            trail[i] = t[i];
        }
    }
}

/*
 * whereCanIgo
 * What are my (Dracula's) possible next moves (locations)
 */
LocationID *whereCanIgo (DracView dv, int *numLocations, bool road, bool sea) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return whereCanTheyGo(dv, numLocations, PLAYER_DRACULA, road, false, sea);
}

/*
 * whereCanTheyGo
 * What are the specified player's next possible moves
 */
LocationID * whereCanTheyGo (DracView dv, int *numLocations, PlayerID player,
    bool road, bool rail, bool sea)
{
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    //if players turn is before ours, then their turn is the next round
    int currRound = getRound(dv->gv);
    if(player< getCurrentPlayer(dv->gv)){
        currRound++;
    }

    //where can we get too from our current location?
    LocationID currLoc = getPastLocation(dv->gv, player, 0);
    LocationID *where = connectedLocations (dv->gv, numLocations,
        currLoc,player, currRound,
        road, rail, sea);

    return whereCanDraculaGoUpdate(dv->gv, numLocations, where, player, road, sea);
}
