////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// testDracView.c: test the DracView ADT
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DracView.h"

#define COLOR_RED     "\x1b[91m"
#define COLOR_GREEN   "\x1b[92m"
#define COLOR_CYAN    "\x1b[96m"
#define COLOR_RESET   "\x1b[0m"
#define COLOR_BLUE    "\x1b[94m"
#define LINE "***************************************************************************\n"
#define PRINT_SUCC printf(COLOR_GREEN"COMPLETED!"COLOR_RESET"\n");
#define PRINT_FAIL printf(COLOR_RED"INCOMPLETE!"COLOR_RESET"\n");


static void blackBoxTests(void);
static int bbTest1(void);
static int bbTest2(void);
static int bbTest3(void);
static int bbTest4(void);
static int bbTest5(void);
static int bbTest6(void);
static int bbTest7(void);
static int bbTest8(void);
static int bbTest9(void);
static int bbTest10(void);
static int bbTest11(void);
static int bbTest12(void);
static int bbTest13(void);
static int bbTest14(void);
static int bbTest15(void);
static int bbTest16(void);
static int bbTest17(void);
static int bbTest18(void);
static int bbTest19(void);
static int bbTest20(void);
static int bbTest21(void);
static int bbTest22(void);
static int bbTest23(void);
static int bbTest24(void);
static int bbTest25(void);
static int bbTest26(void);
static int bbTest27(void);
static int bbTest28(void);
static int bbTest29(void);
static int bbTest30(void);
static int bbTest31(void);

int main (int argc, char *ardv[]) {
    setbuf(stdout,NULL);
    /***********************
    *   FUNCTION TESTING  *
    ************************/
    printf(LINE);
    printf(LINE);
    printf(COLOR_CYAN"Running DracView Tests!"COLOR_RESET"\n");
    printf(LINE);
    printf(LINE);

    /***********************
    *   BLACKBOX TESTING  *
    ************************/
    printf(COLOR_CYAN"Black Box tests:"COLOR_RESET"\n");
    printf(LINE);


    blackBoxTests();
}

static void bb_tests(int (*testFunction) (void),char *msg)
{
    printf("%s",msg);
    if(testFunction()){
        PRINT_SUCC
    }else{
        PRINT_FAIL
    }
}

static void blackBoxTests(void) {
    bb_tests(bbTest1,"Test 1: Basic initialisation\t\t\t\t\t");
    bb_tests(bbTest2,"Test 2: giveMeTheRound\t\t\t\t\t\t");
    bb_tests(bbTest3,"Test 3: Extended Play(no health changes)\t\t\t");
    bb_tests(bbTest4,"Test 4: After one move\t\t\t\t\t\t");
    bb_tests(bbTest5,"Test 5: Dracula encounter\t\t\t\t\t");
    bb_tests(bbTest6,"Test 6: Trap encounter\t\t\t\t\t\t");
    bb_tests(bbTest7,"Test 7: Vampire encounter\t\t\t\t\t");
    bb_tests(bbTest8,"Test 8: Hunter loses all health (and gets it back)\t\t");
    bb_tests(bbTest9,"Test 9: Dracula loses health at sea\t\t\t\t");
    bb_tests(bbTest10,"Test 10: Dracula regains health\t\t\t\t\t");
    bb_tests(bbTest11,"Test 11: Hunter gains health\t\t\t\t\t");
    bb_tests(bbTest12,"Test 12: Vampire Matures\t\t\t\t\t");
    bb_tests(bbTest13,"Test 13: Basic lastMove\t\t\t\t\t\t");
    bb_tests(bbTest14,"Test 14: Calling lastMove before a move has been made\t\t");
    bb_tests(bbTest15,"Test 15: Calling lastMove before/after teleportation\t\t");
    bb_tests(bbTest16,"Test 16: Testing lastMove on a resting, hide, and double backs\t");
    bb_tests(bbTest17,"Test 17: Basic whatsThere\t\t\t\t\t");
    bb_tests(bbTest18,"Test 18: Traps are triggered or malfunction\t\t\t");
    bb_tests(bbTest19,"Test 19: Vampires mature or are encountered\t\t\t");
    bb_tests(bbTest20,"Test 20: Give me the trail\t\t\t\t\t");
    bb_tests(bbTest21,"Test 21: Give me the trail after tp, hide, or double back\t");
    bb_tests(bbTest22,"Test 22: whereCanIgo\t\t\t\t\t\t");
    bb_tests(bbTest23,"Test 23: whereCanIgo: with Restrictions\t\t\t\t");
    bb_tests(bbTest24,"Test 24: whereCanIgo: teleportation\t\t\t\t");
    bb_tests(bbTest25,"Test 25: whereCanIgo: some falses\t\t\t\t");
    bb_tests(bbTest26,"Test 26: whereCanTheyGo: Dracula\t\t\t\t");
    bb_tests(bbTest27,"Test 27: whereCanTheyGo: hunters and empty string\t\t");
    bb_tests(bbTest28,"Test 28: whereCanTheyGo: hunters by road\t\t\t");
    bb_tests(bbTest29,"Test 29: whereCanTheyGo: hunters by sea\t\t\t\t");
    bb_tests(bbTest30,"Test 30: whereCanTheyGo: hunters by rail\t\t\t");
    bb_tests(bbTest31,"Test 31: whereCanTheyGo: hunters by all\t\t\t\t");

    printf(LINE);
    printf(LINE);
    printf(COLOR_CYAN"All tests completed!"COLOR_RESET"\n");
    printf(LINE);
    printf(LINE);
}


/*
* Basic Function Tests
*/
static int bbTest1(void) {
    char *trail = "GLO.... SBA.... HGE.... MIR.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    assert(giveMeTheScore(dv) == GAME_START_SCORE);
    assert(giveMeTheRound(dv) == 0);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == LONDON);
    assert(whereIs(dv, PLAYER_DR_SEWARD) == BARCELONA);
    assert(whereIs(dv, PLAYER_VAN_HELSING) == GENEVA);
    assert(whereIs(dv, PLAYER_MINA_HARKER) == IRISH_SEA);
    assert(whereIs(dv, PLAYER_DRACULA) == UNKNOWN_LOCATION);
    disposeDracView(dv);

    return 1;
}

/*
* Test GiveMeTheRound Function
*/
static int bbTest2(void) {
    char *trail = "";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    assert(giveMeTheRound(dv) == 0);
    disposeDracView(dv);

    char *trail2 = "GLO.... SBA.... HGE.... MIR.... ";
    dv = newDracView(trail2, messages);
    assert(giveMeTheRound(dv) == 0);
    disposeDracView(dv);

    char *trail3 = "GLO.... SBA.... HGE.... MIR.... DZU.V.. ";
    dv = newDracView(trail3, messages);
    assert(giveMeTheRound(dv) == 1);
    disposeDracView(dv);

    char *trail4 = "GHA.... SLO.... HLS.... MCF.... DMU.V.. GBR.... SSW.... "
                    "HMA.... MTO.... DVIT... GLI.... SLV.... HCA.... MMR.... "
                    "DBDT... GHA.... SMN.... HLS.... MCF.... DSZT... ";
    dv = newDracView(trail4, messages);
    assert(giveMeTheRound(dv) == 4);
    disposeDracView(dv);

    return 1;
}

/*
* Extended Game Play (no health changes)
*/
static int bbTest3(void) {
    char *trail = "GHA.... SLO.... HLS.... MCF.... DGO.V.. GBR.... SSW.... \
                HMA.... MTO.... DVET... GLI.... SLV.... HCA.... MMR.... \
                DVIT... GHA.... SMN.... HLS.... MCF.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    assert (giveMeTheRound (dv) == 3);
    assert (whereIs (dv, PLAYER_LORD_GODALMING) == HAMBURG);
    assert (whereIs (dv, PLAYER_DR_SEWARD) == MANCHESTER);
    assert (whereIs (dv, PLAYER_VAN_HELSING) == LISBON);
    assert (whereIs (dv, PLAYER_MINA_HARKER) == CLERMONT_FERRAND);
    assert (whereIs (dv, PLAYER_DRACULA) == VIENNA);
    assert (howHealthyIs (dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (howHealthyIs (dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (dv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (dv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    disposeDracView(dv);

    return 1;
}

/*
* Test Game State after one move
*/
static int bbTest4(void) {
    char *trail = "GLO.... SZU.... HMR.... MSO.... ";
    PlayerMessage messages[] = {"random"};
    DracView dv = newDracView(trail, messages);

    assert(giveMeTheRound(dv) == 0);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == LONDON);
    assert(whereIs(dv, PLAYER_DR_SEWARD) == ZURICH);
    assert(whereIs(dv, PLAYER_VAN_HELSING) == MARSEILLES);
    assert(whereIs(dv, PLAYER_MINA_HARKER) == SOFIA);
    assert(whereIs(dv, PLAYER_DRACULA) == UNKNOWN_LOCATION);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    disposeDracView(dv);

    return 1;
}

/*
* Test Game State after Dracula Encounter
*/
static int bbTest5(void) {
    char *trail = "GST.... SAO.... HCD.... MAO.... DGE.V.. "
                    "GGEVD.. SIR.... HKL.... MGA.... ";
    PlayerMessage messages[] = {
        "Hello", "Rubbish",  "Stuff", "", "Mwahahah", "Aha!"};
    DracView dv = newDracView (trail, messages);

    assert (whereIs (dv, PLAYER_DRACULA) == GENEVA);
    assert (howHealthyIs (dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_DRACULA_ENCOUNTER);
    assert (howHealthyIs (dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert (whereIs (dv, PLAYER_LORD_GODALMING) == GENEVA);
    disposeDracView (dv);

    //check dracula encounters are only read from string
    //not hardcoded in (even if string is invalid play);
    char *trail2 = "GST.... SAO.... HCD.... MAO.... DGE.... "
                    "GGE....";
    dv = newDracView (trail2, messages);

    assert (whereIs (dv, PLAYER_DRACULA) == GENEVA);
    assert (howHealthyIs (dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (whereIs (dv, PLAYER_LORD_GODALMING) == GENEVA);
    disposeDracView (dv);

    return 1;
}

/*
* Test Game State after Trap Encounter
*/
static int bbTest6(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DZUT.... GLVT.. ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    DracView dv = newDracView(trail, messages);

    assert(whereIs(dv, PLAYER_DRACULA) == ZURICH);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) ==
        GAME_START_HUNTER_LIFE_POINTS - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeDracView(dv);

    //check trap encounters are only read from string
    //not hardcoded in (even if string is invalid play);
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DLVT... GLV.... ";
    dv = newDracView (trail2, messages);
    assert(whereIs(dv, PLAYER_DRACULA) == LIVERPOOL);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeDracView (dv);

    return 1;
}

/*
* Test Game State after Vampire Enounter
*/
static int bbTest7(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DC?.V.. GLV.V.. ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    DracView dv = newDracView(trail, messages);

    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    disposeDracView(dv);

    return 1;
}

/*
* Test Hunter loses all health (and gets it back)
*/
static int bbTest8(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SCF.... HGA.... MCO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    //checking a hunter successfully dies
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == 0);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert(howHealthyIs(dv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(whereIs(dv, PLAYER_DRACULA) == MANCHESTER);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == ST_JOSEPH_AND_ST_MARYS);
    assert(whereIs(dv, PLAYER_DR_SEWARD) == CLERMONT_FERRAND);
    assert(whereIs(dv, PLAYER_VAN_HELSING) == GALATZ);
    assert(whereIs(dv, PLAYER_MINA_HARKER) == COLOGNE);
    disposeDracView(dv);

    //lets now resurrect out Hunter
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SMR.... HCD.... MAM.... "
            "DSW.... GSZ.... SCF.... HGA.... MAM.... ";
    dv = newDracView(trail2, messages);

    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == SZEGED);
    disposeDracView(dv);

    return 1;
}

/*
* Test Dracula loses health at sea
*/
static int bbTest9(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DAS.... "
                    "GLO.... SMR.... HCD.... MAM.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    //Dracula loses 2 blood points if ending his turn at sea
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS -
        LIFE_LOSS_SEA);
    assert(whereIs(dv, PLAYER_DRACULA) == ADRIATIC_SEA);
    disposeDracView(dv);

    return 1;
}

/*
* Test Dracula regains health
*/
static int bbTest10(void) {
    //before Dracula regains his health
    char *trail = "GMR.... SAO.... HCD.... MAO.... DGO.... GGOD... "
                    "SAO.... HCD.... MAO.... DTS.... GMR.... SAO.... "
                    "HCD.... MAO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    assert(whereIs(dv, PLAYER_DRACULA) == TYRRHENIAN_SEA);
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA);
    disposeDracView(dv);

    //Then gains health at Castle Dracula
    char *trail2 =  "GMR.... SAO.... HZU.... MAO.... DGO.... GGOD... "
                    "SAO.... HZU.... MAO.... DTS.... GMR.... SAO.... "
                    "HZU.... MAO.... DCD.... GMR.... SAO.... HZU.... "
                    "MAO.... DCD.... GMR.... SAO.... HZU.... MAO.... ";
    dv = newDracView(trail2, messages);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA + LIFE_GAIN_CASTLE_DRACULA
        + LIFE_GAIN_CASTLE_DRACULA);
    assert(howHealthyIs(dv, PLAYER_DRACULA) >= GAME_START_BLOOD_POINTS);
    assert(whereIs(dv, PLAYER_DRACULA) == CASTLE_DRACULA);
    disposeDracView(dv);

    //sim, but checking dracula encounters are only determined from
    // pastPlays string, regardless of whether its a valid history or not
    char *trail3 =  "GMR.... SAO.... HCD.... MAO.... DGO.... GGOD... "
    "SAO.... HCD.... MAO.... DTS.... GMR.... SAO.... "
    "HCD.... MAO.... DCD.... GMR.... SAO.... "
    "HCD.... MAO.... DCD.... ";
    dv = newDracView(trail3, messages);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));
    assert(howHealthyIs(dv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA + LIFE_GAIN_CASTLE_DRACULA
        + LIFE_GAIN_CASTLE_DRACULA);
    assert(howHealthyIs(dv, PLAYER_DRACULA) >= GAME_START_BLOOD_POINTS);
    assert(whereIs(dv, PLAYER_DRACULA) == CASTLE_DRACULA);
    disposeDracView(dv);

    return 1;
}

/*
* Test Hunter gains health
*/
static int bbTest11(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... "
                    "GSWT.... SMR.... HCD.... MAM.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeDracView(dv);

    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... "
                    "SMR.... HCD.... MAM.... ";
    dv = newDracView(trail2, messages);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeDracView(dv);

    char *trail3 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... "
                    "SMR.... HCD.... MAM.... ";
    dv = newDracView(trail3, messages);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER + LIFE_GAIN_REST);
    disposeDracView(dv);

    char *trail4 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... "
                    "SMR.... HCD.... MAM.... ";
    dv = newDracView(trail4, messages);
    assert(howHealthyIs(dv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeDracView(dv);

    return 1;
}

/*
* Test Vampire Matures
*/
static int bbTest12(void) {
    char *trail = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... ";
    PlayerMessage messages[] = {};
    GameView gv = newGameView(trail, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    disposeGameView(gv);

    //now the vampire matures...
    char *trail2 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED..V. ";
    gv = newGameView(trail2, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv)
        - SCORE_LOSS_VAMPIRE_MATURES);
    disposeGameView(gv);

    //check vampire maturing is only read from string
    //and not hardcoded in (even if invalid play)
    char *trail3 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... ";
    gv = newGameView(trail3, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    disposeGameView(gv);

    return 1;
}

/*
* Test Basic lastMove
*/
static int bbTest13(void) {
    char *trail = "GLO.... SBA.... HZU.... MSO.... DBR.... GMN.... STO.... "
                    "HST.... MVR.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    assert(giveMeTheRound(dv) == 1);
    assert(giveMeTheScore(dv) == GAME_START_SCORE - giveMeTheRound(dv));

    LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
    lastMove(dv, PLAYER_LORD_GODALMING, &start, &end);
    assert(start == LONDON);
    assert(end == MANCHESTER);

    lastMove(dv, PLAYER_DR_SEWARD, &start, &end);
    assert(start == BARCELONA);
    assert(end == TOULOUSE);

    lastMove(dv, PLAYER_VAN_HELSING, &start, &end);
    assert(start == ZURICH);
    assert(end == STRASBOURG);

    lastMove(dv, PLAYER_MINA_HARKER, &start, &end);
    assert(start == SOFIA);
    assert(end == VARNA);

    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == BERLIN);
    disposeDracView(dv);
    return 1;
}

/*
* Test Calling lastMove before a move has been made
*/
static int bbTest14(void) {
    char *trail = "GLO.... SBA.... HZU.... MSO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
    lastMove(dv, PLAYER_LORD_GODALMING, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == LONDON);

    lastMove(dv, PLAYER_DR_SEWARD, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == BARCELONA);

    lastMove(dv, PLAYER_MINA_HARKER, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == SOFIA);

    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == UNKNOWN_LOCATION);
    disposeDracView(dv);

    return 1;
}

/*
* Test Calling lastMove after teleportation
*/
static int bbTest15(void) {
    //teleport hunter to the hospital
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SCF.... HGA.... MCO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
    lastMove(dv, PLAYER_LORD_GODALMING, &start, &end);
    assert(start == LIVERPOOL);
    assert(end == MANCHESTER);
    lastMove(dv, PLAYER_DR_SEWARD, &start, &end);
    assert(start == MARSEILLES);
    assert(end == CLERMONT_FERRAND);
    disposeDracView(dv);

    //resurrect the hunter
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SMR.... HCD.... MAM.... "
            "DSW.... GSZ.... SCF.... HGA.... MAM.... ";
    dv = newDracView(trail2, messages);
    lastMove(dv, PLAYER_LORD_GODALMING, &start, &end);
    assert(start == MANCHESTER);
    assert(end == SZEGED);
    disposeDracView(dv);

    // dracula can teleport to Castle Dracula if there are no valid MOVEs
    char *trail3 = "GLO.... SMR.... HZU.... MAM.... DTPT... GSWT.... "
            "SCF.... HST.... MCO.... ";
    dv = newDracView(trail3, messages);
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == TELEPORT);
    disposeDracView(dv);

    char *trail4 = "GST.... SMA.... HSZ.... MSW.... DNP.V.. "
        "GCO.... SBO.... HCN.... MIR.... DBIT... "
        "GFR.... SBU.... HVR.... MAO.... DHIT... "
        "GHA.... SPA.... HCN.... MLS.... DD2T... "
        "GNS.... SMR.... HBS.... MSN....";

    dv = newDracView (trail4, messages);
    lastMove(dv,PLAYER_DRACULA,&start,&end);
    assert(start == HIDE && end == DOUBLE_BACK_2);
    assert(whereIs(dv,PLAYER_DRACULA) == BARI);
    disposeDracView(dv);

    return 1;
}

/*
* Testing lastMove on a resting, hide, and double backs
*/
static int bbTest16(void) {
    //Godalming resting
    char *trail = "GLO.... SAM.... HZU.... MBA.... DZU.... GLO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
    lastMove(dv, PLAYER_LORD_GODALMING, &start, &end);
    assert(start == LONDON);
    assert(end == LONDON);

    lastMove(dv, PLAYER_VAN_HELSING, &start, &end);
    assert(start == UNKNOWN_LOCATION);
    assert(end == ZURICH);
    disposeDracView(dv);

    //Dracula hides
    char *trail2 = "GLO.... SAM.... HZU.... MBA.... DZU.... GSW.... "
                   "SCO.... HST.... MTO.... DHI.... GLV.... SHA.... "
                   "HNU.... MCF.... ";
    dv = newDracView(trail2, messages);

    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == ZURICH);
    assert(end == HIDE);
    disposeDracView(dv);

    //dracula doubles back
    char *trail3 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                   "SCO.... HST.... MTO.... DD1.... GLV.... SHA.... "
                        "HNU.... MCF.... ";
    dv = newDracView(trail3, messages);

    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == ZURICH);
    assert(end == DOUBLE_BACK_1);
    disposeDracView(dv);

    char *trail4 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DMU.... GIR.... "
                    "SLI.... HFR.... MMR.... DD2.... GLV.... SHA.... "
                    "HNU.... MCF.... ";
    dv = newDracView(trail4, messages);
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == MUNICH);
    assert(end == DOUBLE_BACK_2);
    disposeDracView(dv);

    char *trail5 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                   "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                   "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                   "MCF.... DD3.... GMN.... SBR.... HLI.... MBO.... ";
    dv = newDracView(trail5, messages);
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == MILAN);
    assert(end == DOUBLE_BACK_3);
    disposeDracView(dv);

    char *trail6 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                   "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                   "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                   "MCF.... DGE.... GMN.... SBR.... HLI.... MBO.... "
                   "DD4.... GED.... SPR.... HBR.... MNA.... ";
    dv = newDracView(trail6, messages);
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == GENEVA);
    assert(end == DOUBLE_BACK_4);
    disposeDracView(dv);

    char *trail7 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                   "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                   "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                   "MCF.... DGE.... GMN.... SBR.... HLI.... MBO.... "
                   "DMR.... GED.... SPR.... HBR.... MNA.... DD5.... "
                   "GNS.... SVI.... HPR.... MLE.... ";
    dv = newDracView(trail7, messages);
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    assert(start == MARSEILLES);
    assert(end == DOUBLE_BACK_5);
    disposeDracView(dv);

    return 1;
}

/*
* Testing Basic whatsThere
*/
static int bbTest17(void) {
    //basic adding of traps and vampires
    char *trail = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR.... DMITV.. GLV.... SHA.... HNU.... "
                   "MCF.... DGETT.. GMN.... SBR.... HLI.... MBO.... "
                   "DMIT... GED.... SPR.... HBR.... MNA.... DVE.... "
                   "GNS.... SVI.... HPR.... MLE.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int numTraps = 0, numVamps = 0;
    whatsThere(dv, AMSTERDAM, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 0);

    whatsThere(dv, LONDON, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 0);

    whatsThere(dv, ZURICH, &numTraps, &numVamps);
    assert(numTraps == 1);
    assert(numVamps == 0);

    whatsThere(dv, MUNICH, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 1);

    whatsThere(dv, MILAN, &numTraps, &numVamps);
    assert(numTraps == 2);
    assert(numVamps == 1);

    whatsThere(dv, GENEVA, &numTraps, &numVamps);
    assert(numTraps == 2);
    assert(numVamps == 0);

    whatsThere(dv, VENICE, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 0);
    disposeDracView(dv);

    return 1;
}

/*
* Testing Traps are triggered or malfunction
*/
static int bbTest18(void) {
    //malfunctioning trap
    char *trail = "GHA.... SMA.... HVR.... MBI.... DSZT... "
                     "GCO.... SSR.... HSO.... MNP.... DZAT... "
                     "GFR.... STO.... HBE.... MRO.... DVIT... "
                     "GCO.... SSR.... HSO.... MNP.... DPRT... "
                     "GHA.... SMA.... HVR.... MBI.... DNUT... "
                     "GFR.... STO.... HBE.... MRO.... DSTT... "
                     "GHA.... SMA.... HVR.... MBI.... DPAT.M. "
                     "GHA.... SMA.... HVR.... MBI.... ";
     PlayerMessage messages[] = {};
     DracView dv = newDracView(trail, messages);

     int numTraps = 0, numVamps = 0;
     whatsThere(dv, SZEGED, &numTraps, &numVamps);
     assert(numTraps == 0);
     assert(numVamps == 0);
     disposeDracView(dv);
     
    //trap is triggered
    char *trail2 = "GNU.... SAM.... HGE.... MBA.... DZUT... GST.... "
                   "SCO.... HST.... MTO.... DZU.... GZUT... SFR.... "
                   "HFR.... MCF.... ";
    dv = newDracView(trail2, messages);
    whatsThere(dv, ZURICH, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 0);
    disposeDracView(dv);

    return 1;
}

/*
* Testing Vampires mature or are encountered
*/
static int bbTest19(void) {
    //vampire matures
    char *trail = "GHA.... SMA.... HVR.... MBI.... DSZ.V.. "
                     "GCO.... SSR.... HSO.... MNP.... DZAT... "
                     "GFR.... STO.... HBE.... MRO.... DVIT... "
                     "GCO.... SSR.... HSO.... MNP.... DPRT... "
                     "GHA.... SMA.... HVR.... MBI.... DNUT... "
                     "GFR.... STO.... HBE.... MRO.... DSTT... "
                     "GHA.... SMA.... HVR.... MBI.... DPAT.V. "
                     "GHA.... SMA.... HVR.... MBI.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int numTraps = 0, numVamps = 0;
    whatsThere(dv, ZURICH, &numTraps, &numVamps);
    assert(numTraps == 0);
    assert(numVamps == 0);
    disposeDracView(dv);

    //vampire encountered and killed by hunters
    char *trail2 = "GNU.... SAM.... HGE.... MBA.... DZUV... GST.... "
                   "SCO.... HST.... MTO.... DZUT... GZUV... SFR.... "
                   "HFR.... MCF.... ";
    dv = newDracView(trail2, messages);
    whatsThere(dv, ZURICH, &numTraps, &numVamps);
    assert(numTraps == 1);
    assert(numVamps == 0);
    disposeDracView(dv);

    return 1;
}

/*
* Testing Give me the trail
*/
static int bbTest20(void) {
    //check EMPTY trail
    char *trail = "";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    LocationID history[TRAIL_SIZE];
    giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == UNKNOWN_LOCATION);
    disposeDracView(dv);

    //check 1 trail
    char *trail2 = "GLO.... SAM.... HGE.... MBA.... ";
    dv = newDracView(trail2, messages);

    giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == LONDON);
    assert (history[1] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == AMSTERDAM);
    assert (history[1] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == GENEVA);
    assert (history[1] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == BARCELONA);
    assert (history[1] == UNKNOWN_LOCATION);
    disposeDracView(dv);

    //check 2 trail
    char *trail3 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... ";
    dv = newDracView(trail3, messages);
    giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == ZURICH);
    assert (history[1] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == SWANSEA);
    assert (history[1] == LONDON);
    assert (history[2] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == COLOGNE);
    assert (history[1] == AMSTERDAM);
    assert (history[2] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == STRASBOURG);
    assert (history[1] == GENEVA);
    assert (history[2] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == TOULOUSE);
    assert (history[1] == BARCELONA);
    assert (history[2] == UNKNOWN_LOCATION);
    disposeDracView(dv);

    //check 3 trail
    char *trail4 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR....";
    dv = newDracView(trail4, messages);
    giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == MUNICH);
    assert (history[1] == ZURICH);
    assert (history[2] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == IRISH_SEA);
    assert (history[1] == SWANSEA);
    assert (history[2] == LONDON);
    assert (history[3] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == LEIPZIG);
    assert (history[1] == COLOGNE);
    assert (history[2] == AMSTERDAM);
    assert (history[3] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == FRANKFURT);
    assert (history[1] == STRASBOURG);
    assert (history[2] == GENEVA);
    assert (history[3] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == MARSEILLES);
    assert (history[1] == TOULOUSE);
    assert (history[2] == BARCELONA);
    assert (history[3] == UNKNOWN_LOCATION);
    disposeDracView(dv);

    //check 4 trail
    char *trail5 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR.... DMITV.. GLV.... SHA.... HNU.... "
                   "MCF....";
   dv = newDracView(trail5, messages);
   giveMeTheTrail (dv, PLAYER_DRACULA, history);
   assert (history[0] == MILAN);
   assert (history[1] == MUNICH);
   assert (history[2] == ZURICH);
   assert (history[3] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
   assert (history[0] == LIVERPOOL);
   assert (history[1] == IRISH_SEA);
   assert (history[2] == SWANSEA);
   assert (history[3] == LONDON);
   assert (history[4] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
   assert (history[0] == HAMBURG);
   assert (history[1] == LEIPZIG);
   assert (history[2] == COLOGNE);
   assert (history[3] == AMSTERDAM);
   assert (history[4] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
   assert (history[0] == NUREMBURG);
   assert (history[1] == FRANKFURT);
   assert (history[2] == STRASBOURG);
   assert (history[3] == GENEVA);
   assert (history[4] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
   assert (history[0] == CLERMONT_FERRAND);
   assert (history[1] == MARSEILLES);
   assert (history[2] == TOULOUSE);
   assert (history[3] == BARCELONA);
   assert (history[4] == UNKNOWN_LOCATION);
   disposeDracView(dv);

    //check 5 trail
    char *trail6 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR.... DMITV.. GLV.... SHA.... HNU.... "
                   "MCF.... DGETT.. GMN.... SBR.... HLI.... MBO.... ";
   dv = newDracView(trail6, messages);
   giveMeTheTrail (dv, PLAYER_DRACULA, history);
   assert (history[0] == GENEVA);
   assert (history[1] == MILAN);
   assert (history[2] == MUNICH);
   assert (history[3] == ZURICH);
   assert (history[4] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == MANCHESTER);
   assert (history[1] == LIVERPOOL);
   assert (history[2] == IRISH_SEA);
   assert (history[3] == SWANSEA);
   assert (history[4] == LONDON);
   assert (history[5] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == BERLIN);
   assert (history[1] == HAMBURG);
   assert (history[2] == LEIPZIG);
   assert (history[3] == COLOGNE);
   assert (history[4] == AMSTERDAM);
   assert (history[5] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == LEIPZIG);
   assert (history[1] == NUREMBURG);
   assert (history[2] == FRANKFURT);
   assert (history[3] == STRASBOURG);
   assert (history[4] == GENEVA);
   assert (history[5] == UNKNOWN_LOCATION);

   giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == BORDEAUX);
   assert (history[1] == CLERMONT_FERRAND);
   assert (history[2] == MARSEILLES);
   assert (history[3] == TOULOUSE);
   assert (history[4] == BARCELONA);
   assert (history[5] == UNKNOWN_LOCATION);
   disposeDracView(dv);

    //check 6 trail
    char *trail7 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR.... DMITV.. GLV.... SHA.... HNU.... "
                   "MCF.... DGETT.. GMN.... SBR.... HLI.... MBO.... "
                   "DMIT... GED.... SPR.... HBR.... MNA.... ";
    dv = newDracView(trail7, messages);
   giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == MILAN);
    assert (history[1] == GENEVA);
    assert (history[2] == MILAN);
    assert (history[3] == MUNICH);
    assert (history[4] == ZURICH);
    assert (history[5] == UNKNOWN_LOCATION);

    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == EDINBURGH);
    assert (history[1] == MANCHESTER);
    assert (history[2] == LIVERPOOL);
    assert (history[3] == IRISH_SEA);
    assert (history[4] == SWANSEA);
    assert (history[5] == LONDON);

    giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
    assert (history[0] == PRAGUE);
    assert (history[1] == BERLIN);
    assert (history[2] == HAMBURG);
    assert (history[3] == LEIPZIG);
    assert (history[4] == COLOGNE);
    assert (history[5] == AMSTERDAM);

    giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
    assert (history[0] == BERLIN);
    assert (history[1] == LEIPZIG);
    assert (history[2] == NUREMBURG);
    assert (history[3] == FRANKFURT);
    assert (history[4] == STRASBOURG);
    assert (history[5] == GENEVA);

    giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
    assert (history[0] == NANTES);
    assert (history[1] == BORDEAUX);
    assert (history[2] == CLERMONT_FERRAND);
    assert (history[3] == MARSEILLES);
    assert (history[4] == TOULOUSE);
    assert (history[5] == BARCELONA);
    disposeDracView(dv);

    //check all trails have dropped off
    char *trail8 = "GLO.... SAM.... HGE.... MBA.... DZUT... GSW.... "
                   "SCO.... HST.... MTO.... DMU.V.. GIR.... SLI.... "
                   "HFR.... MMR.... DMITV.. GLV.... SHA.... HNU.... "
                   "MCF.... DGETT.. GMN.... SBR.... HLI.... MBO.... "
                   "DMIT... GED.... SPR.... HBR.... MNA.... DVE.... "
                   "GNS.... SVI.... HPR.... MLE.... ";
    dv = newDracView(trail8, messages);
    giveMeTheTrail (dv, PLAYER_DRACULA, history);
    assert (history[0] == VENICE);
     assert (history[1] == MILAN);
     assert (history[2] == GENEVA);
     assert (history[3] == MILAN);
     assert (history[4] == MUNICH);
     assert (history[5] == ZURICH);

     giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
     assert (history[0] == NORTH_SEA);
     assert (history[1] == EDINBURGH);
     assert (history[2] == MANCHESTER);
     assert (history[3] == LIVERPOOL);
     assert (history[4] == IRISH_SEA);
     assert (history[5] == SWANSEA);

     giveMeTheTrail (dv, PLAYER_DR_SEWARD, history);
     assert (history[0] == VIENNA);
     assert (history[1] == PRAGUE);
     assert (history[2] == BERLIN);
     assert (history[3] == HAMBURG);
     assert (history[4] == LEIPZIG);
     assert (history[5] == COLOGNE);

     giveMeTheTrail (dv, PLAYER_VAN_HELSING, history);
     assert (history[0] == PRAGUE);
     assert (history[1] == BERLIN);
     assert (history[2] == LEIPZIG);
     assert (history[3] == NUREMBURG);
     assert (history[4] == FRANKFURT);
     assert (history[5] == STRASBOURG);

     giveMeTheTrail (dv, PLAYER_MINA_HARKER, history);
     assert (history[0] == LE_HAVRE);
     assert (history[1] == NANTES);
     assert (history[2] == BORDEAUX);
     assert (history[3] == CLERMONT_FERRAND);
     assert (history[4] == MARSEILLES);
     assert (history[5] == TOULOUSE);
    disposeDracView(dv);

    return 1;
}

/*
* Testing Give me the trail after teleportation
*/
static int bbTest21(void) {
    //teleport hunter to the hospital
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SCF.... HGA.... MCO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    LocationID history[TRAIL_SIZE];
    giveMeTheTrail (dv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == MANCHESTER); //ACTUALLY THE HOSPITAL!
    assert (history[2] == SWANSEA);
    assert (history[1] == LIVERPOOL);
    assert (history[3] == LONDON);
    assert (history[4] == UNKNOWN_LOCATION);
    assert (history[5] == UNKNOWN_LOCATION);
    disposeDracView(dv);

    //Dracula teleports to Castle DRACULA,
    char *trail2 = "GMN.... SGE.... HMU.... MBU.... DBD.... GLO.... "
                    "SMR.... HZU.... MAM.... DTPT... GSWT.... SCF.... "
                    "HST.... MCO.... ";
     dv = newDracView(trail2, messages);

     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == CASTLE_DRACULA);
     assert(history[1] == BUDAPEST);
     assert(history[2] == UNKNOWN_LOCATION);
     assert(history[3] == UNKNOWN_LOCATION);
     assert(history[4] == UNKNOWN_LOCATION);
     assert(history[5] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     //Dracula hides
     char *trail3 = "GLO.... SAM.... HZU.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DHI.... GLV.... SHA.... "
                    "HNU.... MCF.... ";
     dv = newDracView(trail3, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == ZURICH);
     assert(history[2] == UNKNOWN_LOCATION);
     assert(history[3] == UNKNOWN_LOCATION);
     assert(history[4] == UNKNOWN_LOCATION);
     assert(history[5] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     //Dracula doubles back
     char *trail4 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DD1.... GLV.... SHA.... "
                         "HNU.... MCF.... ";
     dv = newDracView(trail4, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == ZURICH);
     assert(history[2] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     char *trail5 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                     "SCO.... HST.... MTO.... DMU.... GIR.... "
                     "SLI.... HFR.... MMR.... DD2.... GLV.... SHA.... "
                     "HNU.... MCF.... ";
     dv = newDracView(trail5, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == MUNICH);
     assert(history[2] == ZURICH);
     assert(history[3] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     char *trail6 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                    "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                    "MCF.... DD3.... GMN.... SBR.... HLI.... MBO.... ";
     dv = newDracView(trail6, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == MILAN);
     assert(history[2] == MUNICH);
     assert(history[3] == ZURICH);
     assert(history[4] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     char *trail7 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                    "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                    "MCF.... DGE.... GMN.... SBR.... HLI.... MBO.... "
                    "DD4.... GED.... SPR.... HBR.... MNA.... ";
     dv = newDracView(trail7, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == GENEVA);
     assert(history[2] == MILAN);
     assert(history[3] == MUNICH);
     assert(history[4] == ZURICH);
     assert(history[5] == UNKNOWN_LOCATION);
     disposeDracView(dv);

     char *trail8 = "GLO.... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                    "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                    "MCF.... DGE.... GMN.... SBR.... HLI.... MBO.... "
                    "DMR.... GED.... SPR.... HBR.... MNA.... DD5.... "
                    "GNS.... SVI.... HPR.... MLE.... ";
     dv = newDracView(trail8, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == ZURICH);
     assert(history[1] == MARSEILLES);
     assert(history[2] == GENEVA);
     assert(history[3] == MILAN);
     assert(history[4] == MUNICH);
     assert(history[5] == ZURICH);
     disposeDracView(dv);

     //and placing the hides etc. in the middle of strings
     char *trail9 =  "GLO.... SAM.... HAT.... MBA.... DZU.... GSW.... "
                    "SCO.... HVA.... MTO.... DHI.... GLV.... SHA.... "
                    "HSO.... MCF.... DMI.... GMN.... SBR.... HBC.... "
                    "MPA.... ";
     dv = newDracView(trail9, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == MILAN);
     assert(history[1] == ZURICH);
     assert(history[2] == ZURICH);
     assert(history[3] == UNKNOWN_LOCATION);
     disposeDracView(dv);

    char *trail10 = "GLOT... SAM.... HGE.... MBA.... DZU.... GSW.... "
                    "SCO.... HST.... MTO.... DMU.... GIR.... SLI.... "
                    "HFR.... MMR.... DMI.... GLV.... SHA.... HNU.... "
                    "MCF.... DGE.... GMN.... SBR.... HLI.... MBO.... "
                    "DMR.... GED.... SPR.... HBR.... MNA.... DD5.... "
                    "GNS.... SVI.... HPR.... MLE.... DST.... GAM.... "
                    "SBD.... HVI.... MEC.... DFRTT.. GCO.... SSZ.... "
                    "HBD.... MLO.... DHI.... GFRTTD. SKL.... HSZ.... "
                    "MMN.... DLI.... GNU.... SCD.... HKL.... MLV.... ";
     dv = newDracView(trail10, messages);
     giveMeTheTrail(dv, PLAYER_DRACULA, history);
     assert(history[0] == LEIPZIG);
     assert(history[1] == FRANKFURT);
     assert(history[2] == FRANKFURT);
     assert(history[3] == STRASBOURG);
     assert(history[4] == ZURICH);
     assert(history[5] == MARSEILLES);
     giveMeTheTrail(dv, PLAYER_LORD_GODALMING, history);

     assert(history[0] == NUREMBURG);
     assert(history[1] == FRANKFURT);
     assert(history[2] == COLOGNE);
     assert(history[3] == AMSTERDAM);
     assert(history[4] == NORTH_SEA);
     assert(history[5] == EDINBURGH);
     disposeDracView(dv);

     return 1;
}

/*
* Where can Dracula Go
*/
static int bbTest22(void) {
    //test Dracula's first turn (can travel anywhere)
    char *trail = "GLO.... SAM.... HGE.... MBA....";
    PlayerMessage messages[] = {""};
    DracView dv = newDracView (trail, messages);
    int size = 0;
    int *edges = whereCanIgo (dv, &size, true, true);

    assert (size == NUM_MAP_LOCATIONS);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++)
        seen[edges[i]] = true;
    for(int i = 0; i < NUM_MAP_LOCATIONS; i++)
        assert (seen[i] == true);

    free (edges);
    disposeDracView (dv);

    //can travel on land except by rail
    char *trail2 = "GLO.... SAM.... HZU.... MBA.... DGET... GSW.... "
                   "SCO.... HST.... MTO.... ";
    dv = newDracView(trail2, messages);

    edges = whereCanIgo (dv, &size, true, true);
    assert(size == 6);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[GENEVA]);
    assert(seen2[ZURICH]);
    assert(seen2[MARSEILLES]);
    assert(seen2[CLERMONT_FERRAND]);
    assert(seen2[PARIS]);
    assert(seen2[STRASBOURG]);
    assert(seen2[MILAN] == false); //rail connection

    free (edges);
    disposeDracView (dv);

    //can travel by sea
    char *trail3 = "GMR.... SAO.... HCD.... MAO.... DGO.... "
    "GGOD... SAO.... HCD.... MAO.... DTS.... "
    "GMR.... SAO.... HCD.... MAO.... ";
    dv = newDracView(trail3, messages);
    assert(whereIs(dv, PLAYER_DRACULA) == TYRRHENIAN_SEA);

    edges = whereCanIgo (dv, &size, true, true);
    //assert(size == 7); //Not valid for the Iteration 1
    assert(size == 6);
    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++) {
        seen3[edges[i]] = true;
    }

    assert(seen3[TYRRHENIAN_SEA]);
    assert(seen3[MEDITERRANEAN_SEA]);
    assert(seen3[IONIAN_SEA]);
    assert(seen3[ROME]);
    assert(seen3[NAPLES]);
    //assert(seen3[GENOA]); //Not valid for the Iteration 1
    assert(seen3[CAGLIARI]);

    free (edges);
    disposeDracView (dv);

    return 1;
}

/*
* Testing restrictions on whereCanIgo
*/
static int bbTest23(void) {
    //cannot double back
    char *trail = "GMR.... SAO.... HZU.... MAO.... DGO.... "
        "GGOD... SAO.... HZU.... MAO.... DTS.... "
        "GMR.... SAO.... HZU.... MAO.... DD1.... "
        "GMR.... SAO.... HZU.... MAO...."; //we cannot go to genoa or tyrrhenian!
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);
    assert(whereIs(dv, PLAYER_DRACULA) == TYRRHENIAN_SEA);

    int size = 0;
    LocationID *edges = whereCanIgo (dv, &size, true, true);
    bool seen[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++)
        seen[edges[i]] = true;

    //we have a double back in our trail, we canot go anywhere we have been before!
    //assert(size == 5); //Not valid for the Iteration 1
    assert(size == 6);
    //assert(!seen[TYRRHENIAN_SEA]);//Not valid for the Iteration 1
    assert(seen[TYRRHENIAN_SEA]);// only valid for iteration 1
    assert(seen[MEDITERRANEAN_SEA]);
    assert(seen[IONIAN_SEA]);
    assert(seen[ROME]);
    assert(seen[NAPLES]);
    assert(!seen[GENOA]);
    assert(seen[CAGLIARI]);

    free (edges);
    disposeDracView (dv);

    //check ST_JOSEPH_AND_ST_MARYS doesn't come up as a possible move
    char *trail2 = "GMN.... SGE.... HBA.... MSO.... DZA.... GLV.... SZU.... "
                    "HAL.... MVR.... ";
    dv = newDracView(trail2, messages);
    assert(whereIs(dv, PLAYER_DRACULA) == ZAGREB);

    edges = whereCanIgo(dv, &size, true, true);
    assert(size == 6);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
       seen2[edges[i]] = true;

    assert(seen2[ZAGREB]);
    assert(seen2[MUNICH]);
    assert(seen2[VIENNA]);
    assert(seen2[BUDAPEST]);
    assert(seen2[SZEGED]);
    assert(seen2[SARAJEVO]);
    assert(!seen2[ST_JOSEPH_AND_ST_MARYS]);

    free(edges);
    disposeDracView(dv);

    //cannot hide at sea
    char *trail3 = "GLO.... SAM.... HZU.... MMR.... DAO.... "
        "GSW.... SCO.... HST.... MCF.... DD1.... "
        "GLV.... SFR.... HFR.... MGE.... ";
    dv = newDracView(trail3, messages);
    assert(whereIs(dv, PLAYER_DRACULA) == ATLANTIC_OCEAN);

    edges = whereCanIgo(dv, &size, true, true);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
       seen3[edges[i]] = true;

     //assert(size == 8); //not valid for iteration 1
     assert(size == 9); //only valid for iteration 1
    assert(seen3[IRISH_SEA]);
    assert(seen3[BAY_OF_BISCAY]);
    assert(seen3[ENGLISH_CHANNEL]);
    assert(seen3[MEDITERRANEAN_SEA]);
    assert(seen3[NORTH_SEA]);
    assert(seen3[ATLANTIC_OCEAN]);//only valid for iteration 1
    //assert(!seen3[ATLANTIC_OCEAN]); //not valid for iteration 1
    assert(seen3[GALWAY]);
    assert(seen3[LISBON]);
    assert(seen3[CADIZ]);

    free(edges);
    disposeDracView(dv);

    return 1;
}

/*
* Testing rteleportation with whereCanIgo
*/
static int bbTest24(void) {
    //dracula must teleport- should be the only option in his whereCanIgo
    char *trail = "GMN.... SGE.... HBA.... MSO.... DAO.... "
        "GLV.... SZU.... HAL.... MVR.... DGW.... "
        "GIR.... SMU.... HMA.... MCN.... DHI.... "
        "GAO.... SVI.... HCA.... MBC.... DDU.... "
        "GEC.... SZA.... HLS.... MKL.... DD3.... "
        "GLE.... SJM.... HAO.... MCD.... ";
     PlayerMessage messages[] = {};
     DracView dv = newDracView(trail, messages);
     assert(whereIs(dv, PLAYER_DRACULA) == GALWAY);

     int size = 0;
     LocationID *edges = whereCanIgo(dv, &size, true, true);
     assert(size == 1);
     assert(edges[0] == GALWAY);

     free(edges);
     disposeDracView(dv);

     return 1;
}

/*
* Testing some falses in whereCanIgo
*/
static int bbTest25(void) {
     //checking with some road/sea as false
     //especially checking that we do not teleport if we list road or sea as
     //     false, when that could be an option
     char *trail = "GLO.... SAM.... HZU.... MMR.... DAT.... GSW.... "
                    "SCO.... HMU.... MTO.... DVA.... GLV.... SFR.... "
                    "HZA.... MBA.... DD2.... GMN.... SNU.... HVI.... "
                    "MAL.... ";
     PlayerMessage messages[] = {};
     DracView dv = newDracView(trail, messages);

     assert(whereIs(dv, PLAYER_DRACULA) == ATHENS);

     int size = 0;
     LocationID *edges = whereCanIgo(dv, &size, true, false);
     assert(size == 1);
     assert(edges[0] == ATHENS);

     free(edges);
     disposeDracView(dv);

     //no moves, but not teleporting
     char *trail2 = "GLO.... SAM.... HZU.... MMR.... DAT.... GSW.... "
                    "SCO.... HMU.... MTO.... DVA.... GLV.... SFR.... "
                    "HZA.... MBA.... DD2.... GMN.... SNU.... HVI.... "
                    "MAL.... DHI.... GLO.... SMU.... HBD.... MGR.... ";
     dv = newDracView(trail2, messages);
     assert(whereIs(dv, PLAYER_DRACULA) == ATHENS);
     edges = whereCanIgo(dv, &size, true, false);
     assert(size == 1); //for iteration one he can move to his current location
     free(edges);
     disposeDracView(dv);

     //no moves- road and rail false- but not teleporting
     char *trail3 = "GLO.... SAM.... HZU.... MMR.... DCD.... GSW.... "
                    "SCO.... HST.... MCF.... DHI.... GLV.... SFR.... "
                    "HFR.... MGE.... DD1.... GMN.... SLI.... HLI.... "
                    "MST.... ";
     dv = newDracView(trail3, messages);
     assert(whereIs(dv, PLAYER_DRACULA) == CASTLE_DRACULA);
     edges = whereCanIgo(dv, &size, false, false);
     assert(size == 1); //for iteration one he can move to his current location
     free(edges);
     disposeDracView(dv);

     return 1;
}

/*
* Testing Where can Players Go?
*/
static int bbTest26(void) {
    //check its the same as where can I go if called for dracula
    char *trail = "GLO.... SAM.... HZU.... MBA.... DGET... GSW.... "
                   "SCO.... HST.... MTO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0, size2 = 0;
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_DRACULA, true, false, true);
    LocationID *edges2 = whereCanIgo(dv, &size2, true, true);

    assert(size == size2);
    for(int i = 0; i < size; i++) {
        assert(edges[i] == edges2[i]);
    }

    free(edges);
    free(edges2);
    disposeDracView(dv);

    char *trail2 = "GMR.... SAO.... HZU.... MAO.... DGO.... GGOD... "
                    "SAO.... HZU.... MAO.... DTS.... GMR.... SAO.... "
                    "HZU.... MAO.... DD1.... GMR.... SAO.... HZU.... "
                    "MAO....";
    dv = newDracView(trail2, messages);

    edges = whereCanTheyGo(dv, &size, PLAYER_DRACULA, true, true, true);
    edges2 = whereCanIgo(dv, &size2, true, true);

    assert(size == size2);
    for(int i = 0; i < size; i++) {
        assert(edges[i] == edges2[i]);
    }

    free(edges);
    free(edges2);
    disposeDracView(dv);

    char *trail3 = "GMN.... SGE.... HBA.... MSO.... DAO.... GLV.... SZU.... "
                    "HAL.... MVR.... DGW.... GIR.... SMU.... HMA.... MCN.... "
                    "DHI.... GAO.... SVI.... HCA.... MBC.... DDU.... GEC.... "
                    "SZA.... HLS.... MKL.... DD3.... GLE.... SJM.... HAO.... "
                    "MCD.... ";
     dv = newDracView(trail3, messages);

     edges = whereCanTheyGo(dv, &size, PLAYER_DRACULA, true, true, true);
     edges2 = whereCanIgo(dv, &size2, true, true);

     assert(size == size2);
     for(int i = 0; i < size; i++) {
        assert(edges[i] == edges2[i]);
     }

     free(edges);
     free(edges2);
     disposeDracView(dv);

     return 1;
}

/*
* Testing Basic whereCanTheyGo for Hunters
*/
static int bbTest27(void) {
    //empty pastPlays
    char *trail = "";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_LORD_GODALMING,
                                        true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_VAN_HELSING, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
        seen[edges[i]] = true;
    }

    for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
        assert(seen[i] == true);
    }
    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_MINA_HARKER, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeDracView(dv);

    return 1;
}

/*
* Testing whereCanTheyGo road connections only
*/
static int bbTest28(void) {
    char *trail = "GGW.... SHA.... HMI.... MAO.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_LORD_GODALMING,
                                        true, false, false);
    assert(size == 2);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[GALWAY]);
    assert(seen[DUBLIN]);
    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_DR_SEWARD, true, false, false);
    assert(size == 4);
    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[HAMBURG]);
    assert(seen2[BERLIN]);
    assert(seen2[LEIPZIG]);
    assert(seen2[COLOGNE]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_VAN_HELSING, true, false, false);
    assert(size == 6);
    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[MILAN]);
    assert(seen3[GENOA]);
    assert(seen3[VENICE]);
    assert(seen3[MUNICH]);
    assert(seen3[ZURICH]);
    assert(seen3[MARSEILLES]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_MINA_HARKER, true, false, false);
    assert(size == 1);
    free(edges);
    disposeDracView(dv);

    return 1;
}

/*
* Testing whereCanTheyGo sea connections only
*/
static int bbTest29(void) {
    char * trail = "GAO.... SCG.... HRO.... MZU.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_LORD_GODALMING,
                                        false, false, true);
    assert(size == 9);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[ATLANTIC_OCEAN]);
    assert(seen[NORTH_SEA]);
    assert(seen[GALWAY]);
    assert(seen[IRISH_SEA]);
    assert(seen[ENGLISH_CHANNEL]);
    assert(seen[BAY_OF_BISCAY]);
    assert(seen[LISBON]);
    assert(seen[CADIZ]);
    assert(seen[MEDITERRANEAN_SEA]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_DR_SEWARD, false, false, true);
    assert(size == 3);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[CAGLIARI]);
    assert(seen2[MEDITERRANEAN_SEA]);
    assert(seen2[TYRRHENIAN_SEA]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_VAN_HELSING, false, false, true);
    assert(size == 2);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[ROME]);
    assert(seen3[TYRRHENIAN_SEA]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_MINA_HARKER, false, false, true);
    assert(size == 1);

    free(edges);
    disposeDracView(dv);

    return 1;
}

/*
* Testing whereCanTheyGo rail connections only
*/
static int bbTest30(void) {
    char *trail = "GPA.... SPA.... HPA.... MPA.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_LORD_GODALMING,
                                    false, true, false);
    assert(size == 5);//Round + PLAYER_LORD_GODALMING = 1 (for his next turn)

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[PARIS]);
    assert(seen[LE_HAVRE]);
    assert(seen[BORDEAUX]);
    assert(seen[MARSEILLES]);
    assert(seen[BRUSSELS]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_DR_SEWARD, false, true, false);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[PARIS]);
    assert(seen2[LE_HAVRE]);
    assert(seen2[BORDEAUX]);
    assert(seen2[MARSEILLES]);
    assert(seen2[BRUSSELS]);
    assert(seen2[COLOGNE]);
    assert(seen2[SARAGOSSA]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_VAN_HELSING, false, true, false);
    assert(size == 10);
    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[PARIS]);
    assert(seen3[LE_HAVRE]);
    assert(seen3[BORDEAUX]);
    assert(seen3[MARSEILLES]);
    assert(seen3[BRUSSELS]);
    assert(seen3[COLOGNE]);
    assert(seen3[SARAGOSSA]);
    assert(seen3[MADRID]);
    assert(seen3[BARCELONA]);
    assert(seen3[FRANKFURT]);

    free(edges);

    edges = whereCanTheyGo(dv, &size, PLAYER_MINA_HARKER, false, true, false);
    assert(size == 1);

    free(edges);
    disposeDracView(dv);

    return 1;
}

/*
* Testing whereCanTheyGo combination of all for Hunters
*/
static int bbTest31(void) {
    char *trail = "GBR.... SNU.... HAT.... MSN.... DCD.... "
                    "GHA.... SIO.... HST.... MMA.... ";
    PlayerMessage messages[] = {};
    DracView dv = newDracView(trail, messages);

    int size = 0;
    //2 rail hops allowed
    LocationID *edges = whereCanTheyGo(dv, &size, PLAYER_LORD_GODALMING,
                                        true, true, true);
    assert(whereIs(dv, PLAYER_LORD_GODALMING) == HAMBURG);
    assert(size == 6);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(seen[HAMBURG]);
    assert(seen[BERLIN]);
    assert(seen[LEIPZIG]);
    assert(seen[COLOGNE]);
    assert(seen[NORTH_SEA]);
    assert(seen[PRAGUE]);

    free(edges);

    //3 rail hops allowed
    edges = whereCanTheyGo(dv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen2[edges[i]] = true;
    }


    assert(seen2[IONIAN_SEA]);
    assert(seen2[TYRRHENIAN_SEA]);
    assert(seen2[ADRIATIC_SEA]);
    assert(seen2[VALONA]);
    assert(seen2[ATHENS]);
    assert(seen2[SALONICA]);
    assert(seen2[BLACK_SEA]);

    free(edges);

    //0 rail hops allowed
    edges = whereCanTheyGo(dv, &size, PLAYER_VAN_HELSING, true, true, true);
    assert(size == 9);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[STRASBOURG]);
    assert(seen3[GENEVA]);
    assert(seen3[PARIS]);
    assert(seen3[BRUSSELS]);
    assert(seen3[COLOGNE]);
    assert(seen3[FRANKFURT]);
    assert(seen3[NUREMBURG]);
    assert(seen3[MUNICH]);
    assert(seen3[ZURICH]);

    free(edges);

    //No rail hops
    edges = whereCanTheyGo(dv, &size, PLAYER_MINA_HARKER, true, true, true);
    assert(size == 7);

    bool seen4[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen4[edges[i]] = true;

    assert(seen4[MADRID]);
    assert(seen4[LISBON]);
    assert(seen4[CADIZ]);
    assert(seen4[GRANADA]);
    assert(seen4[ALICANTE]);
    assert(seen4[SARAGOSSA]);
    assert(seen4[SANTANDER]);

    free(edges);
    disposeDracView(dv);

    return 1;
}
