////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Custom.h: an interface that will be merged with GameView.h
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

//This is a temporary file until we can move them into the other .h files for iteration 2

#ifndef CUSTOM_H_
#define CUSTOM_H_

#include "Player.h"


#define isHospital(place)  (place == ST_JOSEPH_AND_ST_MARYS)
#define currRound(moveID) (moveID/NUM_PLAYERS)
#define isValidRound(gv,round) (round <= getRound(gv) &&  round >= 0)
#define isValidMove(gv,move) (move < getCurrMove(gv)  &&  move >= 0)
#define gameMoveID(currentMoveID,moves)(currentMoveID-(NUM_PLAYERS*moves))

typedef struct move * GameMove;
typedef int GameScore;

int getCurrMove(GameView gv);
GameScore getMoveCost(GameMove move);
GameMove createMove();
GameMove * getGameHistory(GameView gv);

Player getMovePlayer(GameMove mv);
Player getPlayer (GameView gv, PlayerID player);
Player getCurrentPlayerObj (GameView gv);
LocationID getMoveLocation(GameMove mv);
LocationID getMoveType(GameMove mv);
LocationID moveLocation(GameView gv, PlayerID player, int pastMove);
LocationID * getTrail(GameView gv, PlayerID playerID);
LocationID getPastLocation (GameView gv, PlayerID playerID, int pastRound);
LocationID getPastMoveLocation (GameView gv, PlayerID playerID, int pastRound);
GameMove getMove(GameView gv, int moveID);
GameMove getPastMove(GameView gv, GameMove move, int travelBack);
GameMove getPlayerPastMove (GameView gv, PlayerID playerID, int pastRound);
int getTrapsFromGV(GameView gv, LocationID where);
int getVampsFromGV(GameView gv, LocationID where);
int getMoveID(GameMove mv);
int getMoveTrap(GameMove mv);
int getMoveVampire(GameMove mv);
int getMoveDracula(GameMove mv);
int getMoveMalfunction(GameMove mv);
int getMoveMatured(GameMove mv);

void setMoveID(GameMove mv, int id);
void setMoveLocation(GameMove move, LocationID location);
void setMoveTrap(GameMove mv, int trap);
void setMoveVampire(GameMove mv, int vampire);
void setMoveDracula(GameMove mv, int dracula);
void setMoveMalfunction(GameMove mv, int malfunction);
void setMoveMatured(GameMove mv, int matured);

int timeSinceTurn(GameView gv, PlayerID player);
LocationID * whereCanDraculaGoUpdate(GameView gv, int *numLocations, LocationID *places, PlayerID player, bool road, bool sea);

#endif // !defined(MAP_H_)
