
////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Player.h: an interface to the Player
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#ifndef PLAYER_H_
#define PLAYER_H_

#define isPlayer(player) ((player>=0) && (player < NUM_PLAYERS))

typedef int Health;
typedef struct player *Player;

Player createPlayer(int id);
PlayerID getPlayerID(Player player);
LocationID getSmartPlayerLocation(Player player);
LocationID getPlayerLocationType(Player player);
LocationID getPlayerLocation(Player player);
Health getPlayerHealth(Player player);

void setPlayerID(Player player, PlayerID id);
void setPlayerLocation(Player player, LocationID id);
void setPlayerLocationType(Player player, LocationID id);
void setPlayerHealth(Player player, Health health);

PlayerID playerAbbrevToID(char playerAbv);
bool isEnemy(Player p1, Player p2);
bool isHunter(Player player);
char * idToPlayerName (PlayerID player);


#endif // !defined (PLAYER_H_)
