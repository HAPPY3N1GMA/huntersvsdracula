////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// HunterView.c: the HunterView ADT implementation
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "HunterView.h"
#include "Custom.h"
#include "Map.h"


struct hunterView {
    GameView gv;
};


/*
 * newHunterView
 * Creates a new HunterView to summarise the current state of the game
 */
HunterView newHunterView (char *pastPlays, PlayerMessage messages[]) {
    HunterView new = malloc (sizeof *new);
    if (new == NULL) {
        fprintf(stderr, "%s: Couldn't allocate HunterView\n", __func__);
    }
    new->gv = newGameView(pastPlays, messages);
    return new;
}


/*
 * disposeHunterView
 * Frees all memory previously allocated for the HunterView toBeDeleted
 */
void disposeHunterView (HunterView toBeDeleted) {
    if(toBeDeleted == NULL) {
        return;
    }
    disposeGameView(toBeDeleted->gv);
    free (toBeDeleted);
}

/*
 * giveMeTheRound
 * Get the current round
 */
// Get the current round
Round giveMeTheRound (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getRound(hv->gv);
}


/*
 * whoAmI
 * Get the id of the current player
 */
PlayerID whoAmI (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getCurrentPlayer(hv->gv);
}


/*
 * giveMeTheScore
 * Get the current score
 */
int giveMeTheScore (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getScore(hv->gv);
}

/*
 * howHealthyIs
 * Get the current health points for a given player
 */
int howHealthyIs (HunterView hv, PlayerID player) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getHealth(hv->gv, player);
}


/*
 * whereIs
 * Get the current location id of a given player
 */
LocationID whereIs (HunterView hv, PlayerID player) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    LocationID place = getPlayerLocation(getPlayer(hv->gv, player)); //actual location deduced
    if (player == PLAYER_DRACULA) {
        place = getPlayerLocation(getPlayer(hv->gv, player));
        if(!validPlace(place)) {
            return getLocation(hv->gv, player);
        }else{
            return place;
        }
    }
    return place;
}


/*
 * giveMeTheTrail
 * Fills the trail array with the location ids of the last 6 turns
 */
void giveMeTheTrail (HunterView hv, PlayerID player, LocationID trail[TRAIL_SIZE]) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    //copy the trail
    LocationID *t = getTrail(hv->gv, player);
    for(int i = 0; i<TRAIL_SIZE; i++){
        trail[i] = t[i];
    }
}

/*
 * whereCanIgo
 * What are my possible next moves (locations)
 */
LocationID *whereCanIgo (HunterView hv, int *numLocations,
    bool road, bool rail, bool sea) {

    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return whereCanTheyGo(hv, numLocations, whoAmI(hv), road, rail, sea);
}


/*
 * whereCanTheyGo
 * What are the specified player's next possible moves
 */
LocationID *whereCanTheyGo (HunterView hv, int *numLocations, PlayerID player,
    bool road, bool rail, bool sea) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    //if players turn is before ours, then their turn is the next round
    int currRound = getRound(hv->gv);
    if(player< getCurrentPlayer(hv->gv)){
        currRound++;
    }

    //where can we get too from our current location?
    LocationID currLoc = getPastLocation(hv->gv, player, 0);
    LocationID *where = connectedLocations (hv->gv, numLocations,
        currLoc,player, currRound,
        road, rail, sea);

    if(player == PLAYER_DRACULA){
        LocationID whereIsDrac = whereIs (hv, PLAYER_DRACULA);
        if(validPlace(whereIsDrac) || whereIsDrac == TELEPORT){
            LocationID *places = whereCanDraculaGoUpdate(hv->gv, numLocations, where, player, road, sea);
            return places;
        } else if (currRound == 0) {//can go anywhere on his first turn
            where = realloc(where,sizeof(LocationID)*(NUM_MAP_LOCATIONS));
            for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
                where[i] = i;
            }
            *numLocations = NUM_MAP_LOCATIONS;
            return where;
        } else {//do not know his location, thus return NULL
            free(where);
            *numLocations = 0;
            return NULL;
        }
    }
    return where;
}
