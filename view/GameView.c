////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// GameView.c: GameView ADT implementation
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "Map.h"
#include "Custom.h"
#include "Player.h"


struct gameView{
    int numRounds;                                                      //number of game rounds (every player has played x times)
    int numMoves;                                                       //current game move
    int score;                                                                //current game score
    GameMove *gameHistory;                                   //pointers to each move that has occurred
    Player *players;                                                    //pointers to each players current player struct
    Map gameWorld;                                                  //a fully constructed world map
    LocationID trails[NUM_PLAYERS][TRAIL_SIZE];     //trail for each player
};

struct move{
    int id;                                   //move number
    Player player;                      //player performing the move
    LocationID location;           //location as read from string
    LocationID locationtype;    //deduced location (may differ)
    int trap;                                //number of traps planted
    int vampire;                         //number of vampires planted
    int dracula;                          //number of dracula encounters
    int malfunction;                  //number of traps malfunctioned
    int matured;                        //number of vampires matured
};


//Helper Functions
static GameMove parseMove(char *mvString);
static GameView createGameView(void);
static void buildGameTrails(GameView gv);
static void buildGameMoves(GameView gv, char *pastPlays, int *numMoves, int *numRounds);
static void playGame (GameView gv, int numMoves, int numRounds);


/**********************************************
*    createGameView
*    create a new gameView struct object
**********************************************/
static GameView createGameView(void) {
    GameView g = malloc (sizeof(struct gameView));
    if(g == NULL) {
        fprintf(stderr, "%s: Error creating gameView\n", __func__);
        abort();
    }
    g->numRounds = 0;
    g->numMoves = 0;
    g->score = 0;
    g->gameHistory = NULL;
    g->players = NULL;
    g->gameWorld = NULL;
    return g;
}


/**********************************************
*    newGameView
*    Creates a new GameView to summarise the current state of the game
**********************************************/
GameView newGameView (char *pastPlays, PlayerMessage messages[]) {
    GameView gv = createGameView();

    //create new game world
    gv->gameWorld = newMap();

    //init gameHistory
    gv->gameHistory = calloc(0, sizeof(GameMove));
    if(gv->gameHistory == NULL) {
        fprintf(stderr, "%s: Cannot allocate memory for gameHistory\n",__func__);
        abort();
    }

    //parse pastPlays string into individual game moves
    int numMoves = 0;
    int numRounds = 0;
    buildGameMoves(gv,pastPlays,&numMoves,&numRounds);

    //create some starting players
    gv->players = calloc(NUM_PLAYERS, sizeof(Player));
    if(gv->players == NULL) {
        fprintf(stderr, "%s: Cannot allocate memory for players\n",__func__);
        abort();
    }
    for(int i = numMoves; i < NUM_PLAYERS; i++) {
        Player player = createPlayer(i);
        if(i == PLAYER_DRACULA) {
            setPlayerHealth(player, GAME_START_BLOOD_POINTS);
        } else {
            setPlayerHealth(player, GAME_START_HUNTER_LIFE_POINTS);
        }
        gv->players[i] = player;
    }

    //set the starting score
    gv->score = GAME_START_SCORE;

    //play the game!
    playGame (gv, numMoves, numRounds);

    //build game trails from past moves
    buildGameTrails(gv);

    //if current hunter player is in hospital, update their health
    Player currPlayer = getCurrentPlayerObj(gv);
    if(getPlayerHealth(currPlayer) <= 0 && isHunter(currPlayer)) {
        setPlayerHealth(currPlayer, GAME_START_HUNTER_LIFE_POINTS);
    }
    return gv;
}


/**********************************************
*    buildGameMoves
*    Parse pastPlays string into array of game moves structs
**********************************************/
static void buildGameMoves(GameView gv, char *pastPlays, int *numMoves, int *numRounds){
    char *temp = strdup(pastPlays);
    char *currStr = strtok(temp, " ");
    int moves = *numMoves;
    int rounds = *numRounds;
    while(currStr != NULL) {
        gv->gameHistory = realloc(gv->gameHistory, sizeof(GameMove) * (moves + 1));
        if(gv->gameHistory == NULL) {
            fprintf(stderr, "%s: Error resizing GameView gameHistory\n", __func__);
            abort();
        }
        gv->gameHistory[moves] = parseMove(currStr);
        gv->gameHistory[moves]->id = moves;
        moves++;
        rounds = moves / NUM_PLAYERS;
        currStr = strtok(NULL, " ");
    }
    *numMoves = moves;
    *numRounds = rounds;
    free(temp);
}


/**********************************************
*    buildGameTrails
*    create an array of previous move locations for each player
**********************************************/
static void buildGameTrails(GameView gv){
    //build player trails
    Player currPlayer = getCurrentPlayerObj(gv);
    for(int i = 0; i < NUM_PLAYERS; i++) {
        for(int k = 0; k < TRAIL_SIZE; k++) {
            GameMove move = getPlayerPastMove(gv, i, k);
            if(isHunter(currPlayer)) {
                if(move == NULL) {
                    gv->trails[i][k] = UNKNOWN_LOCATION;
                } else {
                    LocationID resolvedLoc = getPastMoveLocation(gv,i, k);
                    if(validPlace(resolvedLoc)) {
                        gv->trails[i][k] = resolvedLoc;
                    } else {
                        gv->trails[i][k] = move->locationtype;
                    }
                }
            } else {
                if(move == NULL || validPlace(move->locationtype)) {
                    gv->trails[i][k] = getPastMoveLocation(gv,i, k);
                } else {
                    gv->trails[i][k] = move->locationtype;
                }
            }
        }
    }
}


/**********************************************
*    parseMove
*    parse a past move string into a GameMove struct
**********************************************/
static GameMove parseMove(char *mvString) {
    GameMove mv = createMove();
    mv->player = createPlayer(playerAbbrevToID(mvString[0]));

    //store the location
    char tmp[2] = "..";
    LocationID loc = abbrevToID(strncpy(tmp, &(mvString[1]), 2));

    //if location is unknown, determine what type of move it was and store it
    if(loc == NOWHERE) {
        loc = unknownAbvToID(strncpy(tmp, &(mvString[1]), 2));
    }
    mv->locationtype = loc;
    mv->location = loc;

    setPlayerLocation(mv->player, loc);
    setPlayerLocationType(mv->player, loc);

    //Set current players location data
    if(isHunter(mv->player)) {
        //set player health to default full
        setPlayerHealth(mv->player, GAME_START_HUNTER_LIFE_POINTS);
        for(int k = 0; k < 4; k++) {
            switch(mvString[k+3]) {
                case 'T': ++mv->trap; break;
                case 'V': ++mv->vampire; break;
                case 'D': ++mv->dracula; break;
            }
        }
    } else {
        //set player health to default full
        setPlayerHealth(mv->player, GAME_START_BLOOD_POINTS);
        for(int k = 0; k < 2; k++) {
            switch(mvString[k+3]) {
                case 'T': ++mv->trap; break;
                case 'V': ++mv->vampire; break;
            }
        }
        switch(mvString[5]) {
            case 'M': ++mv->malfunction; break;
            case 'V': ++mv->matured; break;
        }
    }
    return mv;
}


/**********************************************
*    playGame
*    Plays the a given game using all available gameHistory, updating the gameworld and players
**********************************************/
static void playGame (GameView gv, int numMoves, int numRounds) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    Map gameWorld = gv->gameWorld;
    GameMove *gameMoves = gv->gameHistory;
    Player *gamePlayers = gv->players;

    for(int i = 0; i < numMoves; i++) {
        //play the game move
        playMove(gv, gameWorld, gameMoves[i], gameMoves);
        Player currPlayer = gameMoves[i]->player;

        //set player health to that of their previous move
        if(currRound(i) > 0) {
            Health oldHealth = getPlayerHealth(gamePlayers[getCurrentPlayer(gv)] );
            setPlayerHealth(currPlayer, oldHealth);
        }
        //update gameView player list
        gamePlayers[getCurrentPlayer(gv)] = currPlayer;

        //update players health (traps encounters etc)
        updateMoveHealth(gv, gameWorld, gamePlayers[getCurrentPlayer(gv)], gameMoves[i]);

        //score game
        gv->score -= getMoveCost(gameMoves[i]);
        gv->numMoves++;
        if(getPlayerID(currPlayer) == PLAYER_DRACULA) {
            setMapTrap(gv->gameWorld, getPlayerLocation(currPlayer), gameMoves[i]->trap);
            setMapVamp(gv->gameWorld, getPlayerLocation(currPlayer), gameMoves[i]->vampire);
            if(i >= NUM_PLAYERS * TRAIL_SIZE && validPlace(getPastLocation(gv, getPlayerID(currPlayer), TRAIL_SIZE))) {
                setMapTrap(gv->gameWorld, getPastLocation(gv, getPlayerID(currPlayer), TRAIL_SIZE), -gameMoves[i]->malfunction);
                setMapVamp(gv->gameWorld, getPastLocation(gv, getPlayerID(currPlayer), TRAIL_SIZE), -gameMoves[i]->matured);
            }
        }
    }
    gv->numRounds = numRounds;
}


/**********************************************
*    disposeGameView
*    Frees all memory previously allocated for the GameView gv
**********************************************/
void disposeGameView (GameView gv) {
    if(gv == NULL) {
        return;
    }
    for(int i = 0; i < gv->numMoves; i++) {
        free(gv->gameHistory[i]->player);
        free(gv->gameHistory[i]);
    }
    for(int i = gv->numMoves; i < NUM_PLAYERS; i++) {
        free(gv->players[i]);
    }
    disposeMap(gv->gameWorld);
    free(gv->players);
    free(gv->gameHistory);
    free (gv);
}


/**********************************************
* getRound
* return the current game round
**********************************************/
Round getRound (GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return  gv->numRounds;
}


/**********************************************
* getCurrentPlayer
* Get the id of current player - ie whose turn is it?
**********************************************/
PlayerID getCurrentPlayer (GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return (gv->numMoves % NUM_PLAYERS);
}


/**********************************************
* getCurrentPlayerObj
* Get the current player object - ie whose turn is it?
**********************************************/
Player getCurrentPlayerObj (GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return gv->players[getCurrentPlayer(gv)];
}


/**********************************************
* getScore
* Get the current game score
**********************************************/
int getScore (GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return gv->score;
}


/**********************************************
* getHealth
* Get the current health points for a given player
**********************************************/
int getHealth (GameView gv, PlayerID player) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isPlayer(player)) {
        fprintf(stderr, "%s: Invalid Player\n", __func__);
        abort();
    }
    return getPlayerHealth(gv->players[player]);
}


/**********************************************
* getLocation
* Get the current location (from the parsed string) for this playerID
* We dont want their "actual" location
**********************************************/
LocationID getLocation (GameView gv, PlayerID player) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isPlayer(player)) {
        fprintf(stderr, "%s: Invalid Player\n", __func__);
        abort();
    }

    if(player == PLAYER_DRACULA) {
        GameMove move = getPlayerPastMove(gv, PLAYER_DRACULA, 0);
        if(move == NULL) return UNKNOWN_LOCATION;
        return getMoveLocation(getPlayerPastMove(gv, PLAYER_DRACULA, 0));
    } else {
        return getPlayerLocationType(getPlayer(gv, player));
    }
}


/**********************************************
* setMoveLocation
* Set the move objects Location
**********************************************/
void setMoveLocation(GameMove move, LocationID location) {
    if(move == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    move->location = location;
}


/**********************************************
* getHistory
* Fills the trail array with the location ids of the last 6 turns
**********************************************/
void getHistory (GameView gv, PlayerID player, LocationID trail[TRAIL_SIZE]) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    int move = (((gv->numMoves - player - 1 + NUM_PLAYERS) / NUM_PLAYERS) - 1) * NUM_PLAYERS + player;
    int i = 0;
    while(i < TRAIL_SIZE && move >= 0) {
        trail[i] = gv->gameHistory[move]->location;
        move -= NUM_PLAYERS;
        i++;
    }
    for(; i < TRAIL_SIZE; i++) {
        trail[i] = UNKNOWN_LOCATION;
    }
    return;
}



/**********************************************
* connectedLocations
* Returns an array of LocationIDs for all directly connected locations
**********************************************/
LocationID * connectedLocations (GameView gv, int *numLocations, LocationID from,
    PlayerID player, Round round, bool road, bool rail, bool sea) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }

    //if from UNKNOWN_LOCATION in the first round, you can go anywhere!
    if(from == UNKNOWN_LOCATION && round == 0) {
        *numLocations = NUM_MAP_LOCATIONS;
        LocationID *locations = malloc(sizeof(LocationID) * NUM_MAP_LOCATIONS);
        int curr = MIN_MAP_LOCATION;
        for(int i = 0; i < NUM_MAP_LOCATIONS; i++,curr++)
            locations[i] = curr;
        return locations;
    }
    return updateConnectedLocations(gv->gameWorld, numLocations, from, player, round,
        road, rail, sea);
}


/**********************************************
* getPlayer
* Get the requrested player object
**********************************************/
Player getPlayer (GameView gv, PlayerID player) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isPlayer(player)) {
        fprintf(stderr, "%s:Invalid Player\n", __func__);
        abort();
    }
    return gv->players[player];
}


/**********************************************
*    createMove
*    create a new move struct object
**********************************************/
GameMove createMove() {
    GameMove mv = malloc (sizeof(struct move));
    if(mv == NULL) {
        fprintf(stderr, "%s:Error Creating Move\n", __func__);
        abort();
    }
    mv->id = -1;
    mv->player = NULL;
    mv->location = -1;
    mv->locationtype = -1;
    mv->trap = 0;
    mv->vampire = 0;
    mv->dracula = 0;
    mv->malfunction = 0;
    mv->matured = 0;
    return mv;
}


/**********************************************
*   getMoveCost
*   calculates the score lost at a particular move
**********************************************/
GameScore
getMoveCost(GameMove move) {
    if(move == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    int penalty = 0;

    // * The score decreases by 1 each time Dracula finishes a turn
    if(getPlayerID(move->player) == PLAYER_DRACULA) {
        penalty += SCORE_LOSS_DRACULA_TURN;
    } else {
        // * The score decreases by 6 each time a hunter is teleported to the hospital.
        // note a player can move to hospital when not sick, thus only deduct when player has <=0 health
        if(getPlayerHealth(move->player) <= 0) {
            penalty += SCORE_LOSS_HUNTER_HOSPITAL;
        }
    }
    // * The score decreases by 13 each time a vampire matures
    penalty += (SCORE_LOSS_VAMPIRE_MATURES * move->matured );
    return penalty;
}


/**********************************************
*   getMove
*   get particular move from game history
**********************************************/
GameMove getMove(GameView gv, int moveID) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isValidMove(gv, moveID)) {
        fprintf(stderr, "%s: Move out of Bounds\n", __func__);
        abort();
    }
    return gv->gameHistory[moveID];
}


/**********************************************
*   getPastMove
*   travel back in time to get a past move (ie 2 rounds back)
**********************************************/
GameMove getPastMove(GameView gv, GameMove move, int travelBack) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(move == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    int moveID = getMoveID(move);
    return getMove(gv,gameMoveID(moveID,travelBack));
}


/**********************************************
*   getTrail
*   return a players trail
**********************************************/
LocationID * getTrail(GameView gv, PlayerID playerID) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isPlayer(playerID)) {
        fprintf(stderr, "%s:invalid PlayerID\n", __func__);
        abort();
    }
    return gv->trails[playerID];
}


/**********************************************
*   timeSinceTurn
*   Return how many moves since players last move
**********************************************/
int timeSinceTurn(GameView gv, PlayerID player) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    if(!isPlayer(player)) {
        fprintf(stderr, "%s: Invalid PlayerID\n", __func__);
        abort();
    }
    PlayerID currPlayerID = getCurrentPlayer(gv);
    if(currPlayerID >= player) return  (currPlayerID - player);
    return ((currPlayerID - player) + NUM_PLAYERS);
}


/**********************************************
*   getMoveID
*   returns the game move id
**********************************************/
int getMoveID(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->id;
}


/**********************************************
*   getMovePlayer
*   get a requested moves player object
**********************************************/
Player getMovePlayer(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->player;
}


/**********************************************
*   getMoveLocation
*   get a requested moves location object
**********************************************/
LocationID getMoveLocation(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->location;
}


/**********************************************
*   getMoveType
*   get a requested moves original locationID (not estimated)
*   the estmated location will be stored in mv->location
**********************************************/
LocationID getMoveType(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->locationtype;
}


/**********************************************
*   getMoveTrap
*   get number of traps on a given move
**********************************************/
int getMoveTrap(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->trap;
}


/**********************************************
*   setMoveTrap
*   set number of traps on a given move
**********************************************/
void setMoveTrap(GameMove mv, int trap) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    mv->trap = trap;
}


/**********************************************
*   getMoveVampire
*   get number of immature vampires on a given move
**********************************************/
int getMoveVampire(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->vampire;
}


/**********************************************
*   setMoveVampire
*   set number of immature vampires on a given move
**********************************************/
void setMoveVampire(GameMove mv, int vampire) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    mv->vampire = vampire;
}


/**********************************************
*   getMoveDracula
*   get number of dracula encounters on a given move
**********************************************/
int getMoveDracula(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->dracula;
}


/**********************************************
*   setMoveDracula
*   set number of dracula encounters on a given move
**********************************************/
void setMoveDracula(GameMove mv, int dracula) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    mv->dracula = dracula;
}


/**********************************************
*   getMoveMalfunction
*   get number of malfunctions on a given move
**********************************************/
int getMoveMalfunction(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->malfunction;
}


/**********************************************
*   setMoveMalfunction
*   set number of malfunctions on a given move
**********************************************/
void setMoveMalfunction(GameMove mv, int malfunction) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    mv->malfunction = malfunction;
}


/**********************************************
*   getMoveMatured
*   get number of vampires that matured on a given move
**********************************************/
int getMoveMatured(GameMove mv) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    return mv->matured;
}


/**********************************************
*   setMoveMatured
*   set number of vampires that matured on a given move
**********************************************/
void setMoveMatured(GameMove mv, int matured) {
    if(mv == NULL) {
        fprintf(stderr, "%s: Null GameMove\n", __func__);
        abort();
    }
    mv->matured = matured;
}


/**********************************************
*   getGameHistory
*   return a games entire history of moves
**********************************************/
GameMove *getGameHistory(GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return gv->gameHistory;
}


/**********************************************
* getPlayerPastMove
* Get the past move for any given player at requested Round
**********************************************/
GameMove getPlayerPastMove (GameView gv, PlayerID playerID, int pastRound) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }

    PlayerID currPlayerID = getCurrentPlayer(gv);

    int playerDifference = currPlayerID - playerID;
    if(playerDifference <= 0) { //if its zero its an edge case
        pastRound++;
    }

    int timePassed =  ((playerDifference % NUM_PLAYERS) + (NUM_PLAYERS * pastRound));
    if(timePassed < 0) {
        return NULL;
    }

    int pastMoveID = getCurrMove(gv) - timePassed;
    if(!isValidMove(gv, pastMoveID)) return NULL;

    return getMove(gv, pastMoveID);
}


Map getGameMap(GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return gv->gameWorld;
}


/**********************************************
*   getCurrMove
*   return the current move a game is upto
**********************************************/
int getCurrMove(GameView gv) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    return gv->numMoves;
}


/*
 * getTrapsFromGV
 * return number of traps at a location on the gameWorld map
 */
int getTrapsFromGV(GameView gv, LocationID where) {
    return getTrapsFromMap(gv->gameWorld, where);
}


/*
 * getVampsFromGV
 * return number of vampires at a location on the gameWorld map
 */
int getVampsFromGV(GameView gv, LocationID where) {
    return getVampsFromMap(gv->gameWorld, where);
}




/**********************************************
* getPastLocation
* Get the past location id for any given player at requested Round
**********************************************/
LocationID getPastLocation (GameView gv, PlayerID playerID, int pastRound) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }

    if(pastRound == 0) return getPlayerLocationType(getPlayer(gv, playerID));

    //get the past move from our games history
    GameMove move = getPlayerPastMove (gv, playerID, pastRound);
    if(move == NULL) return UNKNOWN_LOCATION;

    //get the player from that move
    Player pastPlayer = getMovePlayer(move);

    LocationID pastLocationID = UNKNOWN_LOCATION;
    pastLocationID = getSmartPlayerLocation(pastPlayer);
    return pastLocationID;
}


/**********************************************
* getPastMoveLocation
* Get the past moves location id for any given player at requested Round
**********************************************/
LocationID getPastMoveLocation (GameView gv, PlayerID playerID, int pastRound) {
    if(gv == NULL) {
        fprintf(stderr, "%s: Null GameView\n", __func__);
        abort();
    }
    //get the past move from our games history
    GameMove move = getPlayerPastMove (gv, playerID, pastRound);
    if(move == NULL) return UNKNOWN_LOCATION;

    return getMoveLocation(move);
}


/*
 * whereCanDraculaGoUpdate
 * Takes into consideration all restrictions on Draculas movement
 */
LocationID * whereCanDraculaGoUpdate(GameView gv, int *numLocations,
    LocationID *places, PlayerID player, bool road, bool sea) {

    //only dracula has connection updates
    if(player != PLAYER_DRACULA) {
        return places;
    }

    // According to The View game specs, Dracula can "go" to any connected location
    // not in his trail, excepting he can go to his current location in the trail.
    // This is only for the first iteration of the assignment and is not valid for The Hunt
    // which requires testing for DoubleBacks and Hides!

    LocationID currLoc = getPastLocation(gv, player, 0);
    LocationID *trail = getTrail(gv, player);
    LocationID *newLocations = malloc(sizeof(LocationID));
    newLocations[0] = UNKNOWN_LOCATION;
    int newSize = 0;

    //go through all locations and copy over any NOT in our trail
    for(int i = 0; i<*numLocations; i++){
        bool inTrail = false;
        for(int k = 0; !inTrail && k<(TRAIL_SIZE-1); k++){
            if(places[i]==trail[k] && trail[k]!=currLoc){
                inTrail = true;
            }
        }
        if(!inTrail){
            newLocations = realloc(newLocations,sizeof(LocationID)*(newSize+1));
            newLocations[newSize] = places[i];
            newSize++;
        }
    }
    free(places);
    *numLocations = newSize;
    return newLocations;

}
