////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Map.h: an interface to a Map data type
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#ifndef MAP_H_
#define MAP_H_

#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "Custom.h"
#include "Player.h"


typedef struct map *Map;
typedef struct location *Location;

typedef struct edge {
    LocationID start;
    LocationID end;
    TransportID type;
} edge;


#define NUM_GAME_LOCATIONS (NUM_MAP_LOCATIONS + 1)
#define UNKNOWN ((int)NUM_MAP_LOCATIONS)

#define isDoubleBack(locID) (locID >=DOUBLE_BACK_1 && locID <= DOUBLE_BACK_5)
#define isTP(locID) (locID == TELEPORT)
#define isHide(locID) (locID == HIDE)
#define isUnknown(locID) ((locID >= CITY_UNKNOWN && locID <= SEA_UNKNOWN) || \
    (locID >= UNKNOWN_LOCATION))
#define isUnknownCity(locID)(locID == CITY_UNKNOWN)
#define isCity(locID) (isUnknownCity(locID)||!isSea(locID))
#define isUnknownSea(locID) (locID == SEA_UNKNOWN)
#define validLocation(pid) ((validPlace(pid)) || (pid == UNKNOWN))

// operations on graphs
Map newMap (void);
void disposeMap (Map);
void showMap (Map);
int numV (Map);
int numE (Map, TransportID);

//operations on map locations
Map getGameMap(GameView gv);
Location getMapLocation(Map map, LocationID location);
Player getLocationPlayer(Location location, PlayerID player);
int getTrapsFromMap(Map map, LocationID where);
int getVampsFromMap(Map map, LocationID where);
void setMapTrap(Map map, LocationID where, int traps);
void setMapVamp(Map map, LocationID where, int traps);
bool atPort(Map g, LocationID locID);
bool atSea(LocationID locID);

//game play operations
void playMove(GameView gv, Map map, GameMove move, GameMove *history);
void movePlayer(GameMove move, Map map, Player player, LocationID newLocID,
    LocationID newLocTypeID);
void updateMoveHealth(GameView gv,  Map map, Player player, GameMove move);
int moveBackDistance(LocationID location);

//other
LocationID unknownAbvToID (char *abbrev); //move to places.h for part 2 of assignment
LocationID *updateConnectedLocations(Map map, int *numLocations,
    LocationID from, PlayerID player, Round round,
    bool road, bool rail, bool sea);



#endif // !defined(MAP_H_)
