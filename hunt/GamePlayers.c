
////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Players.h: an interface to the Player
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <errno.h>
#include <error.h>
#include <stdio.h>

#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "Map.h"

#include "GamePlayers.h"


struct player{
    PlayerID id;
    LocationID location;
    LocationID locationtype;
    Health health;
};


/*
*    createPlayer
*    create a new player struct object
*/
Player
createPlayer(int id){
    Player p = malloc (sizeof(struct player));
    if (p == NULL){
        error(errno,errno,"%s: Error creating Player\n",__func__);
        abort();
    }
    p->id = id;
    p->location = -1;
    p->locationtype = -1;
    p->health = -1;
    return p;
}


/*
 * getPlayerHealth
 * return health of current player object
 */
Health getPlayerHealth(Player player){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    return player->health;
}


/*
 * setPlayerHealth
 * set health of current player object
 */
void setPlayerHealth(Player player, Health health){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    player->health = health;
}


/*
 * getPlayerLocationType
 * get location type of current player object
 */
LocationID getPlayerLocationType(Player player){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    return player->locationtype;
}


/*
 * getSmartPlayerLocation
 * if possible get the determined location of current player object
 */
LocationID getSmartPlayerLocation(Player player){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    //if player location and type dont match, check if we have calculated their location
    //if not, then return the default type of location (IE UNKNOWN_SEA)
    if(!validLocation(player->locationtype)){
        if(player->location == UNKNOWN){
            return player->locationtype;
        }
    }
    return player->location;
}


/*
 * getPlayerLocation
 * get location of current player object
 */
LocationID getPlayerLocation(Player player){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    return player->location;
}


/*
 * getPlayerID
 * get player object id
 */
PlayerID getPlayerID(Player player){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    return player->id;
}


/*
 * setPlayerID
 * set player object id
 */
void setPlayerID(Player player, PlayerID id){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    player->id = id;
}


/*
 * setPlayerLocation
 * set player location
 */
void setPlayerLocation(Player player, LocationID id){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    player->location = id;
}


/*
 * setPlayerLocationType
 * set player location type
 */
void setPlayerLocationType(Player player, LocationID id){
    if(player == NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    player->locationtype = id;
}


/*
 * idToPlayerName
 * convert player ID to string
 */
char * idToPlayerName(PlayerID player){
    switch (player) {
        case PLAYER_LORD_GODALMING: return "Lord Godalming";
        case PLAYER_DR_SEWARD: return "Dr Seward";
        case PLAYER_VAN_HELSING: return "Van Helsing";
        case PLAYER_MINA_HARKER: return "Mina Harker";
        case PLAYER_DRACULA: return "Count Dracula";
        default: return "Unknown";
    }
}


/*
 * isEnemy
 * are two players enemies
 */
bool isEnemy(Player p1, Player p2){
    if(p1 == NULL || p2 == NULL){
        return true;
    }
    return (isHunter(p1) != isHunter(p2));
}


/*
 * isHunter
 * is player a hunter
 */
bool isHunter(Player player){
    if(player == NULL){
        return false;
    }
    return (getPlayerID(player) != PLAYER_DRACULA);
}
