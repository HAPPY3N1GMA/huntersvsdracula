////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// DracView.c: the DracView ADT implementation
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "DracView.h"
#include "Game.h"
#include "GameView.h"
#include "Globals.h"

#include "GamePlayers.h"
#include "Map.h"

struct dracView {
    GameView gv;
};

static LocationID dracLocToMoveType(DracView dv, LocationID location);

/*
 * newDracView
 * Creates a new DracView to summarise the current state of the game
 */
DracView newDracView (char *pastPlays, PlayerMessage messages[]) {
    DracView new = malloc (sizeof *new);
    if (new == NULL) {
        fprintf(stderr, "%s: Couldn't allocate Dracview\n", __func__);
        abort();
    }
    new->gv = newGameView(pastPlays, messages);
    return new;
}



/*
 * disposeDracView
 * Frees all memory previously allocated for the DracView toBeDeleted
 */
void disposeDracView (DracView toBeDeleted) {
    if(toBeDeleted == NULL) {
        return;
    }
    disposeGameView(toBeDeleted->gv);
    free (toBeDeleted);
}



/*
 * giveMeTheRound
 * Get the current round
 */
Round giveMeTheRound (DracView dv) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return getRound(dv->gv);
}



/*
 * giveMeTheScore
 * Get the current score
 */
int giveMeTheScore (DracView dv) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return getScore(dv->gv);
}



/*
 * howHealthyIs
 * Get the current health points for a given player
 */
int howHealthyIs (DracView dv, PlayerID player) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    return getHealth(dv->gv, player);
}



/*
 * whereIs
 * Get the current location of a given player
 */
LocationID whereIs (DracView dv, PlayerID player) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    return getPastLocation(dv->gv, player, 0);
}



/*
 * lastMove
 * Get the most recent move of a given player
 */
void lastMove (DracView dv, PlayerID player, LocationID *start, LocationID *end) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    LocationID *trail = getTrail(dv->gv, player);
    *start = trail[1];
    *end = trail[0];
}



/*
 * whatsThere
 * Find out what minions are placed at a specified location
 */
void whatsThere (DracView dv, LocationID where, int *numTraps, int *numVamps) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!validPlace(where)){
        fprintf(stderr, "%s: Invalid Location Id\n", __func__);
        abort();
    }
    *numTraps = getTrapsFromGV(dv->gv, where);
    *numVamps = getVampsFromGV(dv->gv, where);
}



/*
 * giveMeTheTrail
 * Fills the trail array with the location ids of the last 6 turns
 */
void giveMeTheTrail (DracView dv, PlayerID player,
    LocationID trail[TRAIL_SIZE])
{
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    LocationID *t = getTrail(dv->gv, player);
    for(int i = 0; i<TRAIL_SIZE; i++){
        int moveDist = moveBackDistance(t[i]);
        //if its an invalid location, try and resolve it (Hides, Backtracks, TP)
        if(!validPlace(t[i])){
            trail[i] = getPastLocation(dv->gv,player, moveDist+i);
        }else{
            trail[i] = t[i];
        }
    }
}



/*
 * whereCanIgo
 * What are my (Dracula's) possible next moves (locations)
 */
LocationID *whereCanIgo (DracView dv, int *numLocations, bool road, bool sea) {
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return whereCanTheyGo(dv, numLocations, PLAYER_DRACULA, road, false, sea);
}




/*
 * whereCanTheyGo
 * What are the specified player's next possible moves
 */
LocationID * whereCanTheyGo (DracView dv, int *numLocations, PlayerID player,
    bool road, bool rail, bool sea)
{
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    if(!isPlayer(player)){
        fprintf(stderr, "%s: Invalid Player Id\n", __func__);
        abort();
    }
    //if players turn is before ours, then their turn is the next round
    int currRound = getRound(dv->gv);
    if(player< getCurrentPlayer(dv->gv)){
        currRound++;
    }

    //where can we get too from our current location?
    LocationID currLoc = getPastLocation(dv->gv, player, 0);
    LocationID *where = connectedLocations (dv->gv, numLocations,
        currLoc,player, currRound,
        road, rail, sea);

    if(player!=PLAYER_DRACULA){
        return where;
    }

    return whereCanDraculaGoUpdate(dv->gv, numLocations, where, player, road, sea);
}



/*
 * locationToMoveType
 * checks if a location is in our trail, and converts to correct doubleBack / Hide
 */
static LocationID dracLocToMoveType(DracView dv, LocationID location){
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }

    if(!validPlace(location)) return location;

    bool hide = true;
    bool doubleBack = true;

    LocationID *trail = getTrail(dv->gv, PLAYER_DRACULA);

    //check we have any hides or backtracks in our trail
    for(int i = 0; i<(TRAIL_SIZE - 1); i++){
        if(trail[i]==HIDE){
            hide = false;
        }else if(moveBackDistance(trail[i]) != 0){
            doubleBack = false;
        }
    }

    //we already have hides and double backs, assumes location is already permitted
    if(!hide && !doubleBack) return location;

    if(hide && !isSea(location)){ //not permitted to hide at sea
        if(whereIs(dv,PLAYER_DRACULA)==location) return HIDE;
    }

    LocationID resolvedTrail[TRAIL_SIZE];
    giveMeTheTrail (dv, PLAYER_DRACULA, resolvedTrail);

    //if its an invalid trail location, check if its the same real world location
    for(int i = 0; i < TRAIL_SIZE; i++){
        //go from furthest back in trail to closest (incase it would be a db5 not db1)
        if(resolvedTrail[i] == location){
            //its a match!
            //if we already have a double back, it has to be a hide
            if(doubleBack){
                LocationID type = distanceToDoubleBack(i);
                if(type == UNKNOWN){
                    return location; //we are looking beyond our trail
                }else{
                    return type;
                }
            }
        }
    }
    return location;
}



/*
 * locToPlay
 * converts location to correct abbreviation
 */
char * locToPlay(DracView dv, LocationID location){
    if(dv == NULL) {
        fprintf(stderr, "%s: Null DracView\n", __func__);
        abort();
    }
    return idToAbbrev(dracLocToMoveType(dv, location));
}



/*
 * getConnections
 * get list of all possible connections to godalming
 */
LocationID *getConnections (DracView dv, LocationID from, int *size) {

    return connectedLocations(dv->gv, size, from, PLAYER_LORD_GODALMING,
        1, true, true, true);

}



/*
 * getCastlePath
 * get path to castle
 */
LocationID getCastlePath (DracView dv, LocationID where) {
    Map map = getGameMap(dv->gv);
    int pathLength = 0;
    bool prohibited[NUM_MAP_LOCATIONS] = {false};
    prohibited[ST_JOSEPH_AND_ST_MARYS] = true;
    bool transportTypes[MAX_TRANSPORT] = {true, false, true};
    LocationID *path = findShortestPermittedPath(map, where, CASTLE_DRACULA, prohibited, transportTypes, &pathLength);
    LocationID G = getLocation(dv->gv, PLAYER_LORD_GODALMING);
    LocationID S = getLocation(dv->gv, PLAYER_DR_SEWARD);
    LocationID H = getLocation(dv->gv, PLAYER_VAN_HELSING);
    LocationID M = getLocation(dv->gv, PLAYER_MINA_HARKER);
    for(int i = 0; i < NUM_HUNTERS; i++) {
        if(path[1] == getLocation(dv->gv, i) || isConnected(map, path[1], getLocation(dv->gv, i))) {
            if(isConnected(map, where, path[2]) && !isConnected(map, path[2], getLocation(dv->gv, i))) {
                LocationID loc = path[2];
                free(path);
                return loc;
            } else {
                path[1] = findOtherPath(map, where, path[2], path[1], G, S, H, M);
            }
        }
    }
    LocationID loc = path[1];
    free(path);
    return loc;
}


/*
 * getPath
 * get path to location
 */
LocationID *getPath(DracView dv, LocationID start, LocationID end, int *pathLength) {
    //return getPathGV(dv->gv, start, end, pathLength);
    bool prohibited[NUM_MAP_LOCATIONS] = {false};
    prohibited[ST_JOSEPH_AND_ST_MARYS] = true;
    bool transportTypes[MAX_TRANSPORT] = {true, false, true};
    return findShortestPermittedPath(getGameMap(dv->gv), start, end, prohibited, transportTypes, pathLength);
}


/*
 * getMessages
 * get gameview messages
 */
PlayerMessage *getMessages(DracView dv, int *numMess) {
    return getGVMessages(dv->gv, numMess);
}
