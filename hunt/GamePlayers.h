
////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// GamePlayers.h: an interface to the Player
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Places.h"
#include <stdbool.h>

#define isPlayer(player) ((player>=0) && (player < NUM_PLAYERS))

typedef int Health;
typedef struct player *Player;

Health getPlayerHealth(Player player);
LocationID getSmartPlayerLocation(Player player);
LocationID getPlayerLocationType(Player player);
LocationID getPlayerLocation(Player player);
PlayerID getPlayerID(Player player);
Player createPlayer(int id);
void setPlayerID(Player player, PlayerID id);
void setPlayerLocation(Player player, LocationID id);
void setPlayerLocationType(Player player, LocationID id);
void setPlayerHealth(Player player, Health health);
bool isEnemy(Player p1, Player p2);
bool isHunter(Player player);
char * idToPlayerName (PlayerID player);


#endif // !defined (PLAYER_H_)
