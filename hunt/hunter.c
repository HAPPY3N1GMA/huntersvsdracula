////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// hunter.c: your "Fury of Dracula" hunter AI.
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "Game.h"
#include "Globals.h"
#include "GamePlayers.h"
#include "Map.h"
#include "HunterView.h"
#include "hunter.h"

void decideHunterMove (HunterView hv)
{

    LocationID startLocations[] = {DUBLIN,LISBON,ZAGREB,CASTLE_DRACULA};
    PlayerID currPlayer = whoAmI(hv);
    Round currRound = giveMeTheRound(hv);
    LocationID currPosition = whereIs (hv,currPlayer);
    LocationID dracLoc = whereIs (hv,PLAYER_DRACULA);

    char *startingQuotes[] = {"We're gonna be gods.","Of course we are.","I'm gonna be naughty.","I'm too old for this!"};
    char *huntingQuotes[] = {"So what do we use? Stakes? Crosses?","Crosses don't do squat.","I've got hollow-points filled with garlic!","Kill him!"};


    /////////////////////////////////////
    //if first round, choose locations from array
    /////////////////////////////////////
    if(currRound==0){
        int numLocations = 0;
        LocationID *where = getConnectedLocations(hv, &numLocations, currPlayer,
            currRound, startLocations[currPlayer], true, true, false);
        unsigned int seed = time(NULL) + currPlayer + numLocations;
        int move = rand_r(&seed) % numLocations;
        registerBestPlay (idToAbbrev(where[move]), startingQuotes[currPlayer]);
        free(where);
        return;
    }


    /////////////////////////////////////
    //lets hunt for dracula!
    /////////////////////////////////////
    //list of places that dracula is not at! do not search those spots
    bool prohibitedLocations[NUM_MAP_LOCATIONS];

    //if set to a location, this is where drac is
    LocationID chosenDest = UNKNOWN_LOCATION;

    if(letsHunt(hv, prohibitedLocations, &chosenDest)){
        //we need to find best path to this location
        chosenDest = findBestMove(hv, chosenDest);
        registerBestPlay (idToAbbrev(chosenDest),huntingQuotes[currPlayer]);
        return;
    }


    /////////////////////////////////////
    //lets reveal dracula!
    /////////////////////////////////////
    if((currRound == 6) || (dracLoc == DOUBLE_BACK_5)){
        LocationID history[TRAIL_SIZE];
        giveMeTheTrail(hv, PLAYER_DRACULA, history);
        //only rest if we dont know where dracula is to within a few moves!
        bool knownTrail = false;
        for(int i =0; i<TRAIL_SIZE; i++){
            if(validPlace(history[i])){
                knownTrail = true;
                break;
            }
        }
        if(!knownTrail){
            registerBestPlay (idToAbbrev(currPosition), "I promise you, you will be dead by dawn");
            return;
        }
    }


    /////////////////////////////////////
    //Where Dracula At?
    /////////////////////////////////////
    int numMoves = 0;
	chosenDest = calculateBestDestination(hv,prohibitedLocations);

    //how should we get to this chosenDest?
	//get list of all possible legal moves (preferrably by road)
	LocationID *possibleLocs = whereCanIgo (hv, &numMoves, true, false, true);
	if(possibleLocs==NULL){
	    //only travel by rail if you HAVE TO!
	    free(possibleLocs);
	    possibleLocs = whereCanIgo (hv, &numMoves, true, true, false);
	    if(possibleLocs==NULL){
	        //only travel by sea if you HAVE TO!
	        free(possibleLocs);
	        possibleLocs = whereCanIgo (hv, &numMoves, true, true, true);
	    }
	}

	//i dont want hunters to backtrack via somewhere they just came from
	//so add that to list of prohibited locations to travel via!
	bool prohibited[NUM_MAP_LOCATIONS] = {false};
	for(int i = 0; i<NUM_HUNTERS; i++){
	    LocationID history[TRAIL_SIZE];
	    giveMeTheTrail(hv, i, history);
	    for(int h = 0; h<2; h++){
	        if(validPlace(history[h])) {
	            prohibited[history[h]] = true;
	        }
	    }
	}

	//we now have a list of a few possible next moves to make
	//which is quickest to get to my chosenDest?
	LocationID *path = NULL;
	LocationID bestPosition = UNKNOWN_LOCATION; //last resort stay here

	int shortestLength = INT_MAX;
	int pathLength = 0;
	bool transportTypes[MAX_TRANSPORT] = {true,false,false};

	//check for each of these moves, how long it will take to get to where we want to go
	for(int i = 0; i < numMoves ; i++){
	    //we dont want to travel via rail or sea unles we have too
	    transportTypes[1] = false;
	    transportTypes[2] = false;

	    //if this place is prohibited (IE I just came from there), then dont bother calculating a path
	    if(prohibited[possibleLocs[i]]) continue;

	    //dont prohibit any paths
	    bool prohib[NUM_MAP_LOCATIONS] = {false};

	    //calculate the path
	    path = findShortestPermittedPath(giveMeTheMap(hv),possibleLocs[i], chosenDest, prohibited, transportTypes, &pathLength);

	    //if we cannot travel by road, then order of preference is rail, then sea
	    if(path==NULL){
	        //only travel by rail if you HAVE TO! Add penalty to this path as we are searching not fast travelling!
	        transportTypes[1] = true;
	        path = findShortestPermittedPath(giveMeTheMap(hv),possibleLocs[i], chosenDest, prohib, transportTypes, &pathLength);
	        pathLength += 5;
	        if(path==NULL){
	            //only travel by sea if you HAVE TO! Add penalty to this path as we are searching not fast travelling!
	            transportTypes[2] = true;
	            path = findShortestPermittedPath(giveMeTheMap(hv),possibleLocs[i], chosenDest, prohib, transportTypes, &pathLength);
	            pathLength += 10;
	        }
	    }

	    //store the best path that we find
	    if((path!=NULL) && (pathLength < shortestLength)){
	        bestPosition = path[0];
	        shortestLength = pathLength;
	    }
	    free(path);
	}

	if(validPlace(bestPosition)){
	    chosenDest = bestPosition;
	}

    LocationID lastResort = possibleLocs[0];
    if(numMoves>0){
        lastResort = possibleLocs[numMoves-1];
    }

	free(possibleLocs);
	if(bestPosition == chosenDest && validPlace(bestPosition)){
	    registerBestPlay (idToAbbrev(chosenDest), "Drac baby... are you outta yo damn mind?!?");
	}else{
         //last last resort move to last loc in list of possible moves
         //(ie we only have locations we previously travelled to available as a next move)
	    registerBestPlay (idToAbbrev(lastResort), "You know, my mother used to say: A cold heart is a dead heart.");
	}
	 return;
}
