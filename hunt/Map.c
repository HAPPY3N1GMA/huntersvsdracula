////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Map.c: an implementation of a Map type
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <limits.h>
#include "Queue.h"
//#include "PQueue.h"
#include "Map.h"



typedef struct vNode *VList;
struct vNode {
    LocationID  v;    // ALICANTE, etc
    TransportID type; // ROAD, RAIL, BOAT
    VList next; // link to next node
};

struct location {
    LocationID  v;                                 // ALICANTE, etc
    Player players[NUM_PLAYERS]; //array of players at this location
    int traps;                                          //count of traps at this location
    int vampires;                                   //count of vampires at this location
};

struct map {
    int nV;                                                                       // #vertices
    int nE;                                                                       // #edges
    VList connections[NUM_GAME_LOCATIONS]; // array of lists of graph connections
    Location locations[NUM_GAME_LOCATIONS]; // array of locations
};

static void addConnections(Map);
static void addLocations(Map g);
static int appendLoc(LocationID **locations, LocationID loc, int len, PlayerID player);
static int railAppendLoc(LocationID **locations, Map map, LocationID from, int len,
                                int railAllowance, PlayerID player);





/*
* disposeMap
* Create a new empty graph (for a map)
* #Vertices always same as NUM_PLACES
*/
Map newMap (void)
{
    Map g = malloc (sizeof *g);
    if (g == NULL){
        error(errno,errno,"%s: Couldn't allocate Map\n",__func__);
        abort();
    }
    (*g) = (struct map){
        .nV = NUM_GAME_LOCATIONS,
        .nE = 0,
        .connections = { NULL },
        .locations = { NULL }
    };
    addConnections (g);
    addLocations(g);
    return g;
}


/*
* disposeMap
* Remove an existing graph
*/
void disposeMap (Map g)
{
    // wait, what?
    if (g == NULL) return;

    for (int i = 0; i < g->nV; i++) {
        VList curr = g->connections[i];
        while (curr != NULL) {
            VList next = curr->next;
            free (curr);
            curr = next;
        }
        free(g->locations[i]);
    }
    free (g);
}


/*
* insertVList
* insert new place into list of connections
*/
static VList insertVList (VList L, LocationID v, TransportID type)
{
    VList new = malloc (sizeof(struct vNode));
    if (new == NULL){
        error(errno,errno,"%s: Couldn't allocate vNode\n",__func__);
        abort();
    }
    (*new) = (struct vNode){
        .v = v,
        .type = type,
        .next = L
    };
    return new;
}


/*
* inVList
* is a place in the list of connections
*/
static int inVList (VList L, LocationID v, TransportID type)
{
    for (VList cur = L; cur != NULL; cur = cur->next)
        if (cur->v == v && cur->type == type){
            return 1;
        }
    return 0;
}


/*
* addLink
* Add a new edge to the Map/Graph
*/
static void addLink (Map g, LocationID start, LocationID end, TransportID type)
{
    if (g == NULL){
        error(errno,errno,"%s: Map is Null\n",__func__);
        abort();
    }

    // don't add edges twice
    if (inVList (g->connections[start], end, type)) return;

    g->connections[start] = insertVList(g->connections[start],end,type);
    g->connections[end] = insertVList(g->connections[end],start,type);
    g->nE++;
}


/*
* typeToString
* convert place type (int) to string
*/
static const char * typeToString (TransportID t)
{
    switch (t) {
    case ROAD: return "road";
    case RAIL: return "rail";
    case BOAT: return "boat";
    default:   return "????";
    }
}


/*
* numE
* Display content of Map/Graph
*/
void showMap (Map g)
{
    if (g == NULL){
        error(errno,errno,"%s: Map is Null\n",__func__);
        abort();
    }

    printf ("V=%d, E=%d\n", g->nV, g->nE);
    for (int i = 0; i < g->nV; i++)
        for (VList n = g->connections[i]; n != NULL; n = n->next)
            printf ("%s connects to %s by %s\n",
                idToName (i), idToName (n->v), typeToString (n->type));
}


/*
* numE
* Return count of nodes
*/
int numV (Map g)
{
    if (g == NULL){
        error(errno,errno,"%s: Map is Null\n",__func__);
        abort();
    }
    return g->nV;
}


/*
* numE
* Return count of edges of a particular type
*/
int numE (Map g, TransportID type)
{
    assert (g != NULL);
    assert (0 <= type && type <= ANY);
    int nE = 0;
    for (int i = 0; i < g->nV; i++)
        for (VList n = g->connections[i]; n != NULL; n = n->next)
            if (n->type == type || type == ANY){
                nE++;
            }
    return nE;
}



/*
* addConnections
* Add edges to Graph representing map of Europe
*/
static void addConnections (Map g){
    #define IS_SENTINEL(x) ((x).v == -1 && (x).w == -1 && (x).t == ANY)
    for (int i = 0; ! IS_SENTINEL (CONNECTIONS[i]); i++)
        addLink (g, CONNECTIONS[i].v, CONNECTIONS[i].w, CONNECTIONS[i].t);
}


/*
* addLocations
* build world map locations for storing players, traps, vampires etc
*/
static void addLocations(Map g){
    if(g==NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    //build our locations
    for(int i = 0; i<NUM_GAME_LOCATIONS; i++){
        g->locations[i] = calloc(1,sizeof(struct location));
        if(g->locations[i]==NULL){
            error(errno,errno,"%s: Error Creating Location\n",__func__);
            abort();
        }
        g->locations[i]->v = i;
        memset(g->locations[i]->players,0,sizeof(Player)*NUM_PLAYERS);
        g->locations[i]->traps = 0;
        g->locations[i]->vampires = 0;
    }
}

/*
* moveBackDistance
* returns how many moves back we need to travel
* why is a hide the same as a double back one in the rules?
*/
int moveBackDistance(LocationID location){
        switch (location) {
            case HIDE: return 1;
            case DOUBLE_BACK_1: return 1;
            case DOUBLE_BACK_2: return 2;
            case DOUBLE_BACK_3: return 3;
            case DOUBLE_BACK_4: return 4;
            case DOUBLE_BACK_5: return 5;
            default: return 0;
        }
}


/*
* distanceToDoubleBack
* convert distance to doubleback id
*/
LocationID distanceToDoubleBack(int distance){
//    printf("distance: %d\n",distance);
        switch (distance) {
            case 0: return DOUBLE_BACK_1;
            case 1: return DOUBLE_BACK_2;
            case 2: return DOUBLE_BACK_3;
            case 3: return DOUBLE_BACK_4;
            case 4: return DOUBLE_BACK_5;
            default: return UNKNOWN;
        }
}


/*
* convertLocationType
* Dracula has the ability to see the actual location a double back, hide, tp etc is at
* Hunters only get to see that he made a double back etc
*/
static LocationID convertLocationType(Player player, LocationID newLocID,
    LocationID newLocTypeID){
    if (player==NULL){
        error(errno,errno,"%s: Null Player\n",__func__);
        abort();
    }
    if(isHunter(player)) return newLocTypeID;
    //only drac gets special viewing abilities
    switch(newLocTypeID){
        case TELEPORT : return CASTLE_DRACULA;
        case HIDE : return newLocID;
        case DOUBLE_BACK_1: return newLocID;
        case DOUBLE_BACK_2: return newLocID;
        case DOUBLE_BACK_3: return newLocID;
        case DOUBLE_BACK_4: return newLocID;
        case DOUBLE_BACK_5: return newLocID;
        default: return newLocTypeID; //dont change
    }
}


/*
* unknownAbvToID
* returns the location id of an unknown type of location
* Note: Move this for part 2 of assignment into places.c and places.h
*/
LocationID unknownAbvToID (char *abbrev){
    char *abvList[9] = {"C?","S?","HI","D1","D2","D3","D4","D5","TP"};
    int chk = -1;
    for(int i=0; i<=9; i++ ){
        chk = strncmp(abbrev,abvList[i],2);
        if(chk==0){
            return (LocationID)(CITY_UNKNOWN + i);
        }
    }
    return NOWHERE;
}


/*
* movePlayer
* on the map, move a player from their current position (if they have one) to their new position
*/
void movePlayer(GameMove move, Map map, Player player, LocationID newLocID,
    LocationID newLocTypeID){
    if(map==NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(player == NULL){
        error(errno,errno,"%s: Null player\n",__func__);
        abort();
    }
    PlayerID pid = getPlayerID(player);

    //dracula gets to see his locations as the location (not just unknown_seas etc)
    newLocTypeID = convertLocationType(player, newLocID, newLocTypeID);
    if(newLocTypeID==CASTLE_DRACULA){
        newLocID = newLocTypeID;
    }

    //store unknown seas etc as UNKNOWN locations in array
    if(!validLocation(newLocID)){
        newLocID = UNKNOWN;
    }

    Location newLoc = map->locations[newLocID];
    LocationID currLocID = getPlayerLocation(player);

    //remove player from old map location
    if(validLocation(currLocID)){
        map->locations[currLocID]->players[pid] = NULL;
    }
    //update player struct to show their new location
    newLoc->players[pid] = player;

    setPlayerLocation(player,newLocID);
    setPlayerLocationType(player,newLocTypeID);
}


/*
* playMove
* updates the gameWorld with the current events from a given game move.
*/
void playMove(GameView gv, Map map, GameMove move, GameMove *history){
    if (map == NULL){
        error(errno,errno,"%s: Null map\n",__func__);
        abort();
    }
    if (move == NULL){
        error(errno,errno,"%s: Null move\n",__func__);
        abort();
    }

    LocationID newLocID = getMoveLocation(move);
    LocationID newLocTypeID = newLocID;
    Player player = getMovePlayer(move);
    int moveID = getMoveID(move);
    int gameRound = currRound(moveID);

    //try and determine the players new location from the type of move carried out
    if(gameRound > 0 && !validPlace(newLocID)){
        int backtrack = moveBackDistance(newLocID);
        if(backtrack > 0){
            if(backtrack > gameRound){
                newLocID = UNKNOWN; //dont backtrack off board
            }else{
                newLocID = getPastLocation (gv, getPlayerID(player), backtrack-1);
            }
        }
    }
    movePlayer(move,map,player,newLocID,newLocTypeID);
}


/*
* updateMoveHealth
* updates the players health for the current game move
*/
void updateMoveHealth(GameView gv, Map map, Player player, GameMove move){
    if (gv == NULL){
        error(errno,errno,"%s: Null map\n",__func__);
        abort();
    }
    if (player == NULL){
        error(errno,errno,"%s: Null player\n",__func__);
        abort();
    }
    if(move == NULL){
        error(errno,errno,"%s: Null move\n",__func__);
        abort();
    }

    //if player health is 0 or less, then they have just been teleported to hospital/castle,
    //and need to receive health
        if (getPlayerHealth(player) <= 0){
            if(isHunter(player)) {
                //player should already be in hospital
                setPlayerHealth(player,GAME_START_HUNTER_LIFE_POINTS);
            }
        }

    Health playerHealth = getPlayerHealth(player);

    int traps = getMoveTrap(move);
    int vampire = getMoveVampire(move);
    int dracula = getMoveDracula(move);

    bool done = false;
    while(!done){
        if(isHunter(player)){
            // Hunter Health
            if(traps){
                // A Hunter loses 2 life points if they encounter a trap
                playerHealth -= LIFE_LOSS_TRAP_ENCOUNTER;
                setMoveTrap(move,--traps);
                setMapTrap(map, getMoveLocation(move), -1);
            }else if(vampire){
                // kill the immature vampire
                setMoveVampire(move,--vampire);
                setMapVamp(map, getMoveLocation(move), -1);
            }else if(dracula){
                // A Hunter loses 4 life points if they encounter Dracula
                playerHealth -= LIFE_LOSS_DRACULA_ENCOUNTER;
                setMoveDracula(move,--dracula);
                // But, a Hunter also damages Dracula 10 blood points
                Player dracula = getPlayer(gv,PLAYER_DRACULA);
                Health draculaHealth = getPlayerHealth(dracula)-LIFE_LOSS_HUNTER_ENCOUNTER;
                setPlayerHealth(dracula,draculaHealth);
            }else{
                // A Hunter gains 3 life points each time they rest in a city
                //(if their previous move was in the same city or sea)
                LocationID currLocID = getMoveLocation(move);
                if(currRound(getMoveID(move))>0/* && isLand(currLocID)*/){ //rules changed on forums
                    if (getMoveLocation(getPastMove(gv, move, 1)) == currLocID){
                        playerHealth += LIFE_GAIN_REST;
                    }
                }
                done = true;
            }
            if(playerHealth>GAME_START_HUNTER_LIFE_POINTS) {
                playerHealth = GAME_START_HUNTER_LIFE_POINTS;
            }
            setPlayerHealth(player,playerHealth);

            //teleport to hospital if required at any stage
            if(playerHealth<=0) {
                setPlayerHealth(player,0); //to pass ==0 health checks
                movePlayer(move,map,player,ST_JOSEPH_AND_ST_MARYS,
                    ST_JOSEPH_AND_ST_MARYS);
                done = true;
            }
        }else{
            // Dracula Health
            LocationID draculaLoc = getSmartPlayerLocation(player);
            //get the past move location that was stored if we dont have a known location (fixes unknown seas etc)
            if(!validPlace(draculaLoc)){
                int backtrack = moveBackDistance(draculaLoc);
                if(backtrack){
                    draculaLoc = getMoveLocation(getPastMove(gv, move, backtrack));
                }
            }
            // Dracula loses 2 blood points each time he is at sea at the end of his turn
            if(atSea(draculaLoc)){
                playerHealth -= LIFE_LOSS_SEA;
            }

            //  Dracula dies if his health reaches zero
            // Dracula is permitted to exceed 40 blood points.
            // Dracula regains 10 blood points if he is in Castle Dracula
            // at the end of his turn and has not yet been defeated
            if(draculaLoc==CASTLE_DRACULA){
                playerHealth += LIFE_GAIN_CASTLE_DRACULA;
            }
            setPlayerHealth(player,playerHealth);
            done = true;
        }
    }
}


/*
* updateConnectedLocations
* implementation of connectedLocations using the VList struct members
* global variable pre used to keep track of visited and unvisited locations
*/
int pre[NUM_MAP_LOCATIONS];

LocationID *updateConnectedLocations(Map map, int *numLocations,
    LocationID from, PlayerID player, Round round,
    bool road, bool rail, bool sea) {
        if (map == NULL){
            error(errno,errno,"%s: Null map\n",__func__);
            abort();
        }

        if(!validPlace(from)) {
            int size = 0;
            LocationID *locs = malloc(sizeof(LocationID));
            if (locs == NULL){
                error(errno,errno,"%s: Out of Memory\n",__func__);
                abort();
            }
            for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
                locs = realloc(locs, sizeof(LocationID) * (size + 1));
                if (locs == NULL){
                    error(errno,errno,"%s: Out of Memory\n",__func__);
                    abort();
                }
                locs[size] = i;
                size++;
            }
            *numLocations = size;
            return locs;
        }

        memset(pre, NOT_SEEN, NUM_MAP_LOCATIONS * sizeof(int));
        pre[from] = SEEN;

        int railAllowance = ((int)round + (int)player) % 4;
        LocationID *locations = malloc(sizeof(LocationID));
        if (locations == NULL){
            error(errno,errno,"%s: Out of Memory\n",__func__);
            abort();
        }
        locations[0] = from;
        int len = 1;
        if(rail && player != PLAYER_DRACULA) {
            len = railAppendLoc(&locations, map, from, len, railAllowance, player);
        }
        VList curr = map->connections[from];
        while(curr != NULL) {
            switch (curr->type) {
                case ROAD: {
                    if(road) len = appendLoc(&locations, curr->v, len, player);
                } break;
                case RAIL: break;
                case BOAT: {
                    if(sea) len = appendLoc(&locations, curr->v, len, player);
                } break;
                case NONE: break;
                case ANY: break;
            }
            curr = curr->next;
        }
        *numLocations = len;
        return locations;
}


/*
* appendLoc
* append a location to an array of LocationIDs, given satisfied conditions
*   and return the length of the array
*/
static int appendLoc(LocationID **locations, LocationID loc, int len, PlayerID player) {
    if (!validPlace(loc)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    if (!isPlayer(player)){
        error(errno,errno,"%s: Invalid Player ID\n",__func__);
        abort();
    }
    if(player == PLAYER_DRACULA) {
        if(loc == ST_JOSEPH_AND_ST_MARYS) return len;
    }
    if(pre[loc] == SEEN) return len;
    *locations = realloc(*locations, sizeof(LocationID) * (len + 1));
    if (*locations == NULL){
        error(errno,errno,"%s: Out of Memory\n",__func__);
        abort();
    }
    (*locations)[len] = loc;
    return ++len;
}


/*
* railAppendLoc
* append all valid rail locations from "from" to an array of LocationIDs,
*  given satisfied conditions and return the length of the array
*/
static int railAppendLoc(LocationID **locations, Map map, LocationID from, int len,
        int railAllowance, PlayerID player) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if (!isPlayer(player)){
        error(errno,errno,"%s: Invalid Player ID\n",__func__);
        abort();
    }
    bool transportTypes[MAX_TRANSPORT] = {false,true,false};
    transportDFS(map, from,transportTypes, railAllowance);
    for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
        if(pre[i] == SEEN && i != from) {
            *locations = realloc(*locations, sizeof(LocationID) * (len + 1));
            if (*locations == NULL){
                error(errno,errno,"%s: Out of Memory\n",__func__);
                abort();
            }
            (*locations)[len] = i;
            len++;
        }
    }
    return len;
}


/*
* transportDFS
* Capped Locate TransportType Depth First Search
*/
void transportDFS (Map map, LocationID from, bool transportTypes[MAX_TRANSPORT], int hopsAllowance) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(hopsAllowance == 0 || !validPlace(from)) return;
    VList curr = map->connections[from];
    while(curr!= NULL) {
        if (validTransport(curr->type) && transportTypes[curr->type - 1] && pre[curr->v] == NOT_SEEN) {
            pre[curr->v] = SEEN;
            transportDFS(map, curr->v, transportTypes, hopsAllowance - 1);
        }
        curr = curr->next;
    }
}




/*
* mapDFS
* find all locations with x distance from given location
*/
void mapDFS (Map map, LocationID pre2[NUM_MAP_LOCATIONS], LocationID from,
    bool transportTypes[MAX_TRANSPORT], int hopsAllowance) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(hopsAllowance == 0 || !validPlace(from)) return;
    VList curr = map->connections[from];
    while(curr!= NULL) {
        if (validTransport(curr->type) && transportTypes[curr->type-1] && pre2[curr->v] == NOT_SEEN) {
            pre2[curr->v] = SEEN;
            mapDFS(map, pre2, curr->v, transportTypes, hopsAllowance - 1);
        }
        curr = curr->next;
    }
}


/*
*  findShortestPath
*  build list of shortest path from x to y
*  returns length of path
*/
LocationID *findShortestPermittedPath(Map map, LocationID start, LocationID end,
    bool prohibited[NUM_MAP_LOCATIONS], bool transportTypes[MAX_TRANSPORT], int *pathLength)
{
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(!validPlace(start) || !validPlace(end)){
        return NULL;
    }

	Queue q = newQueue();
    Item item = newItem(end, 0);
	QueueJoin(q,item);

    LocationID st[NUM_MAP_LOCATIONS];
    memset(pre,-1,sizeof(LocationID)*NUM_MAP_LOCATIONS);
	memset(st,-1,sizeof(LocationID)*NUM_MAP_LOCATIONS);

	//initialize our starting point
    int count = 0;
	pre[end] = count++;
	st[end] = end; //how did you get to this location?
	bool found = false;

	//BFS to find least number of "hops"
    while(!QueueIsEmpty(q) && !found){
        item = QueueLeave(q);
        LocationID curr = getValue(item);
        free(item);
    		//now look at all its children
        for (VList n = map->connections[curr]; n != NULL; n = n->next){
                //check not visited, valid transport connection && not a prohibited location ->need way to islands so hard coded english channel
                if(pre[n->v] == -1 && !prohibited[n->v]){
                    //hard code "bridges" across the english channel and irish sea (even if we only want land paths predominantly)
                    if((validTransport(n->type) && transportTypes[n->type-1]) || (((isLand(curr) && n->v==ENGLISH_CHANNEL) ||
                        (curr==ENGLISH_CHANNEL && isLand(n->v)))) || (((isLand(curr) && n->v==IRISH_SEA) ||
                            (curr==IRISH_SEA && isLand(n->v))))){
                        QueueJoin(q,newItem(n->v, 0));
                        pre[n->v] = count++; //when did we get here?
                        st[n->v] = curr; //we got here from curr
                        if(n->v == end){
                            found = true;
                        }
                }
            }
        }
    }
	dropQueue(q);
	//we couldnt get to our destination!
	if(st[start] == -1){
        return NULL;
    }

	LocationID curr = start;
	LocationID list[NUM_MAP_LOCATIONS] = {UNKNOWN_LOCATION};
	int index = 0;

	//Build a list of places we travelled via (in correct order)
	while(curr!=end){
		list[index++] = curr; //store into new list in reverse order
		curr = st[curr];
	}
	list[index++] = end;

    LocationID *path = malloc(sizeof(LocationID)*(index+1));
    if (path == NULL){
        error(errno,errno,"%s: Out of Memory\n",__func__);
        abort();
    }
    //copy path
    for(int i = 0; i<index; i++){
        path[i] = list[i];
    }

    *pathLength = index;
    return path;
}


/*
* prohibitConnections
*  sets any locations in prohibited list to true, that do not meet the minconnections (to land or sea)
*/
void prohibitConnections(Map map, bool prohibited[NUM_MAP_LOCATIONS],int minConnections,bool land,bool sea){
    if(minConnections < 1){
        return;
    }
    if(map==NULL){
        return;
    }
    for(int i = 0; i<NUM_MAP_LOCATIONS; i++){
        VList loc = map->connections[i];
        int num = 0;
        while(loc!=NULL){
            if((land && isLand(loc->v))||(sea && isSea(loc->v))){
                num++;
            }
            loc = loc->next;
        }
        if(validPlace(i) && (num < minConnections)){
            prohibited[i] = true;
        }
    }
}



/*
* atSea
*   returns true if a location is at sea (not just if it is a specific sea)
*/
bool
atSea(LocationID locID){
    if (validPlace(locID)){
        return isSea(locID);
    }else{
        return (SEA_UNKNOWN == locID);
    }
}


/*
* atPort
*   returns true if a location is a port city
*/
bool
atPort(Map g, LocationID locID){
    //a sea is not a port (but would return true if not checked for)
    if (validPlace(locID) && !isSea(locID)){
        //does it have a sea connection?
        for (VList n = g->connections[locID]; n != NULL; n = n->next)
            if (idToType (n->v) == SEA) return true;
    }
    return false;
}


/*
* getMapLocation
* returns a location object from a given location id
*/
Location
getMapLocation(Map map, LocationID location){
    if (map == NULL){
        error(errno,errno,"%s: Null map\n",__func__);
        abort();
    }
    if(!validLocation(location)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    return map->locations[location];
}


/*
* getLocationPlayer
* returns a player object from a given location
*/
Player
getLocationPlayer(Location location, PlayerID player){
    if (location == NULL){
        error(errno,errno,"%s: Null location\n",__func__);
        abort();
    }
    if(!isPlayer(player)){
        error(errno,errno,"%s: Invalid PlayerID\n",__func__);
        abort();
    }
    return location->players[player];
}


/*
 * getTrapsFromMap
 * return number of traps at a location on the gameWorld map
 */
int getTrapsFromMap(Map map, LocationID where) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(!validLocation(where)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    //comment out line to return unknown locations traps totals
    if(!validPlace(where)) return 0;
    return map->locations[where]->traps;
}


/*
 * getVampsFromMap
 * return number of vampires at a location on the gameWorld map
 */
int getVampsFromMap(Map map, LocationID where) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(!validLocation(where)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    //comment out line to return unknown locations vampires totals
    if(!validPlace(where)) return 0;
    return map->locations[where]->vampires;
}


/*
 * setMapTrap
 * set number of traps at a location on the gameWorld map
 */
void setMapTrap(Map map, LocationID where, int traps) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(!validLocation(where)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    map->locations[where]->traps += traps;
}


/*
 * setMapVamp
 * set number of vampires at a location on the gameWorld map
 */
void setMapVamp(Map map, LocationID where, int vamps) {
    if (map == NULL){
        error(errno,errno,"%s: Null Map\n",__func__);
        abort();
    }
    if(!validLocation(where)){
        error(errno,errno,"%s: Invalid Location\n",__func__);
        abort();
    }
    map->locations[where]->vampires += vamps;
}



/*
 * isConnected
 * is location A directly connected to location B
 */
bool isConnected(Map map, LocationID loc1, LocationID loc2) {
    if(!validPlace(loc1) || !validPlace(loc2)) return false;
    VList curr = map->connections[loc1];
    while(curr != NULL) {
        if(curr->v == loc2) return true;
        curr = curr->next;
    }
    return false;
}


/*
 * findOtherPath
 * find a path for dracula that avoids hunters
 */
LocationID findOtherPath(Map map, LocationID start, LocationID dest, LocationID excluded,
    LocationID G, LocationID S, LocationID H, LocationID M) {
    VList curr = map->connections[start];
    VList conn;
    bool use = false;
    bool dontUse = false;
    while(curr != NULL) {
        if(curr->v != excluded && (curr->v == G || curr->v == S || curr->v == H || curr->v == M)) {
            conn = map->connections[curr->v];
            while(conn != NULL) {
                if(conn->v == dest) {
                    use = true;
                }
                if(conn->v == G || conn->v == S || conn->v == H || conn->v == M) {
                    dontUse = true;
                }
                conn = conn->next;
            }
            if(use && !dontUse) return curr->v;
        }
        curr = curr->next;
    }
    return start;
}
