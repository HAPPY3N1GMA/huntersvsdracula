////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// Map.h: an interface to a Map data type
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#ifndef MAP_H_
#define MAP_H_

#include "Game.h"
#include "GameView.h"
#include "Globals.h"

#include "GamePlayers.h"


typedef struct edge {
    LocationID start;
    LocationID end;
    TransportID type;
} edge;


//for the game location array
#define NUM_GAME_LOCATIONS (NUM_MAP_LOCATIONS + 1)
#define UNKNOWN ((int)NUM_MAP_LOCATIONS)

#define SEEN 1
#define NOT_SEEN 0

#define validLocation(pid) ((validPlace(pid)) || (pid == UNKNOWN))


// graph representation is hidden
typedef struct map *Map;
typedef struct location *Location;

//helper function implemented in Map.c
LocationID *updateConnectedLocations(Map map, int *numLocations,
    LocationID from, PlayerID player, Round round,
    bool road, bool rail, bool sea);

// operations on graphs
Map newMap (void);
void disposeMap (Map);
void showMap (Map);
int numV (Map);
int numE (Map, TransportID);


void transportDFS (Map map, LocationID from, bool transportTypes[MAX_TRANSPORT], int hopsAllowance);

Location getMapLocation(Map map, LocationID location);
Player getLocationPlayer(Location location, PlayerID player);

//operations on map locations
void playMove(GameView gv, Map map, GameMove move, GameMove *history);
void movePlayer(GameMove move, Map map, Player player, LocationID newLocID, LocationID newLocTypeID);
void updateMoveHealth(GameView gv,  Map map, Player player, GameMove move);
int moveBackDistance(LocationID location);
LocationID distanceToDoubleBack(int distance);
LocationID *findShortestPermittedPath(Map map, LocationID start, LocationID end, bool prohibited[NUM_MAP_LOCATIONS], \
    bool transportTypes[MAX_TRANSPORT], int *pathLength);
/*LocationID *findShortestWeightedPath(Map map, LocationID start, LocationID end, bool prohibited[NUM_MAP_LOCATIONS], \
    bool transportTypes[MAX_TRANSPORT], int *pathLength);*/
void mapDFS (Map map, LocationID pre2[NUM_MAP_LOCATIONS], LocationID from, bool transportTypes[MAX_TRANSPORT], int hopsAllowance) ;
Map getGameMap(GameView gv);
int getTrapsFromMap(Map map, LocationID where);
int getVampsFromMap(Map map, LocationID where);
void setMapTrap(Map map, LocationID where, int traps);
void setMapVamp(Map map, LocationID where, int traps);
bool atPort(Map g, LocationID locID);
bool atSea(LocationID locID);
void prohibitConnections(Map map, bool prohibited[NUM_MAP_LOCATIONS],int minConnections,bool land,bool sea);
bool isConnected(Map map, LocationID loc1, LocationID loc2);
LocationID findOtherPath(Map map, LocationID start, LocationID dest, LocationID excluded, LocationID G, LocationID S, LocationID H, LocationID M);


#endif // !defined(MAP_H_)
