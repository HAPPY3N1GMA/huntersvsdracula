////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// dracula.c: your "Fury of Dracula" Dracula AI
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>

#include "DracView.h"
#include "Game.h"
#include "dracula.h"
#include "Item.h"
#include "Queue.h"
#include "Globals.h"
#include "Places.h"
#include "Map.h"

static bool useMessages(DracView dv);
static void DracBFS(DracView dv, Queue q, int moveNum, int seen[NUM_MAP_LOCATIONS],
     int ** minDist, LocationID root, LocationID G, LocationID S, LocationID H, LocationID M);
 static void sortLocations(DracView dv, LocationID *sorted, float *avgDist, int **minDist,int *minMinDist, int numPossibles);
 static void shuffle(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int numPossibles);
 bool inTrail(LocationID loc, LocationID trail[TRAIL_SIZE]);
 static void swap(int *i, int *j);
 static void swapF(float *i, float *j);
 static void swapData(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int j);
 static void printState(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int size);
 static void removeRome(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles);
 static void removeAthens(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles);
 static void removeSea(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles);
 static int landLocations(int *sorted, int numPossibles);
 static void checks(DracView dv, LocationID *sorted, float *avgDist,
     int **minDist, int *minMinDist, int numPossibles, int *move);
 static int checkMinDist(DracView dv, LocationID *sorted, int *minMinDist, int move, int numPossibles);
 static int findMaxMinDist(LocationID *sorted, int *minMinDist, int numPossibles);
 static bool badMove(LocationID loc);
 static int checkNotHunter(DracView dv, LocationID *sorted, int move, int numPossibles);
 static int checkHunterHop(LocationID *sorted, int *minMinDist, int move,  int numPossibles);
 static int checkSea(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int move, int numPossibles);
 static bool isEncounter(int move, int **minDist);
 static int checkCastle(DracView dv, LocationID *sorted, int **minDist, int *minMinDist, int move, int numPossibles);
 static int checkBeingFollowed(DracView dv, LocationID *sorted, int **minDist, int *minMinDist, int move, int numPossibles);
 static int checkDoubleBacksHides(DracView dv, LocationID *sorted, float *avgDist, int *minMinDist, int orig,int move, int numPossibles);
 static int chaseHunters(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int move, int numPossibles);
 static bool sneak(DracView dv);


// STRATEGY:
//     * Create a list of all possible and legal moves from current destination *
//     * Find the (average) distance from each possible to move to all hunters  *
//     * Select the lowest one *
//     * If the selected move is a move to the sea, then if there is a 'similar'
//         move NOT to the sea, choose that *
//     * If a connection is Castle Dracula AND no hunter is not within 2 steps
//         of Castle Dracula, choose Castle Dracula *
//     * Never choose a move that is within one place of a hunter, where possible *
//     * Never go closer to a hunter in your trail, where possible *
//     * Plus various others!!

void decideDraculaMove (DracView dv) {

    //we know one groups message format... this ones just for you g014890
    if(useMessages(dv)) return;

    int numMess = 0;
    PlayerMessage *messages = getMessages(dv, &numMess);
    //Hardcoded avoidance strategy against a few teams that smash drac ;)
    if(strcmp(messages[0],"The flower spreading throughout the land, Cure Blossom!") == 0
        || strcmp(messages[0], "You're dead, Dracula!") == 0
        || strcmp(messages[0], "Spawn") == 0
        || strcmp(messages[0], "mmmhmmmmm") == 0
        || strcmp(messages[0], "Normotensive Snavel's HunterAI ...") == 0
        || strcmp(messages[0], "Bats are not furries.") == 0
        || strcmp(messages[0], "Humans are not furries.") == 0) {
            LocationID cycle1[13] = {PRAGUE, VIENNA, BUDAPEST, KLAUSENBURG, CASTLE_DRACULA, DOUBLE_BACK_2, GALATZ, CONSTANTA, BLACK_SEA, IONIAN_SEA, TYRRHENIAN_SEA, MEDITERRANEAN_SEA, ATLANTIC_OCEAN};
            LocationID cycle2[13] = {GALWAY, DUBLIN, DOUBLE_BACK_2, HIDE, TELEPORT, KLAUSENBURG, GALATZ, CONSTANTA, BLACK_SEA, IONIAN_SEA, TYRRHENIAN_SEA, MEDITERRANEAN_SEA, ATLANTIC_OCEAN};

            int round = giveMeTheRound(dv) % 13;
            if(giveMeTheRound(dv) <=7) {
                registerBestPlay(idToAbbrev(cycle1[round]), "Found it!");
                return;
            }
            registerBestPlay(idToAbbrev(cycle2[round]), "Found it!");
            return;
    }


    //otherwise, we'll do some actual work
    int numPossibles = 0;
    LocationID *sorted = whereCanIgo(dv, &numPossibles, true, true);

    if(numPossibles == 1) {
        registerBestPlay(locToPlay(dv,sorted[0]), "Found it!");
        free(sorted);
        return;
    }

    //store current hunter locations
    LocationID G = whereIs(dv, PLAYER_LORD_GODALMING);
    LocationID S = whereIs(dv, PLAYER_DR_SEWARD);
    LocationID H = whereIs(dv, PLAYER_VAN_HELSING);
    LocationID M = whereIs(dv, PLAYER_MINA_HARKER);

    //store the minimum distance to each hunter
    int **minDist = malloc(sizeof(int *) * NUM_HUNTERS);
    if(minDist==NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }
    for(int i = 0; i < NUM_HUNTERS; i++){
        minDist[i] = malloc(sizeof(int) * numPossibles);
        if(minDist[i]==NULL){
            fprintf(stderr, "%s: Out of Memory\n", __func__);
            abort();
        }
    }

    //store the average to distance to all hunters from each possible move
    float *avgDist = malloc(sizeof(float) * numPossibles);
    if(avgDist == NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }
    for(int i = 0; i < numPossibles; i++) {
        avgDist[i] = 0;
    }

    //perform the BFS search from each location, to determine the minimum distance
    //from each possible move to each hunter
    Queue q;
    int seen[NUM_MAP_LOCATIONS];
    int layer;
    for(int i = 0; i < numPossibles; i++) {
        layer = 0;
        q = newQueue();
        for(int k = 0; k < NUM_MAP_LOCATIONS; k++) {
            seen[k] = NOT_SEEN;
        }
        seen[sorted[i]] = SEEN;
        Item item = newItem(sorted[i], layer);
        QueueJoin(q, item);
        if(sorted[i] == G) minDist[PLAYER_LORD_GODALMING][i] = getLayer(item);
        if(sorted[i] == S) minDist[PLAYER_DR_SEWARD][i] = layer;
        if(sorted[i] == H) minDist[PLAYER_VAN_HELSING][i] = layer;
        if(sorted[i] == M) minDist[PLAYER_MINA_HARKER][i] = layer;
        DracBFS(dv, q, i, seen, minDist, sorted[i], G, S, H, M);
        dropQueue(q);
    }

    //for each move, set the minimum of the minimum distances (handy later on)
    int *minMinDist = malloc(sizeof(int) * numPossibles);
    if(minMinDist == NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }
    for(int i = 0; i < numPossibles; i++) {
        minMinDist[i] = INT_MAX;
        for(int j = 0; j < NUM_HUNTERS; j++) {
            if(minDist[j][i] < minMinDist[i]) minMinDist[i] = minDist[j][i];
        }
    }

    //set the average minimum distance from each possible move to all hunters
    for(int i = 0; i < numPossibles; i++) {
        for(int j = 0; j < NUM_HUNTERS; j++) {
            avgDist[i] += minDist[j][i];
        }
        avgDist[i] /= NUM_HUNTERS;
    }

    //sort our possible moves by max average distance to least
    //sorted[0] will be the furthest from all hunters (on average)
    sortLocations(dv, sorted, avgDist, minDist, minMinDist, numPossibles);

    //for debugging
    printState(dv, sorted, avgDist, minDist, minMinDist, numPossibles);

    //really bad moves- don't go to Italy!!
    if(whereIs(dv, PLAYER_DRACULA) == FLORENCE) {
        removeRome(sorted, avgDist, minDist, minMinDist, &numPossibles);
    }

    //don't go to Athens!!
    if(whereIs(dv, PLAYER_DRACULA) == VALONA) {
        removeAthens(sorted, avgDist, minDist, minMinDist, &numPossibles);
    }

    //if you have low low health, don't go to sea! (where possible)
    if(howHealthyIs(dv, PLAYER_DRACULA) <= 10 && landLocations(sorted, numPossibles) > 0) {
        removeSea(sorted, avgDist, minDist, minMinDist, &numPossibles);
    }

    //again for debugging
    printState(dv, sorted, avgDist, minDist, minMinDist, numPossibles);

    //now convert any locations that are double backs,
    //hides or teleports into their correct types
    registerBestPlay(locToPlay(dv,sorted[0]), "Found it!");

    //store which move we are currently liking most
    int move = 0;
    //by doing lots of checks
    checks(dv, sorted, avgDist, minDist, minMinDist, numPossibles, &move);

    //if it is ok for us to do so, lets chase a hunter
    //if they are made well enough, they won't even know
    move = chaseHunters(dv, sorted, avgDist, minDist, minMinDist, move, numPossibles);

    // after all those checks, lets register our best move again
    registerBestPlay(locToPlay(dv,sorted[move]), "I hear you've been looking for me. I'm flattered.");

    //free all memory
    for(int i = 0; i < NUM_HUNTERS; i++) {
        free(minDist[i]);
    }
    free(minMinDist);
    free(minDist);
    free(avgDist);
    free(sorted);
    //and we're done! Mwahahahahaha!!!
}

//just for group g014890
static bool useMessages(DracView dv) {
    // int numMess = 0;
    // PlayerMessage *messages = getMessages(dv, &numMess);
    // PlayerMessage check;
    // strcpy(check, messages[numMess - 1]);
    // int len = strlen(check);
    // int pl[3] = {0};
    // bool useMess = false;
    // if(check[0] == '[' && check[len - 1] == ']') {
    //     int i = 1;
    //     int dig;
    //     int j = 0;
    //     while(isdigit(check[i])) {
    //         for(dig = i; check[dig] != ' ' && check[dig] != ']'; dig++);
    //         pl[j] = atoi(&check[i]);
    //         i = dig + 1;
    //         j++;
    //     }
    //     useMess = (pl[0] != 0 || (check[1] == '0' && check[2] == ' '));
    // }
    // if(useMess && pl[0] >= 20) {
    //     run();
    //     return true;
    // }
    return false;
}

//conduct a BFS of the map and determine minimum distance to all hunters
//makes the assumeption of 1 rail hop per person... not the best...
static void DracBFS(DracView dv, Queue q, int moveNum, int seen[NUM_MAP_LOCATIONS],
     int ** minDist, LocationID root, LocationID G, LocationID S, LocationID H, LocationID M) {

    while(!QueueIsEmpty(q)) {
        int size = 0;
        int layer;
        Item it = QueueLeave(q);
        LocationID *conns = getConnections(dv, getValue(it), &size);
        for(int i = 0; i < size; i++) {
            if(seen[conns[i]] == NOT_SEEN) {
                seen[conns[i]] = SEEN;
                if(getValue(it) == root) {
                    layer = getLayer(it) + 1;
                } else {
                    layer = getLayer(it);
                }
                Item item = newItem(conns[i], layer);
                if(conns[i] == G) minDist[PLAYER_LORD_GODALMING][moveNum] = layer;
                if(conns[i] == S) minDist[PLAYER_DR_SEWARD][moveNum] = layer;
                if(conns[i] == H) minDist[PLAYER_VAN_HELSING][moveNum] = layer;
                if(conns[i] == M) minDist[PLAYER_MINA_HARKER][moveNum] = layer;
                updateLayer(item, layer + 1);
                QueueJoin(q, item);
            }
        }
        DracBFS(dv, q, moveNum, seen, minDist, root, G, S, H, M);
        free(conns);
        free(it);
    }
}

//sort locations by various criteria
static void sortLocations(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int numPossibles) {

    //first sort by average distance to all hunters, highest to lowest
    bool cont = true;
    while(cont) {
        cont = false;
        for(int j = 0; j < numPossibles - 1; j++) {
            if(avgDist[j] < avgDist[j + 1]) {
                swapData(sorted, avgDist, minDist, minMinDist, j);
                cont = true;
            }
            //put trail locations towards the end so that we will preference
            //not double backs/hides when choosing a move
            LocationID trail[TRAIL_SIZE];
            giveMeTheTrail(dv, PLAYER_DRACULA, trail);
            if(avgDist[j] == avgDist[j + 1] && inTrail(abbrevToID(locToPlay(dv, sorted[j])), trail)
                && !inTrail(abbrevToID(locToPlay(dv, sorted[j + 1])), trail)) {//ie, avoid an infinite loop
                                                    //of swapping trail locations
                swapData(sorted, avgDist, minDist, minMinDist, j);
                cont = true;
            }
        }
    }

    //shuffle all moves that are to a hunter, or within one hop of a hunter, to the end
    shuffle(sorted, avgDist, minDist, minMinDist, numPossibles);
}

//determine if a location is in the given trail
bool inTrail(LocationID loc, LocationID trail[TRAIL_SIZE]) {
    for(int i = 0; i < TRAIL_SIZE - 1; i++) {
        if(loc == trail[i] && loc != UNKNOWN_LOCATION) return true;
    }
    return false;
}

//swap ints
static void swap(int *i, int *j) {
    int swap = *i;
    *i = *j;
    *j = swap;
}

//swap flaots
static void swapF(float *i, float *j) {
    float swap = *i;
    *i = *j;
    *j = swap;
}

//swap all the data we have collected
//allows us to use the same index for all arrays when grabbing information
static void swapData(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int j) {
    swap(&sorted[j], &sorted[j + 1]);
    swap(&minMinDist[j], &minMinDist[j + 1]);
    for(int i = 0; i < NUM_HUNTERS; i++) {
        swap(&minDist[i][j], &minDist[i][j + 1]);
    }
    swapF(&avgDist[j], &avgDist[j + 1]);
}

//find the number of land locations in out possible moves
static int landLocations(int *sorted, int numPossibles) {
    int numLand = 0;
    for(int i = 0; i < numPossibles; i++) {
        if(isLand(sorted[i])) numLand++;
    }
    return numLand;
}

//shuffle all locations 0 or 1 hop away from a hunter towards the end of our sorted list
static void shuffle(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int numPossibles) {

    //determine how many locations are 0 or 1 hop from a hunter
    int zeroCount = 0;
    int oneCount = 0;
    for(int i = 0; i < numPossibles; i++) {
        switch(minMinDist[i]) {
            case 0: if(!isSea(sorted[i])) {
                zeroCount++;
                break;
            }
            case 1: if(!isSea(sorted[i])) {
                oneCount++;
                break;
            }
        }
    }

    //shuffle zeroes first
    //we do not shuffling moves that are at sea, since encounters do not happen there
    int j;
    int prev = numPossibles - 1;
    int found = 0;
    for(int i = 0; i < zeroCount; i++) {
        j = prev;
        if(j < 0) break;
        if(minMinDist[j] == 0 && !isSea(sorted[j])) {
            found++;
        } else {
            for(; j >= 0; j--) {
                if(minMinDist[j] == 0 && !isSea(sorted[j])) {
                    found++;
                    break;
                }
            }
        }
        prev = j - 1;
        for(; j < (numPossibles - found); j++) {
            swapData(sorted, avgDist, minDist, minMinDist, j);
        }
    }

    //then shuffle ones
    //we do not shuffling moves that are at sea, since encounters do not happen there
    prev = numPossibles - zeroCount - 1;
    found = 0;
    for(int i = 0; i < oneCount; i++) {
        j = prev;
        if(j < 0) break;
        if(minMinDist[j] == 1 && !isSea(sorted[j])) {
            found++;
        } else {
            for(; j >= 0; j--) {
                if(minMinDist[j] == 1 && !isSea(sorted[j])) {
                    found++;
                    break;
                }
            }
        }
        prev = j - 1;
        for(; j < (numPossibles - zeroCount - found); j++) {
            swapData(sorted, avgDist, minDist, minMinDist, j);
        }
    }
}

//remove Rome from our list of possible moves (if allowed)
static void removeRome(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles) {
    bool rome = false;
    int num = *numPossibles;
    for(int i = 0; i < NUM_HUNTERS; i++) {
        if(minDist[i][0] < 3) {
            rome = true;
        }
    }

    for(int i = 0; i < num; i++) {
        if(sorted[i] == ROME && !rome && num > 1) {
            num -= 1;
            for(int j = i; j < num; j++) {
                swapData(sorted, avgDist, minDist, minMinDist, j);
            }
            break;
        }
    }
    *numPossibles = num;
}

//remove Athens from our list of possible moves (if allowed)
static void removeAthens(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles) {
    bool athens = false;
    int num = *numPossibles;
    for(int i = 0; i < NUM_HUNTERS; i++) {
        if(minDist[i][0] < 3) {
            athens = true;
        }
    }

    for(int i = 0; i < num; i++) {
        if(sorted[i] == ATHENS && !athens && num > 1) {
            num -= 1;
            for(int j = i; j < num; j++) {
                swapData(sorted, avgDist, minDist, minMinDist, j);
            }
            break;
        }
    }
    *numPossibles = num;
}

//remove sea locations from our list of possible moves (if allowed)
static void removeSea(LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int *numPossibles) {
    int num = *numPossibles;
    for(int i = num - 1; i >= 0; i--) {
        if(isSea(sorted[i]) && num > 1) {
            num -= 1;
            for(int j = i; j < num; j++) {
                swapData(sorted, avgDist, minDist, minMinDist, j);
            }
            break;
        }
    }
    *numPossibles = num;
}

//perform lots of checks on our sorted list, to return the index of the 'best' move
static void checks(DracView dv, LocationID *sorted, float *avgDist,
    int **minDist, int *minMinDist, int numPossibles, int *move) {

    //special criteria for the first move- start well!
    if(giveMeTheRound(dv) == 0) {
        *move = checkMinDist(dv, sorted, minMinDist, *move, numPossibles);
        return;
    }

    //if we are lacking health, we should get to the castle
    if(howHealthyIs(dv, PLAYER_DRACULA) <= 15 && whereIs(dv, PLAYER_DRACULA) != CASTLE_DRACULA) {
        LocationID where = getCastlePath(dv, whereIs(dv, PLAYER_DRACULA));
        if(where != whereIs(dv, PLAYER_DRACULA)) {
            for(int i = 0; i < numPossibles; i++) {
                if(sorted[i] == where) *move = i;
            }
            *move = checkSea(dv, sorted, avgDist, minDist, minMinDist, *move, numPossibles);
            if(minMinDist[*move] > 2) {
                return;
            }
        }
    }

    *move = checkNotHunter(dv, sorted, *move, numPossibles);

    //can we safely go to the castle?
    *move = checkCastle(dv, sorted, minDist, minMinDist, *move, numPossibles);

    //can we afford to go to sea? Is there a better, 'similar', move?
    *move = checkSea(dv, sorted, avgDist, minDist, minMinDist, *move, numPossibles);

    //are we being followed? How can we lose them?
    *move = checkBeingFollowed(dv, sorted, minDist, minMinDist, *move, numPossibles);

    //should we double back or hide, or avoid that?
    *move = checkDoubleBacksHides(dv, sorted, avgDist, minMinDist, *move, *move, numPossibles);
}

//for our first move, find the move that has a good average distance from all hunters
//but isn't close to any one hunter, or puts us in a bad first position
static int checkMinDist(DracView dv, LocationID *sorted, int *minMinDist, int move, int numPossibles) {
    if(move >= numPossibles) {
        move = findMaxMinDist(sorted, minMinDist, numPossibles);
        return move;
    }
    if(minMinDist[move] <= 4 || badMove(sorted[move])) return checkMinDist(dv, sorted, minMinDist, move + 1, numPossibles);
    return move;
}

//some places are just bad...
static bool badMove(LocationID loc) {
    if(loc == BARI || loc == NAPLES || isSea(loc) || loc == ATHENS || loc == VALONA
        || loc == CAGLIARI || loc == DUBLIN || loc == GALWAY) return true;
    return false;
}

//find the best location in terms of the minimum minimum distance
//ie, find the maximum minimum minimum distance
static int findMaxMinDist(LocationID *sorted, int *minMinDist, int numPossibles) {
    int max = 0;
    for(int i = 1; i < numPossibles; i++) {
        if((minMinDist[i] > minMinDist[max] && !badMove(sorted[i])) || badMove(sorted[max])) {
            max = i;
        }
    }
    return max;
}

//never go somewhere a hunter is
static int checkNotHunter(DracView dv, LocationID *sorted, int move, int numPossibles) {
    if(move >= numPossibles) return 0;
    if(sorted[move] == whereIs(dv, PLAYER_LORD_GODALMING)) return checkNotHunter(dv, sorted, move + 1, numPossibles);
    if(sorted[move] == whereIs(dv, PLAYER_DR_SEWARD)) return checkNotHunter(dv, sorted, move + 1, numPossibles);
    if(sorted[move] == whereIs(dv, PLAYER_VAN_HELSING)) return checkNotHunter(dv, sorted, move + 1, numPossibles);
    if(sorted[move] == whereIs(dv, PLAYER_MINA_HARKER)) return checkNotHunter(dv, sorted, move + 1, numPossibles);
    return move;
}

//never move somewhere within zero or one hops of a hunter! (where possible)
static int checkHunterHop(LocationID *sorted, int *minMinDist, int move, int numPossibles) {
    if(move >= numPossibles) {
        return 0;
    }
    if(minMinDist[move] == 0 || sorted[move] == CASTLE_DRACULA) {
        return checkHunterHop(sorted, minMinDist, move + 1, numPossibles);
    }
    return move;
}

//if our best move so far goes to sea, and there is a "similar" move not to sea, choose the similar move
//I don't really know how this function works, see if you can figure it out
static int checkSea(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int move, int numPossibles) {
    if(move >= numPossibles) {
        if(isSea(sorted[move]) && !isSea(sorted[0])) return 0;
        return move;
    }
    if(move < numPossibles){
        if(isSea(sorted[move])) {
            if(move == numPossibles - 1) {
                return move;
            }
            if(howHealthyIs(dv, PLAYER_DRACULA) <= 5) {
                if(isSea(sorted[move + 1])) return checkSea(dv, sorted, avgDist, minDist, minMinDist, move + 1, numPossibles);
                return move + 1;
            }
            move = checkDoubleBacksHides(dv, sorted, avgDist, minMinDist, move, move, numPossibles);
            LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
            lastMove(dv, PLAYER_DRACULA, &start, &end);
            int move2 = checkHunterHop(sorted, minMinDist, move + 1, numPossibles);
            if(move2 == 0) return move2;//to avoid an infinite loop of search for a better option
            move2 = checkSea(dv, sorted, avgDist, minDist, minMinDist, move2, numPossibles);
            if(avgDist[move2] >= avgDist[move] - 1.5 && !isSea(sorted[move2]) && !isEncounter(move2, minDist)) {
                //definition of similar move; the averagge distance to all hunters is no more than an extra hop and a half
                return move2;
            } else if (isSea(end) && !isSea(sorted[move2])) {
                return move2;
            } else {
                if(howHealthyIs(dv, PLAYER_DRACULA) <= 15) {
                    if(isSea(sorted[move2])) {
                        int move3 = checkHunterHop(sorted, minMinDist, move2 + 1, numPossibles);
                        if(!isEncounter(move3, minDist)) return move3;
                    }
                    return move2;
                } else {
                    return move2;
                }
            }
        }
    }
    return move;
}

//is this move an encounter? I sure hope not
static bool isEncounter(int move, int **minDist) {
    for(int i = 0; i < NUM_HUNTERS; i++) {
        if(minDist[i][move] == 0) return true;
    }
    return false;
}

//if CD is a possible move and no hunter is nearby, go there!
static int checkCastle(DracView dv, LocationID *sorted, int **minDist, int *minMinDist, int move, int numPossibles) {
    if(sorted[move] != CASTLE_DRACULA) {
        for(int i = 0; i < numPossibles; i++) {
            if(sorted[i] == CASTLE_DRACULA) {
                for(int j = 0; j < NUM_HUNTERS; j++) {
                    if(minDist[j][i] <= 2) return move;
                }
                return i;
            }
        }
        return move;
    } else if(minMinDist[move] <= 4) {
        if(move == 0 && numPossibles != 0) return 1;
        return 0;
    }
    return move;
}

//do not move towards someone in your trail!
static int checkBeingFollowed(DracView dv, LocationID *sorted, int **minDist, int *minMinDist, int move, int numPossibles) {
    LocationID trail[TRAIL_SIZE] = {0};
    giveMeTheTrail(dv, PLAYER_DRACULA, trail);
    LocationID G = whereIs(dv, PLAYER_LORD_GODALMING);
    LocationID S = whereIs(dv, PLAYER_DR_SEWARD);
    LocationID H = whereIs(dv, PLAYER_VAN_HELSING);
    LocationID M = whereIs(dv, PLAYER_MINA_HARKER);
    int better = checkHunterHop(sorted, minMinDist, move + 1, numPossibles);//move + 1 = next best index
    for(int i = 0; i < TRAIL_SIZE; i++) {
        if(trail[i] == G) {
            if(minDist[PLAYER_LORD_GODALMING][move] < minDist[PLAYER_LORD_GODALMING][better]) return better;
        } else if(trail[i] == S) {
            if(minDist[PLAYER_DR_SEWARD][move] < minDist[PLAYER_DR_SEWARD][better]) return better;
        } else if(trail[i] == H) {
            if(minDist[PLAYER_VAN_HELSING][move] < minDist[PLAYER_VAN_HELSING][better]) return better;
        } else if(trail[i] == M) {
            if(minDist[PLAYER_MINA_HARKER][move] < minDist[PLAYER_MINA_HARKER][better]) return better;
        }
    }
    return move;
}

//don't double back or hide if a hunter is nearby
static int checkDoubleBacksHides(DracView dv, LocationID *sorted, float *avgDist, int *minMinDist, int orig, int move, int numPossibles) {
    if(move >= numPossibles) return orig;
    if(minMinDist[move] <= 2) {
        LocationID trail[TRAIL_SIZE] = {UNKNOWN_LOCATION};
        giveMeTheTrail(dv, PLAYER_DRACULA, trail);
        for(int j = 0; j < TRAIL_SIZE; j++) {
            if((sorted[move] == trail[j] && !isSea(sorted[move]))) {
                if(move == numPossibles - 1) {
                    return orig;
                }
                if(minMinDist[move + 1] >= 1) {
                    checkDoubleBacksHides(dv, sorted, avgDist, minMinDist, orig, move + 1, numPossibles);
                }
                return move;
            }
        }
    }
    return move;
}

//chase the hunters, hoping they aren't dumb enough to go backwards a lot, but don't actually catch up
//can also be used to sneak past hunters moving towards you
//a fun way to play the game
static int chaseHunters(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int move, int numPossibles) {
    LocationID gStart, gEnd, sStart, sEnd, hStart, hEnd, mStart, mEnd;
    if(sneak(dv)) {
        return move;
    }
    lastMove(dv, PLAYER_LORD_GODALMING, &gStart, &gEnd);
    lastMove(dv, PLAYER_DR_SEWARD, &sStart, &sEnd);
    lastMove(dv, PLAYER_VAN_HELSING, &hStart, &hEnd);
    lastMove(dv, PLAYER_MINA_HARKER, &mStart, &mEnd);
    int possibleMove = -1;
    for(int i = 0; i < numPossibles; i++) {
        if((sorted[i] == gStart || sorted[i] == sStart || sorted[i] == hStart || sorted[i] == mStart) && !isSea(sorted[i]) && sorted[i] != CASTLE_DRACULA) {
            possibleMove = i;
            checks(dv, sorted, avgDist, minDist, minMinDist, numPossibles, &possibleMove);
            if(possibleMove == i) return possibleMove;
        }
    }
    return move;
}

//were we just sneaking past hunters, and not chasing them?
//we were sneaking if they moved closer to us, and chasing if they moved further or equal to us
static bool sneak(DracView dv) {
    LocationID start = UNKNOWN_LOCATION, end = UNKNOWN_LOCATION;
    lastMove(dv, PLAYER_DRACULA, &start, &end);
    LocationID trail[TRAIL_SIZE];
    int i;
    for(i = 0; i < NUM_HUNTERS; i++) {
        giveMeTheTrail(dv, i, trail);
        if(end == trail[2]) break;
    }
    if(i == 4) {
        return false;
    }
    int pathLength1 = 0, pathLength2 = 0;
    LocationID *path = getPath(dv, start, trail[2], &pathLength1);
    free(path);
    path = getPath(dv, start, trail[1], &pathLength2);
    free(path);
    if(pathLength1 >= pathLength2) return true;
    return false;
}

//view all stats for this gameboard, for debugging
static void printState(DracView dv, LocationID *sorted, float *avgDist, int **minDist, int *minMinDist, int numPossibles) {
    // printf("\nNumPossibles: %d\n", numPossibles);
    //
    // printf("\nPlaces: (in order of largest average distance from all hunters to smallest)\n");
    // for(int i = 0; i < numPossibles; i++) {
    //     printf("->%s\n", idToName(sorted[i]));
    // }
    // printf("\nAverages: (same order)\n");
    // for(int i = 0; i < numPossibles; i++)
    //     printf("->%.2f\n", avgDist[i]);
    //
    // printf("\nMin: (Columns in same order)\n");
    // for(int i = 0; i < NUM_HUNTERS; i++) {
    //     for(int j = 0; j < numPossibles; j++) {
    //         printf("%d ", minDist[i][j]);
    //     }
    //     printf("\n");
    // }
    //
    // printf("\nMinMin: (Columns in the same order)\n");
    // for(int i = 0; i < numPossibles; i++) {
    //     printf("%d ", minMinDist[i]);
    // }
    // printf("\n\n");
    //
    // printf("Current Health: %d\n\n", howHealthyIs(dv, PLAYER_DRACULA));
    //
    // printf("Current Location: %s\n\n", idToName(whereIs(dv, PLAYER_DRACULA)));
}
