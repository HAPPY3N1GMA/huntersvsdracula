////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// HunterView.c: the HunterView ADT implementation
//
// 2017-11-25   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>
// 2018-02-04 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>

#include "Game.h"
#include "GameView.h"
#include "Globals.h"
#include "HunterView.h"
#include "Queue.h"
#include "Item.h"
#include "Map.h"


struct hunterView {
    GameView gv;
};

typedef struct path *Path;
struct path{
    LocationID location;
    PlayerID player;
    int distance;
    LocationID *path;
};


Path *calculateHunterSearchPaths(HunterView hv, bool prohibited[NUM_MAP_LOCATIONS]);
static LocationID findHuntBestLocation(HunterView hv, LocationID *possibleLocations, int numLocations,
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]);
static void cullTrailUsingLocationTypes(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]);
static void cullTrailUsingBackTracks(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]);
static void cullTrailUsingConnections(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]);
    static void cullUsingPlayerLocations(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
        bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]);



/*
* createPath
* create a path object
 */
static Path createPath(LocationID location, PlayerID player, int distance, LocationID *path){
    Path p = malloc(sizeof(struct path));
    if(p==NULL){
        fprintf(stderr, "%s: Couldn't allocate Path\n", __func__);
    }
    p->location = location;
    p->player = player;
    p->distance = distance;
    p->path = path;
    return p;
}


/*
* destroyPath
* destroy a path object
 */
static void destroyPath(Path p){
    if(p==NULL) return;
    if(p->path!=NULL){
        free(p->path);
    }
    free(p);
}


/*
 * newHunterView
 * Creates a new HunterView to summarise the current state of the game
 */
HunterView newHunterView (char *pastPlays, PlayerMessage messages[]) {
    HunterView new = malloc (sizeof *new);
    if (new == NULL) {
        fprintf(stderr, "%s: Couldn't allocate HunterView\n", __func__);
    }
    new->gv = newGameView(pastPlays, messages);
    return new;
}


/*
 * disposeHunterView
 * Frees all memory previously allocated for the HunterView toBeDeleted
 */
void disposeHunterView (HunterView toBeDeleted) {
    if(toBeDeleted == NULL) {
        return;
    }
    disposeGameView(toBeDeleted->gv);
    free (toBeDeleted);
}

/*
 * giveMeTheRound
 * Get the current round
 */
// Get the current round
Round giveMeTheRound (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getRound(hv->gv);
}

/*
 * giveMeTheMap
 * Get the current round
 */
// Get the game map
Map giveMeTheMap (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getGameMap(hv->gv);
}


/*
 * whoAmI
 * Get the id of the current player
 */
PlayerID whoAmI (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getCurrentPlayer(hv->gv);
}


/*
 * giveMeTheScore
 * Get the current score
 */
int giveMeTheScore (HunterView hv) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getScore(hv->gv);
}

/*
 * howHealthyIs
 * Get the current health points for a given player
 */
int howHealthyIs (HunterView hv, PlayerID player) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getHealth(hv->gv, player);
}


/*
 * whereIs
 * Get the current location id of a given player
 */
LocationID whereIs (HunterView hv, PlayerID player) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    LocationID place = getPlayerLocation(getPlayer(hv->gv, player)); //actual location deduced
    if (player == PLAYER_DRACULA) {
        if(!validPlace(place)) {
            return getLocation(hv->gv, player);
        }else{
            return place;
        }
    }
    return place;
}


/*
 * whereWas
 * Return the most accurate known/predicted past location for a player
 */
LocationID whereWas (HunterView hv, PlayerID player, int pastRound) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return getPastLocation (hv->gv, player, pastRound);
}


/*
 * giveMeTheTrail
 * Fills the trail array with the location ids of the last 6 turns
 */
void giveMeTheTrail (HunterView hv, PlayerID player, LocationID trail[TRAIL_SIZE]) {
    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    //copy the trail
    LocationID *t = getTrail(hv->gv, player);
    for(int i = 0; i<TRAIL_SIZE; i++){
        trail[i] = t[i];
    }
}


/*
 * whereCanIgo
 * What are my possible next moves (locations)
 */
LocationID *whereCanIgo (HunterView hv, int *numLocations,
    bool road, bool rail, bool sea) {

    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    return whereCanTheyGo(hv, numLocations, whoAmI(hv), road, rail, sea);
}


/*
 * whereCanTheyGo
 * What are the specified player's next possible moves
 */
LocationID *whereCanTheyGo (HunterView hv, int *numLocations, PlayerID player,
    bool road, bool rail, bool sea) {

    if(hv == NULL) {
        fprintf(stderr, "%s: Null HunterView\n", __func__);
    }
    //are we looking at a players turn in the NEXT ROUND which affects rail jumps!
    int currRound = getRound(hv->gv);
    PlayerID currPlayerID = getCurrentPlayer(hv->gv);
    if(player < currPlayerID){
        currRound++;
    }

    //where can we get too?
    LocationID *where = connectedLocations (hv->gv, numLocations,
        getPlayerLocationType(getPlayer(hv->gv, player)), player, currRound,
        road, rail, sea);

    if(player == PLAYER_DRACULA){
        LocationID whereIsDrac = whereIs (hv, PLAYER_DRACULA);
        if(validPlace(whereIsDrac) || whereIsDrac == TELEPORT){
            LocationID *places = whereCanDraculaGoUpdate(hv->gv, numLocations, where, player, road, sea);
            if(*numLocations == 1 && places[0] == TELEPORT) {
                *numLocations = 2;
                places = realloc(places, sizeof(LocationID) * 2);
                if(places == NULL){
                    fprintf(stderr, "%s: Out of Memory\n", __func__);
                    abort();
                }
                places[0] = whereIsDrac;
                places[1] = CASTLE_DRACULA;
                return places;
            } else return places;
        } else if (currRound == 0) {//can go anywhere on his first turn
            where = realloc(where,sizeof(LocationID)*(NUM_MAP_LOCATIONS));
            if(where == NULL){
                fprintf(stderr, "%s: Out of Memory\n", __func__);
                abort();
            }
            for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
                where[i] = i;
            }
            *numLocations = NUM_MAP_LOCATIONS;
            return where;
        } else {//do not know his location, thus return NULL
            free(where);
            *numLocations = 0;
            return NULL;
        }
    }
    return where;
}


/*
 * getConnectedLocations
 * return list of connected locations to given location
 */
 LocationID *getConnectedLocations(HunterView hv, int *numLocations, PlayerID player, Round round, LocationID location,
     bool road, bool rail, bool sea){
     if(hv == NULL) {
         fprintf(stderr, "%s: Null HunterView\n", __func__);
     }
     return connectedLocations (hv->gv, numLocations, location, player,
         round, road, rail, sea);
 }



/*
 * calculateBestDestination
 * Calculate each hunters best path and return current players calculated destination
 */
LocationID calculateBestDestination(HunterView hv, bool prohibited[NUM_MAP_LOCATIONS]){

    PlayerID currPlayer = whoAmI(hv);

    //Hardcode Mina to hang around castle if we dont know where dracula is
    if(currPlayer == PLAYER_MINA_HARKER) {
        LocationID myLoc = whereIs(hv, currPlayer);
        LocationID trail[TRAIL_SIZE] = {UNKNOWN_LOCATION};
        giveMeTheTrail(hv, currPlayer, trail);
        if(myLoc == CASTLE_DRACULA) {
            return GALATZ;
        }
        if(myLoc == GALATZ) {
            return CONSTANTA;
        }
        if(myLoc == CONSTANTA) {
            return VARNA;
        }
        if(myLoc == VARNA) {
            return SOFIA;
        }
        if(myLoc == VARNA) {
            return SARAJEVO;
        }
    }



    //if SELWARD then check if madrid or lisbon connection is a valid location
    //stop dracula trying to hide down south!
    if(currPlayer == PLAYER_DR_SEWARD && (!prohibited[MADRID] || !prohibited[CADIZ] || !prohibited[LISBON])){

        LocationID history[TRAIL_SIZE];
        giveMeTheTrail(hv, PLAYER_DR_SEWARD,history);

        if(history[0] == MADRID && history[1] != ALICANTE){
            return ALICANTE;
        }

        if(history[0] == SARAGOSSA && history[1] == ALICANTE){
            return TOULOUSE;
        }
    }


    //dracula likes to hide in manchester , just check his trail isnt there before crossing EC
    if(currPlayer == PLAYER_LORD_GODALMING && (!prohibited[MANCHESTER] ||
         !prohibited[SWANSEA] || !prohibited[DUBLIN] || !prohibited[ENGLISH_CHANNEL])){

        LocationID history[TRAIL_SIZE];
        giveMeTheTrail(hv, PLAYER_LORD_GODALMING,history);

        if(history[0] == LONDON && history[1] != MANCHESTER){
            return MANCHESTER;
        }
    }


    //Prohib locations from players trails to stop them going back and forth
    //add any location in a players trail to prohibited destinations to travel too
    for(int i = 0; i<NUM_HUNTERS; i++){
        LocationID history[TRAIL_SIZE];
        giveMeTheTrail(hv, i, history);
        for(int k = 0; k<TRAIL_SIZE; k++){
            if(validPlace(history[k]) ) {
                prohibited[history[k]] = true;
            }
        }
    }

    //calculate the best path each hunter can take  to search the most ground across the map
    Path *playerPaths = calculateHunterSearchPaths(hv,prohibited);

    //select our players destination
    LocationID chosenLoc = MADRID;
    if(playerPaths[currPlayer] != NULL){
        chosenLoc = playerPaths[currPlayer]->location;
        if(!validPlace(chosenLoc)){
            chosenLoc = MADRID; //last resort go here
        }
    }

    //free all the predicted player paths
    for(int p = 0; p<NUM_HUNTERS; p++){
        if(playerPaths[p]!=NULL){
            destroyPath(playerPaths[p]);
        }
    }
    free(playerPaths);

    //mina search randomly around the castle
    if(currPlayer == PLAYER_MINA_HARKER) {
        bool transportTypes[] = {true,true,false};
        int pathLength = 0;
        bool prohib[NUM_MAP_LOCATIONS] = {false}; //we dont want to prohibit any locations on the trails
        LocationID *path = findShortestPermittedPath(getGameMap(hv->gv), chosenLoc, CASTLE_DRACULA, prohib, transportTypes, &pathLength);
        free(path);
        if(pathLength > 3){
            chosenLoc = CASTLE_DRACULA;
        }
    }


    //seward search randomly around madrid area IF and only if, its a possible area for drac
    if(currPlayer == PLAYER_DR_SEWARD && (!prohibited[MADRID] || !prohibited[CADIZ] || !prohibited[LISBON])) {
        int home = 0;
        LocationID sewardHistory[TRAIL_SIZE];
        for(int i = 0; i<TRAIL_SIZE; i++){
            if(sewardHistory[i] == MADRID || sewardHistory[i] == ALICANTE){
                home = 1;
                break;
            }
        }
        if(home == 0){
            chosenLoc = MADRID;
        }
    }

    return chosenLoc;
}








/*
 * calculateHunterSearchPaths
 * Search the entire map, and find the furtherest destinations from any player,
 * Using this list, take the longest paths (with its closest hunter) and store it to that hunter
 * Paths take into account prohibited location list and will not permit hunters to choose that
 * location as a destination
 */
Path *calculateHunterSearchPaths(HunterView hv, bool prohibited[NUM_MAP_LOCATIONS]){
    Path *playerPaths = malloc(sizeof(struct path)*NUM_HUNTERS);
    if(playerPaths==NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }

    for(int i = 0; i<NUM_HUNTERS; i++) playerPaths[i] = NULL;

    LocationID *path = NULL;
    for(int m = 0; m<NUM_HUNTERS; m++){

        //keep a list of possible paths we can take
        Path possiblePaths[NUM_MAP_LOCATIONS] = {NULL};

        //find the locations that are furtherest from any hunter
        for(int i = 0; i<NUM_MAP_LOCATIONS; i++){
            if(validPlace(i) && isLand(i) && !prohibited[i]){

                PlayerID locPlayer = -1;
                LocationID *locPath = NULL;

                //what is the shortest path we found
                int minPath = NUM_MAP_LOCATIONS;
                for(int k = 0; k< NUM_HUNTERS; k++){

                    //dont look at this player if we already have a place for them to go
                    if(playerPaths[k] != NULL) continue;

                    bool transportTypes[MAX_TRANSPORT] = {true,true,false};

                    //how far from every player to this map location
                    LocationID end = i;

                    //we always calculate any player that has gone before our turn,
                    //and use their furthest back trail location as the basis for our search
                    //this allows us to better predict the same destination over consecutive turns
                    LocationID start = UNKNOWN_LOCATION;
                    int lookBack = 0;
                    int round = getRound(hv->gv);
                    switch (round) {
                        case 0: lookBack = 0; break;
                        case 1: lookBack = 1; break;
                        case 2: lookBack = 2; break;
                        case 3: lookBack = 3; break;
                        default: lookBack = 4;
                    }

                    PlayerID player = getCurrentPlayer(hv->gv);
                    if(k < player){
                        start = getPastLocation(hv->gv, k, lookBack+1);
                    }else{
                        start = getPastLocation(hv->gv, k, lookBack);
                    }

                    //find the shortest path from the current player to this destination
                    int pathLength = 0;
                    bool prohib[NUM_MAP_LOCATIONS] = {false}; //we dont want to prohibit any locations on the trails
                    path = findShortestPermittedPath(getGameMap(hv->gv), start, end, prohib, transportTypes, &pathLength);

                    //if we didnt find a path, then check if a sea is in the way
                    if(pathLength == 0){
                        transportTypes[2] = true;
                        path = findShortestPermittedPath(getGameMap(hv->gv), start, end, prohib, transportTypes, &pathLength);
                    }

                    //is this path the shortest we have found?
                    //store it along with the locationID, and the PlayerID we have chosen it for
                    if(minPath > pathLength && playerPaths[k]==NULL){
                        if(locPath!=NULL){
                            free(locPath); //free the old stored path
                        }
                        minPath = pathLength;
                        locPath = path;
                        locPlayer = k;
                    }else{
                        free(path);
                    }
                }
                //we now have the shortest path and who is closest to it
                //store this as a possible path we could take
                if(locPath!=NULL){
                    possiblePaths[i] = createPath(i,locPlayer,minPath,locPath);
                }
            }
        }

        //we now have a list of all permitted map locations, and a path from the closest player to that location
        PlayerID chosenPlayer = -1;
        Path chosenPath = NULL;

        //find the furtherest location (and its player) in this list
        int distance = 0;
        for(int i = 0; i<NUM_MAP_LOCATIONS; i++){
            Path curr = possiblePaths[i];
            if(curr!=NULL){
                //assume mina stays near the castle
                if(getCurrentPlayer(hv->gv)!=PLAYER_MINA_HARKER && curr->player == PLAYER_MINA_HARKER){
                    destroyPath(possiblePaths[i]);
                    continue;
                }

                //assume seward stays near madrid
                if(getCurrentPlayer(hv->gv)!=PLAYER_DR_SEWARD && curr->player == PLAYER_DR_SEWARD){
                    destroyPath(possiblePaths[i]);
                    continue;
                }

                //if player hasnt already got a chosen path and this path is longer than one we have found, then store it for our players
                if((curr->distance > distance) && (playerPaths[curr->player] == NULL)){
                    destroyPath(chosenPath);
                    chosenPath = possiblePaths[i];
                    distance = curr->distance;
                    chosenPlayer = curr->player;
                }else{
                    destroyPath(possiblePaths[i]);
                }
            }
        }


        //Update our prohibited list
        // Stop hunters going within 2 hops of this new destination
        // Prevent this player or location being chosen again
        if(chosenPath!=NULL){
            //update players path
            playerPaths[chosenPath->player] = chosenPath;
            possiblePaths[chosenPath->location] = NULL;

            Path pp = playerPaths[chosenPlayer];
            prohibited[pp->location] = true; //this is now a prohibited destination

            // Stop hunters going within 2 hops  of this new destination
            //only road hops permitted
            bool transportTypes[MAX_TRANSPORT] = {true,true,false};
            LocationID pre[NUM_MAP_LOCATIONS] = {NOT_SEEN};
            mapDFS(getGameMap(hv->gv), pre, pp->location,transportTypes, 3);
            for(int i = 0; i < NUM_MAP_LOCATIONS; i++) {
                if(pre[i] == SEEN) {
                    prohibited[i] = true;
                }
            }
        }
    }
    return playerPaths;
}




/*
 * updateConnections
 * set to false any location in possibleMapLocations that is not in the currLocs connections list
 */
static void updateConnections(GameView gv, LocationID currLoc, Round round, PlayerID player,
    bool road, bool rail, bool sea, bool *possibleMapLocations){
    int numConnections;
    LocationID * connections = connectedLocations (gv, &numConnections, currLoc, player,
        round, road, rail, sea);

    //check every location in the possibleMapLocations
    for(int m = 0; m<NUM_MAP_LOCATIONS; m++){
        // if its not a valid connection, set false
        bool connected = false;
        for(int c = 0; c < numConnections; c++){
            LocationID con = connections[c];
            if(possibleMapLocations[m] && con == m){
                connected = true;
            }
        }
        //if not connected then remove it from that trail!
        if(!connected){
            possibleMapLocations[m] = false;
        }
    }
    free(connections);
}



/*
 * checkConn
 * returns true if the currLoc is connected to any of the given possibleMapLocations
 * used to calculate if dracula could get to x location from a given array of starting locations
 */
static bool checkConn(GameView gv, LocationID currLoc, Round round, PlayerID player,
    bool road, bool rail, bool sea, bool *possibleMapLocations){
    int numConnections;
    LocationID * connections = connectedLocations (gv, &numConnections, currLoc, player,
        round, road, rail, sea);
    bool result = true;
    bool connected = false;
    for(int c = 0; c < numConnections; c++){
        LocationID con = connections[c];
        if(possibleMapLocations[con]){
            connected = true;
        }
    }
    //if not connected then remove it from that trail!
    if(!connected){
        result = false;
    }
    free(connections);
    return result;
}




/*
 * setLocationBools
 * sets everywhere but given location to false in array
 */
static void setLocationBools(LocationID location, bool *array){
    for(int m = 0; m<NUM_MAP_LOCATIONS; m++){
        array[m] = false;
    }
    array[location] = true;
}



/*
 * chkBacktrackAndUpdateLists
 * if curr move by dracula is a hide or backtrack, then remove any locations that are not
 * found in the backtracks possible locations list
 */
static void chkBacktrackAndUpdateLists(LocationID *origTrail, LocationID currentLocation, int currentRound,
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){

    if(!validPlace(currentLocation)){
        return;
    }

    //if we are beyond our current round ignore it
    if(origTrail[currentRound] == UNKNOWN_LOCATION) return;

    if(currentRound < 1 || currentRound >= HUNTING_TRAIL){
        return;
    }

    for(int i = 1; i < TRAIL_SIZE && ((currentRound - i) >= 0); i++){
        if(moveBackDistance(origTrail[currentRound - i]) != i){
            masterList[currentRound - i][currentLocation] = false;
        }
    }
}


/*
 * letsHunt
 * Using draculas past 12 moves, calculate using game logic to determine where dracula could be on the board.
 * If a definite location found return true, else return false, and set prohib locations to false where we know dracula
 * definitley cannot be.
 */
bool letsHunt(HunterView hv, bool prohibitedLocations[NUM_MAP_LOCATIONS], LocationID *caughtYou){

    //where is dracula?
    LocationID dracLoc = whereIs (hv,PLAYER_DRACULA);
    if(validPlace(dracLoc)){
        *caughtYou = dracLoc;
        return true;
    }

    if(dracLoc == TELEPORT){
        *caughtYou = CASTLE_DRACULA;
        return true;
    }

    //build list of every players trail
    LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL];
    for(int i = 0; i<NUM_PLAYERS; i++){
        LocationID *tr = getTrail(hv->gv, i);
        for(int t = 0; t<HUNTING_TRAIL; t++){
            playerTrails[i][t] = tr[t];
        }
    }

    //make a list of anywhere dracula could have been at any point of his trail
    //this is for 12 rounds, and holds num map places
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS];
    for(int i = 0; i<HUNTING_TRAIL; i++){
        for(int m = 0; m<NUM_MAP_LOCATIONS; m++) masterList[i][m] = true;

        //set hospital to always be false
        masterList[i][ST_JOSEPH_AND_ST_MARYS] = false;

        //if drac is not on our players LAND locations, then remove this as a possible location
        for(int p = 0; p<NUM_HUNTERS; p++){
            LocationID playerLoc = playerTrails[p][i];
            LocationID dracLoc = playerTrails[PLAYER_DRACULA][i];

            if(validPlace(playerLoc) && isLand(playerLoc)){
                if(dracLoc == playerLoc) {
                    //set everywhere else false
                    setLocationBools(playerLoc, masterList[i]);
                }else{
                    masterList[i][playerLoc] = false;
                }
            }
        }
    }

    //cull list based on is drac on land or sea, or was previous move land/sea
    cullTrailUsingLocationTypes(hv, playerTrails, masterList);
    if(validPlace(playerTrails[PLAYER_DRACULA][0])){
        *caughtYou =playerTrails[PLAYER_DRACULA][0];
        return true;
    }

    //cull list using players positions in the trail
    cullUsingPlayerLocations(hv, playerTrails, masterList);
    if(validPlace(playerTrails[PLAYER_DRACULA][0])){
        *caughtYou =playerTrails[PLAYER_DRACULA][0];
        return true;
    }

    //cull list using previous turns possible location and any connections dracula could make.
    cullTrailUsingConnections(hv,playerTrails,masterList);
    if(validPlace(playerTrails[PLAYER_DRACULA][0])){
        *caughtYou =playerTrails[PLAYER_DRACULA][0];
        return true;
    }

    //cull list using the backtrack possible locations
    cullTrailUsingBackTracks(hv,playerTrails,masterList);
    if(validPlace(playerTrails[PLAYER_DRACULA][0])){
        *caughtYou =playerTrails[PLAYER_DRACULA][0];
        return true;
    }

    //after culling backtracks etc, we need to now check our connections again!
    cullTrailUsingConnections(hv,playerTrails,masterList);
    if(validPlace(playerTrails[PLAYER_DRACULA][0])){
        *caughtYou =playerTrails[PLAYER_DRACULA][0];
        return true;
    }

    //set the probited locations list and work out if only one place drac could be
    int cnt = 0;
    LocationID huntingLocations[NUM_MAP_LOCATIONS];
    //we now have the most accurate list of current dracula positions
    for(int m = 0; m<NUM_MAP_LOCATIONS; m++){
        prohibitedLocations[m] = (masterList[0][m] == false); //prohib list is opp
        if(masterList[0][m]){
            huntingLocations[cnt] = m;
            cnt++;
        }
    }

    //work out where dracula is
    *caughtYou = findHuntBestLocation(hv,huntingLocations,cnt,masterList);
    if(*caughtYou == UNKNOWN_LOCATION){
        return false;
    }
    return true;
}



/*
 * cullUsingPlayerLocations
 * If a player is on land, and we know draculawas not at this position, then we can work out that
 * dracula was NOT at this location at any point in the previous 5 moves
 */
static void cullUsingPlayerLocations(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){

    int maxBackTrack = 0;

    //for every point in dracs trail from this point in time, back max trail size moves, if drac location
    //is not this location, we know for certain he was not here!
    for(int i = HUNTING_TRAIL-1; i >=0 ; i--){
        for(int p = 0; p < NUM_HUNTERS; p++){
            LocationID playerLoc = playerTrails[p][i];
            for(int b = 0; b<=maxBackTrack; b++){
                LocationID dracLoc = playerTrails[PLAYER_DRACULA][i+b];
                if(isLand(playerLoc) && dracLoc != playerLoc){
                    masterList[i+b][playerLoc] = false;
                }
            }
        }
        //dont overflow off the back of the trail array
        if(maxBackTrack < TRAIL_SIZE - 2){
            maxBackTrack++;
        }
    }
}




/*
 * cullTrailUsingLocationTypes
 * Culls the list of possible locations at each point of the trail, based on the type of location it is,
 * and if dracula was previously on land/sea and moved onto sea/land (ie at a port)
 */
static void cullTrailUsingLocationTypes(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){
    if(hv == NULL){
        return;
    }
    Map gameMap = getGameMap(hv->gv);
    Round currRound = getRound(hv->gv);

    LocationID dracLoc = UNKNOWN_LOCATION;
    LocationID dracPrevLoc = UNKNOWN_LOCATION;

    //starting with furtherest location in drac trail work forward
    //and calculate list of locations that dracula could possibly be
    for(int i = (HUNTING_TRAIL-1); i>=0; i--){

        dracLoc = playerTrails[PLAYER_DRACULA][i];
        int backTrack = moveBackDistance(dracLoc);

        if((backTrack+i) >= HUNTING_TRAIL){
            continue;
        }
        dracLoc = playerTrails[PLAYER_DRACULA][i+backTrack];

        if(dracLoc == UNKNOWN_LOCATION) continue;

        //build list of current locations that dracula could CURRENTLY be based on their current
        // and previous trail location
        for(int m = 0; m<NUM_MAP_LOCATIONS; m++){

            //if we know dracs location, then set all other locations to false
            if(validPlace(dracLoc)){
            //    printf("test 1\n");
                if(dracLoc!=m){
                    masterList[i][m] = false;
                }

            }else if(isSea(dracLoc)){
            //if its a sea remove anywhere not a sea from the list of valid locations
                if(isLand(m)){
                    masterList[i][m] = false;
                }
            }else if(isLand(dracLoc)){
            //if its a land position remove anywhere not land from list of valid locations
                if(isSea(m)){
                    masterList[i][m] = false;
                }
                //if dracula was just at sea, remove anywhere that is not a port!
                if(isSea(dracPrevLoc) && !atPort(gameMap,m)){
                    masterList[i][m] = false;
                }
                //if dracula was just at sea, remove anywhere that is not a port!
                if(isSea(dracPrevLoc) && !atPort(gameMap,m)){
                    masterList[i][m] = false;
                }
            }
        }

        // This is a known location, therefore remove anywhere in the previous trail
        // that is not in this places connections
        if(dracPrevLoc != UNKNOWN_LOCATION && validPlace(dracLoc)){
            //only sea connections
            if(isSea(dracPrevLoc)){
                updateConnections(hv->gv, dracLoc, currRound-i, PLAYER_DRACULA, false, false, true, masterList[i+1]);
            }else{
                //only road connections (no rail as drac can't use rail!)
                updateConnections(hv->gv, dracLoc, currRound-i, PLAYER_DRACULA, true, false, false, masterList[i+1]);
            }
        }
         dracPrevLoc = dracLoc;
    }
}



/*
 * cullTrailUsingBackTracks
 * starting at second last round, for each location in a rounds list,
 * check if it was possible to get there from the previous
 * rounds locations and if not set false
 */
static void cullTrailUsingBackTracks(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){
    if(hv == NULL){
        return;
    }

    LocationID dracPrevLoc = UNKNOWN_LOCATION;

    for(int i = (HUNTING_TRAIL-1); i>=0; i--){

        LocationID dracLoc = playerTrails[PLAYER_DRACULA][i];
        if((dracLoc == UNKNOWN_LOCATION) &&
            (dracPrevLoc == UNKNOWN_LOCATION)) continue;

        //starting at second oldest trail position
        if(dracPrevLoc == UNKNOWN_LOCATION){
            dracPrevLoc = dracLoc;
            continue;
        }

        //is this a backtrack move? if so we want to also cull anywhere that doesnt
        //connect to the backtrack trail list
        int backTrack = moveBackDistance(dracLoc);

        LocationID backTrackLoc =  UNKNOWN_LOCATION;
        int index = i + backTrack;

        if(backTrack > 0){
            if(index >= 0 && index < (HUNTING_TRAIL-1)){
                backTrackLoc = playerTrails[PLAYER_DRACULA][index];

                //if its a known location then remove anywhere in our current array, that does not connect to that location
                if(validPlace(backTrackLoc)){
                    playerTrails[PLAYER_DRACULA][i] = backTrackLoc;
                    dracLoc = backTrackLoc;

                    for(int k = 0; k<NUM_MAP_LOCATIONS;k++){
                        masterList[i][k] = false;
                        if(backTrackLoc == k){
                            masterList[i][k] = true;
                        }
                    }
                }else{
                    int count = 0;
                    LocationID knownLocation = UNKNOWN_LOCATION;
                    //for each connection in our current list, check that it can get to  our backtracks trail
                    for(int m = 0; m < NUM_MAP_LOCATIONS; m++){
                        if(!masterList[i][m]) continue;  //only look for connections to valid places
                        //only sea connections
                        if(validPlace(backTrackLoc)){
                            if(isSea(backTrackLoc)){
                                masterList[i][m] = checkConn(hv->gv, m, 1, PLAYER_DRACULA, false, false, true, masterList[i+backTrack]);
                            }else{
                                //only road connections (no rail as drac can't use rail!)
                                masterList[i][m] = checkConn(hv->gv, m, 1, PLAYER_DRACULA, true, false, false, masterList[i+backTrack]);
                            }
                        }else{
                            if(isSea(backTrackLoc)){
                                masterList[i][m] = checkConn(hv->gv, m, 1, PLAYER_DRACULA, false, false, true, masterList[i+backTrack]);
                            }else{
                                //only road connections (no rail as drac can't use rail!)
                                masterList[i][m] = checkConn(hv->gv, m, 1, PLAYER_DRACULA, true, false, false, masterList[i+backTrack]);
                            }
                            //see if its in the list of that backtracks list of possible locations
                            //and that its in the previous location list as well
                            if(backTrack!=1 && masterList[i][m]){
                                    masterList[i][m] = masterList[i+backTrack][m];
                            }else if(backTrack==1){
                                    masterList[i][m] = masterList[i+backTrack][m];
                            }
                        }

                        if(masterList[i][m]){
                            knownLocation = m;
                            count++;
                        }
                    }
                    if(count==1){
                        //set only this as a valid location for this turn!
                        setLocationBools(knownLocation, masterList[i]);
                        playerTrails[PLAYER_DRACULA][i] = knownLocation;
                        dracLoc = knownLocation;
                    }
                }
            }
        }
        dracPrevLoc = dracLoc;
    }
}



/*
 * cullTrailUsingConnections
 * Using draculas previous possible locations, cull down the current list of possible locations
 * IE could dracula get to Madrid if his previous locations were Manchest, Dublin, Edinburgh.
 * No? then remove it from this list!
 */
static void cullTrailUsingConnections(HunterView hv, LocationID playerTrails[NUM_PLAYERS][HUNTING_TRAIL],
    bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){
    if(hv == NULL){
        return;
    }

    LocationID *originalDracTrail = getTrail(hv->gv, PLAYER_DRACULA);

    for(int i = HUNTING_TRAIL - 1;  i > 0 ; i--){
        LocationID dracPrevLoc = playerTrails[PLAYER_DRACULA][i];
        LocationID dracLoc = playerTrails[PLAYER_DRACULA][i-1];

        if(dracPrevLoc == UNKNOWN_LOCATION) continue;

        //go through PREVIOUS turns locations
        if(validPlace(dracLoc)){
            setLocationBools(dracLoc, masterList[i-1]);
            chkBacktrackAndUpdateLists(originalDracTrail, dracLoc, i-1, masterList);
            continue;
        }

        int count = 0;
        LocationID loc = UNKNOWN_LOCATION;
        for(int m = 0; m<NUM_MAP_LOCATIONS; m++){

            //if previous location already discounted then skip
            if(!masterList[i-1][m]){
                continue;
            }

            //if previous location or backtrack location was sea, we are only interested if you can get here via the ocean
            if(isSea(dracLoc) || isSea(dracPrevLoc)){
                masterList[i-1][m]  = checkConn(hv->gv, m, i-1, PLAYER_DRACULA, false, false, true, masterList[i]);
            }else{
                masterList[i-1][m]  = checkConn(hv->gv, m, i-1, PLAYER_DRACULA, true, false, false, masterList[i]);
            }
            if(masterList[i-1][m]){
                loc = m;
                count++;
            }
        }
        if(count == 1){
            playerTrails[PLAYER_DRACULA][i-1] = loc;
            dracLoc = loc;
            setLocationBools(dracLoc, masterList[i-1]);
            chkBacktrackAndUpdateLists(originalDracTrail, dracLoc, i-1, masterList);
            continue;
        }
        //Theres an error in the game engine not feeding hunters hides, so they get lost!
        if(count == 0){
            //so just copy back everything from previous location
            for(int m = 0; m<NUM_MAP_LOCATIONS; m++){
                masterList[i-1][m] = masterList[i][m];
            }
        }
    }
}



/*
 * findBestMove
 * Calculate the best possible Legal move we can make to get to our desired destination
 */
LocationID findBestMove(HunterView hv, LocationID destination){
    //calculate the path to the destination
    int numMoves = 0;
    LocationID *possibleLocs = whereCanIgo (hv, &numMoves, true, true, true);

    //i need the know what the shortest possible path to get there is
    LocationID *path = NULL;
    LocationID bestPosition = possibleLocs[0];
    int shortestLength = MAX_MAP_LOCATION;
    int pathLength = 0;
    bool transportTypes[MAX_TRANSPORT] = {true,true,true};
    bool prohibited[NUM_MAP_LOCATIONS] = {false};

    Map map = giveMeTheMap(hv);

    //check for each of these moves, how long it will take to get to where we are going
    for(int i = 0; i < numMoves ; i++){
        //calculate the path
        path = findShortestPermittedPath(map,possibleLocs[i], destination, prohibited, transportTypes, &pathLength);
        //store the shortest path that we find
        if(path!=NULL && pathLength < shortestLength){
            bestPosition = possibleLocs[i];
            shortestLength = pathLength;
        }
        free(path);
    }
    free(possibleLocs);

    return bestPosition;
}




/*
 * findHuntBestLocation
 * Calculate the best possible Legal move we can make to get to our desired destination
 */
static LocationID findHuntBestLocation(HunterView hv, LocationID *possibleLocations, int numLocations, bool masterList[HUNTING_TRAIL][NUM_MAP_LOCATIONS]){
    if(numLocations==1){
        return possibleLocations[0];
    }

    if(numLocations > 20){
        return UNKNOWN_LOCATION;
    }

    //if theres a lot of possible drac locations, then see which locations show up the most in the past 3 moves
    //we want to look at those locations first!
    if(numLocations > 5){
        int index = 0;
        for(int m = 0; m<NUM_MAP_LOCATIONS; m++){
            int count = 0;
            for(int i = 0; i<3; i++){
                if(masterList[i][m]) count++;
            }
            //if a location in all 3 as a possible spot for drac, then store it as a valid next move
            if(count >= 2){
                possibleLocations[index++] = m;
            }
        }
        if(index > 0){
            numLocations = index;
        }
    }

    int seaCount = 0;
    for(int i = 0; i<numLocations; i++){
        if(isSea(possibleLocations[i])) seaCount++;
    }
    //if more than 6 or more seas then its really just total luck where dracula could be now
    if(seaCount > 5){
        return UNKNOWN_LOCATION;
    }

    //if player in florence and trail has rome or bari available, head for NAPLES
    LocationID history[TRAIL_SIZE];
    giveMeTheTrail(hv, whoAmI(hv),history);
    if(history[0] == FLORENCE && history[0] != ROME){
        for(int i = 0; i<numLocations; i++){
            if(possibleLocations[i] == ROME || possibleLocations[i] == BARI){
                return NAPLES;
            }
        }
    }
    if(history[0] == MANCHESTER && history[1] == LONDON){
        for(int i = 0; i<numLocations; i++){
            if(possibleLocations[i] == EDINBURGH){
                return EDINBURGH;
            }
        }
    }


    //i dont want hunters to backtrack via somewhere they just came from
    //so add that to list of prohibited locations to travel via!
    bool prohibited[NUM_MAP_LOCATIONS] = {false};
	for(int i = 0; i<NUM_HUNTERS; i++){
	    LocationID history[TRAIL_SIZE];
	    giveMeTheTrail(hv, i, history);
	    for(int h = 0; h<3; h++){
	        if(validPlace(history[h])) {
	            prohibited[history[h]] = true;
	        }
	    }
	}

    bool prohib[NUM_MAP_LOCATIONS] = {false};
    bool transportTypes[] = {true,true,true};
    int pathLength = 0;
    LocationID *path  = NULL;
    Map map = getGameMap(hv->gv);

    if(numLocations==0){
         return UNKNOWN_LOCATION;
    }

    int pathLengths[NUM_MAP_LOCATIONS][NUM_HUNTERS];
    for(int i = 0; i < NUM_MAP_LOCATIONS; i++){
        for(int p = 0; p<NUM_HUNTERS; p++){
            pathLengths[i][p] = INT_MAX;
        }
    }

    LocationID theChosenOne = UNKNOWN_LOCATION;
    for(int i = 0; i < numLocations; i++){
        if(!validPlace(possibleLocations[i])) continue;
        //if this place is prohibited (IE I just came from there), then dont bother calculating a path
        //unless we have no choice!!
	    if(prohibited[possibleLocations[i]]){
            if(theChosenOne == UNKNOWN_LOCATION){
                theChosenOne = possibleLocations[i]; //last resort in case no other options
            }
            continue;
        }

        theChosenOne = possibleLocations[i];
        for(int p = 0; p<NUM_HUNTERS; p++){
            pathLength = 0;
            path = findShortestPermittedPath(map, whereIs(hv,p), possibleLocations[i], prohib, transportTypes, &pathLength);
            free(path);
            //store the distance to that location for this player
            pathLengths[i][p] = pathLength;
        }
    }

    //for the current player calculate if we have only one place that we are close too
    PlayerID currPlayer = whoAmI(hv);
    LocationID *closestDestinations = malloc(sizeof(LocationID)*(numLocations+1));
    if(closestDestinations==NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }
    for(int i = 0; i<numLocations+1; i++){
        closestDestinations[i] = UNKNOWN_LOCATION;
    }

    int *closestDistances = malloc(sizeof(int)*(numLocations+1));
    if(closestDistances==NULL){
        fprintf(stderr, "%s: Out of Memory\n", __func__);
        abort();
    }
    for(int i = 0; i<numLocations+1; i++){
        closestDistances[i] = INT_MAX;
    }

    //in case it never fires
    closestDestinations[0] = possibleLocations[0];

    int cnt = 1;
    int shortestPath = INT_MAX;
    for(int i = 0; i<numLocations; i++){
        int dist = pathLengths[i][currPlayer];
        if(dist < shortestPath){
            closestDestinations[0] = possibleLocations[i];
            closestDistances[0] = dist;
            shortestPath = dist;
            cnt = 1;
        }else if(dist == shortestPath){
            closestDistances[cnt] = dist;
            closestDestinations[cnt] = possibleLocations[i];
            cnt++;
        }
    }

    if(cnt==0){
        abort(); //there was an error
    }

    //are we closest to just one location?
    if(cnt == 1){
        theChosenOne = closestDestinations[0];
    }else{
        //we want to go to the destination FURTHEREST from the other players (but close to us)
        int furthest = 0;
        for(int i = 0; i<cnt; i++){
            int closestPlayerDist = INT_MAX;
            for(int p = 0; p<NUM_HUNTERS; p++){
                if(p==currPlayer) continue;
                int pDist = pathLengths[i][p];
                if(pDist < closestPlayerDist){
                    closestPlayerDist = pDist;
                }
            }
            LocationID loc = closestDestinations[i];

            if(closestPlayerDist > furthest){
                furthest = closestPlayerDist;
                theChosenOne = loc;
            }
        }
    }

    free(closestDestinations);
    free(closestDistances);
    return theChosenOne;
}
