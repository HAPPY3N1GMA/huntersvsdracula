//an Item ADT, storing a value and a layer
// Written by John Shepherd, March 2013

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Item.h"

struct item {
	int value;
	int layer;
};

Item newItem(int val, int layer) {
	Item new = malloc(sizeof(struct item));
	if(new == NULL) {
		fprintf(stderr, "%s: Null Item\n", __func__);
		abort();
	}
	new->value = val;
	new->layer = layer;
	return new;
}

int getLayer(Item it) {
	return it->layer;
}

int getValue(Item it) {
	return it->value;
}

void updateLayer(Item item, int layer) {
	item->layer = layer;
}
