////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// testHunterView.c: test the HunterView ADT
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "HunterView.h"

#define COLOR_RED     "\x1b[91m"
#define COLOR_GREEN   "\x1b[92m"
#define COLOR_CYAN    "\x1b[96m"
#define COLOR_RESET   "\x1b[0m"
#define COLOR_BLUE    "\x1b[94m"
#define LINE "***************************************************************************\n"
#define PRINT_SUCC printf(COLOR_GREEN"COMPLETED!"COLOR_RESET"\n");
#define PRINT_FAIL printf(COLOR_RED"INCOMPLETE!"COLOR_RESET"\n");


static void blackBoxTests(void);
static int bbTest1(void);
static int bbTest2(void);
static int bbTest3(void);
static int bbTest4(void);
static int bbTest5(void);
static int bbTest6(void);
static int bbTest7(void);
static int bbTest8(void);
static int bbTest9(void);
static int bbTest10(void);
static int bbTest11(void);
static int bbTest12(void);
static int bbTest13(void);
static int bbTest14(void);
static int bbTest15(void);
static int bbTest16(void);
static int bbTest17(void);
static int bbTest18(void);
static int bbTest19(void);
static int bbTest20(void);
static int bbTest21(void);
static int bbTest22(void);
static int bbTest23(void);
static int bbTest24(void);
static int bbTest25(void);
static int bbTest26(void);
static int bbTest27(void);

int main (int argc, char *ardv[]) {

    setbuf(stdout,NULL);
    /***********************
    *   FUNCTION TESTING  *
    ************************/
    printf(LINE);
    printf(LINE);
    printf(COLOR_CYAN"Running HunterView Tests!"COLOR_RESET"\n");
    printf(LINE);
    printf(LINE);

    /***********************
    *   BLACKBOX TESTING  *
    ************************/
    printf(COLOR_CYAN"Black Box tests:"COLOR_RESET"\n");
    printf(LINE);
    blackBoxTests();
}

static void bb_tests(int (*testFunction) (void),char *msg)
{
    printf("%s",msg);
    if(testFunction()){
        PRINT_SUCC
    }else{
        PRINT_FAIL
    }
}

static void blackBoxTests(void) {

    bb_tests(bbTest1,"Test 1: Basic initialisation\t\t\t\t\t");
    bb_tests(bbTest2,"Test 2: giveMeTheRound\t\t\t\t\t\t");
    bb_tests(bbTest3,"Test 3: Extended Play(no health changes)\t\t\t");
    bb_tests(bbTest4,"Test 4: whoAmI\t\t\t\t\t\t\t");
    bb_tests(bbTest5,"Test 5: Dracula encounter\t\t\t\t\t");
    bb_tests(bbTest6,"Test 6: Trap encounter\t\t\t\t\t\t");
    bb_tests(bbTest7,"Test 7: Vampire encounter\t\t\t\t\t");
    bb_tests(bbTest8,"Test 8: Hunter loses all health (and gets it back)\t\t");
    bb_tests(bbTest9,"Test 9: Dracula loses health at sea\t\t\t\t");
    bb_tests(bbTest10,"Test 10: Dracula regains health\t\t\t\t\t");
    bb_tests(bbTest11,"Test 11: Hunter regains health\t\t\t\t\t");
    bb_tests(bbTest12,"Test 12: Vampire Matures\t\t\t\t\t");
    bb_tests(bbTest13,"Test 13: Give me the trail\t\t\t\t\t");
    bb_tests(bbTest14,"Test 14: Give me the trail after double back\t\t\t");
    bb_tests(bbTest15,"Test 15: Give me the trail after hide\t\t\t\t");
    bb_tests(bbTest16,"Test 16: Give me the trail after teleportation\t\t\t");
    bb_tests(bbTest17,"Test 17: whereCanIgo: empty string\t\t\t\t");
    bb_tests(bbTest18,"Test 18: whereCanIgo: road only\t\t\t\t\t");
    bb_tests(bbTest19,"Test 19: whereCanIgo: sea only\t\t\t\t\t");
    bb_tests(bbTest20,"Test 20: whereCanIgo: rail only\t\t\t\t\t");
    bb_tests(bbTest21,"Test 21: whereCanIgo: combination of all\t\t\t");
    bb_tests(bbTest22,"Test 22: whereCanTheygo: empty string\t\t\t\t");
    bb_tests(bbTest23,"Test 23: whereCanTheygo: road only\t\t\t\t");
    bb_tests(bbTest24,"Test 24: whereCanTheygo: sea only\t\t\t\t");
    bb_tests(bbTest25,"Test 25: whereCanTheygo: rail only\t\t\t\t");
    bb_tests(bbTest26,"Test 26: whereCanTheygo: combination of all\t\t\t");
    bb_tests(bbTest27,"Test 27: Where can Dracula Go?\t\t\t\t\t");

    printf(LINE);
    printf(LINE);
    printf(COLOR_CYAN"All tests completed!"COLOR_RESET"\n");
    printf(LINE);
    printf(LINE);
}


/*
* Basic Game Initialisation Checks
*/
static int bbTest1(void) {
    char *trail = "GBE.... SBR.... HLO.... MCA....";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE);
    assert(giveMeTheRound(hv) == 0);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == BELGRADE);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == BERLIN);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == LONDON);
    assert(whereIs(hv, PLAYER_MINA_HARKER) == CADIZ);
    assert(whereIs(hv, PLAYER_DRACULA) == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    return 1;
}


/*
* GiveMeTheRound
*/
static int bbTest2(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(giveMeTheRound(hv) == 0);
    disposeHunterView(hv);

    char *trail2 = "GBE.... SBR.... HLO.... MCA.... DC?.V..";
    hv = newHunterView(trail2, messages);
    assert(giveMeTheRound(hv) == 1);
    disposeHunterView(hv);

    char *trail3 = "GMN.... SPL.... HAM.... MPA.... DC?.V.. "
                    "GLV.... SLO.... HNS.... MST.... DC?T... "
                    "GIR....";
    hv = newHunterView(trail3, messages);
    assert(giveMeTheRound(hv) == 2);
    disposeHunterView(hv);

    char *trail4 = "GED.... SGE.... HZU.... MCA.... DCF.V.. "
                    "GMN.... SCFVD.. HGE.... MLS.... DBOT... "
                    "GLO.... SMR.... HCF.... MMA.... DC?T... "
                    "GPL.... SMS.... HMR.... MGR.... DBAT... "
                    "GLO.... SBATD.. HMS.... MMA.... DC?T... "
                    "GPL.... SSJ.... HBA.... MGR.... DC?T... "
                    "GPL.... SSJ.... HBA.... MGR.... DC?T.V. ";
    hv = newHunterView(trail4, messages);
    assert(giveMeTheRound(hv) == 7);
    disposeHunterView(hv);

    return 1;
}

/*
* Extended Game Play (No health changes)
*/
static int bbTest3(void) {
    char *trail = "GHA.... SLO.... HLS.... MCF.... DC?.V.. GBR.... SSW.... "
                    "HMA.... MTO.... DC?T... GLI.... SLV.... HCA.... MMR.... "
                    "DC?T... GHA.... SMN.... HLS.... MCF.... DC?T... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert (giveMeTheRound (hv) == 4);
    assert (whereIs (hv, PLAYER_LORD_GODALMING) == HAMBURG);
    assert (whereIs (hv, PLAYER_DR_SEWARD) == MANCHESTER);
    assert (whereIs (hv, PLAYER_VAN_HELSING) == LISBON);
    assert (whereIs (hv, PLAYER_MINA_HARKER) == CLERMONT_FERRAND);
    assert (whereIs (hv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert (howHealthyIs (hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (howHealthyIs (hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (hv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (hv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    return 1;
}

/*
* WhoAmI?
*/
static int bbTest4(void) {
    char *trail = "GLO.... SAO.... HCD.... ";
    PlayerMessage messages[] = {"random"};
    HunterView hv = newHunterView(trail, messages);
    assert(giveMeTheRound(hv) == 0);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == LONDON);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == ATLANTIC_OCEAN);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == CASTLE_DRACULA);
    assert(whereIs(hv, PLAYER_MINA_HARKER) == UNKNOWN_LOCATION);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    char *trail2 = "GMN.... SPL.... HAM.... MPA.... DC?.V.. "
                    "GLV.... SLO.... HNS.... MST.... DC?T... "
                    "GIR.... ";
    hv = newHunterView(trail2, messages);
    assert(giveMeTheRound(hv) == 2);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == IRISH_SEA);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == LONDON);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == NORTH_SEA);
    assert(whereIs(hv, PLAYER_MINA_HARKER) == STRASBOURG);

    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    return 1;
}

/*
* Encounters with Dracula
*/
static int bbTest5(void) {
    char *trail = "GST.... SAO.... HCD.... MAO.... DGE.V.. "
                    "GGEVD..";
    PlayerMessage messages[] = {
        "Hello", "Rubbish",  "Stuff", "", "Mwahahah", "Aha!"};
    HunterView hv = newHunterView (trail, messages);
    assert (whereIs (hv, PLAYER_DRACULA) == GENEVA);
    assert (howHealthyIs (hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_DRACULA_ENCOUNTER);
    assert (howHealthyIs (hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert (whereIs (hv, PLAYER_LORD_GODALMING) == GENEVA);
    disposeHunterView (hv);

    //check dracula encounters are only read from string and are not predicted by our hunters!
    char *trail2 = "GST.... SAO.... HCD.... MAO.... DGE.... "
                    "GGE....";
    hv = newHunterView (trail2, messages);
    assert (whereIs (hv, PLAYER_DRACULA) == GENEVA);
    assert (howHealthyIs (hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (howHealthyIs (hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (whereIs (hv, PLAYER_LORD_GODALMING) == GENEVA);
    disposeHunterView (hv);

    return 1;
}

/*
* Encounters with Traps
*/
static int bbTest6(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DLVT.... "
                    "GLVT.. ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    HunterView hv = newHunterView(trail, messages);

    assert(whereIs(hv, PLAYER_DRACULA) == LIVERPOOL);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) ==
        GAME_START_HUNTER_LIFE_POINTS - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeHunterView(hv);

    //check trap encounters are only read from string and are not predicted by our hunters!
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DLVT... GLV.... ";
    hv = newHunterView (trail2, messages);
    assert(whereIs(hv, PLAYER_DRACULA) == LIVERPOOL);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeHunterView (hv);

    return 1;
}


/*
* Encounters with Vampires
*/
static int bbTest7(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DC?.V.. GLV.V.. ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    HunterView hv = newHunterView(trail, messages);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(howHealthyIs(hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    return 1;
}

/*
* Hunter Loses all Health - TP and gain health
*/
static int bbTest8(void) {
    char *trail = "GLS.... SMR.... HCD.... MAM.... DSN.V.. "
                    "GMA..... SCF.... HGA.... MCO.... DSRT.... "
                    "GSNT.... SMR.... HCD.... MAM.... DMAT... "
                    "GSRT.... SCF.... HGA.... MBU.... DHIT... "
                    "GMATTD. SCF.... HGA.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    //checking a hunter successfully "dies" and gets TP to hospital
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == 0);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert(howHealthyIs(hv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(whereIs(hv, PLAYER_DRACULA) == MADRID);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == ST_JOSEPH_AND_ST_MARYS);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == CLERMONT_FERRAND);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == GALATZ);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);
    assert(whereIs(hv, PLAYER_MINA_HARKER) == BRUSSELS);
    disposeHunterView(hv);

    //lets now resurrect our Hunter
    char *trail2 = "GLS.... SMR.... HCD.... MAM.... DSN.V.. "
                    "GMA..... SCF.... HGA.... MCO.... DSRT.... "
                    "GSNT.... SMR.... HCD.... MAM.... DMAT... "
                    "GSRT.... SCF.... HGA.... MBU.... DHIT... "
                    "GMATTD. SMR.... HGA.... MAM.... DC?T... "
                    "GSZ.... SCF.... HCD.... MCO.... DC?T....";
    hv = newHunterView(trail2, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == SZEGED);
    assert(whereIs(hv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);
    disposeHunterView(hv);

    return 1;
}


/*
* Dracula loses health at sea:....
*/
static int bbTest9(void) {
    char *trail = "GLO.... SED.... HPL.... MAM.... DS?.V.. "
                    "GMN.... SMN.... HLO.... MCO.... DD1T... "
                    "GLV.... SLO.... HEC.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    //Dracula loses 2 blood points if ending his turn at sea
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS -
        (LIFE_LOSS_SEA*2));
    assert(whereIs(hv, PLAYER_DRACULA) == DOUBLE_BACK_1); //check its a double back
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == LONDON);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == ENGLISH_CHANNEL);
    assert(whereIs(hv, PLAYER_MINA_HARKER) == COLOGNE);
    disposeHunterView(hv);

    return 1;
}


/*
* Dracula regains his lost health
*/
static int bbTest10(void) {
    //before Dracula regains his health
    char *trail = "GBC.... SAO.... HCD.... MAO.... DGA.V.. "
                    "GGAD... SAO.... HGA.... MAO.... DC?T... "
                    "GKL.... SAO.... HCN.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    assert(whereIs(hv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    disposeHunterView(hv);

    //Then gains health at Castle Dracula
    char *trail2 =  "GBC.... SAO.... HCD.... MAO.... DGA.V.. "
                    "GGAD... SAO.... HGA.... MAO.... DCDT... "
                    "GKL.... SAO.... HCN.... MAO.... DD2T... " //dracula at GA
                    "GCD.... SAO.... HVR.... MAO.... DC?T... " //dracula at BC
                    "GMR.... SAO.... HSO.... MAO.... DC?T... ";
    hv = newHunterView(trail2, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    assert(howHealthyIs(hv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER + LIFE_GAIN_CASTLE_DRACULA);
    assert(howHealthyIs(hv, PLAYER_DRACULA) >= GAME_START_BLOOD_POINTS);
    assert(whereIs(hv, PLAYER_DRACULA) == CITY_UNKNOWN);
    disposeHunterView(hv);

    return 1;
}


/*
* Test Hunter regains health to full
*/
static int bbTest11(void) {

    //hunter has an encounter
    char *trail = "GZA.... SMR.... HCD.... MAM.... DC?.V.. "
                    "GSJ..... SMR.... HCD.... MAM.... DC?T... "
                    "GBET.... SMR.... HCD.... MAM.... DC?T... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER);
    assert(whereIs(hv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == BELGRADE);
    disposeHunterView(hv);

    char *trail2 = "GZA.... SMR.... HCD.... MAM.... DC?.V.. "
                    "GSJ..... SMR.... HCD.... MAM.... DC?T... "
                    "GBET.... SMR.... HCD.... MAM.... DC?T... "
                    "GJM..... SMR.... HCD.... MAM.... DC?T... ";
    hv = newHunterView(trail2, messages);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == ST_JOSEPH_AND_ST_MARYS);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeHunterView(hv);

    char *trail3 = "GZA.... SMR.... HCD.... MAM.... DC?.V.. "
                    "GSJ..... SMR.... HCD.... MAM.... DC?T... "
                    "GBET.... SMR.... HCD.... MAM.... DC?T... "
                    "GBE..... SMR.... HCD.... MAM.... DC?T... "
                    "GBE..... SMR.... HCD.... MAM.... DC?T... ";
    hv = newHunterView(trail3, messages);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeHunterView(hv);

    char *trail4 = "GZA.... SMR.... HCD.... MAM.... DC?.V.. "
                    "GSJ..... SMR.... HCD.... MAM.... DC?T... "
                    "GBETTT.. SMR.... HCD.... MAM.... DC?T... "
                    "GBE..... SMR.... HCD.... MAM.... DC?T... "
                    "GBE..... SMR.... HCD.... MAM.... DC?T... ";
    hv = newHunterView(trail4, messages);
    assert(howHealthyIs(hv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
    - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER
    + LIFE_GAIN_REST + LIFE_GAIN_REST);
    disposeHunterView(hv);

    return 1;
}


/*
* Test Vampire has not yet Matured
*/
static int bbTest12(void) {
    char *trail = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    //now the vampire matures...
    char *trail2 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DED..V. ";
    hv = newHunterView(trail2, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv)
        - SCORE_LOSS_VAMPIRE_MATURES);

    disposeHunterView(hv);

    //check vampire maturing is only read from string
    //and not hardcoded in (even if its an invalid play)
    char *trail3 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... "
                    "GGE.... SGE.... HGE.... MGE.... DEDT... ";
    hv = newHunterView(trail3, messages);
    assert(giveMeTheScore(hv) == GAME_START_SCORE - giveMeTheRound(hv));
    disposeHunterView(hv);

    return 1;
}


/*
* Test their Trails grow and drop off
*/
static int bbTest13(void) {

    //check EMPTY trail
    char *trail = "";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    LocationID history[TRAIL_SIZE];
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    //check 1 trail
    char *trail2 = "GLO.... SAM.... HGE.... ";
    hv = newHunterView(trail2, messages);

    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == LONDON);
    assert (history[1] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == AMSTERDAM);
    assert (history[1] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == GENEVA);
    assert (history[1] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    //check 2 trail//GLO.... SAM.... HGE.... MBA.... DC?.V..
    char *trail3 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HST.... ";
    hv = newHunterView(trail3, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == SWANSEA);
    assert (history[1] == LONDON);
    assert (history[2] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == COLOGNE);
    assert (history[1] == AMSTERDAM);
    assert (history[2] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == STRASBOURG);
    assert (history[1] == GENEVA);
    assert (history[2] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == BARCELONA);
    assert (history[1] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == CITY_UNKNOWN);
    assert (history[1] == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    //check 3 trail
    char *trail4 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HST.... MTO.... DS?T... "
                    "GIR.... SLI.... HFR.... ";
    hv = newHunterView(trail4, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == IRISH_SEA);
    assert (history[1] == SWANSEA);
    assert (history[2] == LONDON);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == LEIPZIG);
    assert (history[1] == COLOGNE);
    assert (history[2] == AMSTERDAM);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == FRANKFURT);
    assert (history[1] == STRASBOURG);
    assert (history[2] == GENEVA);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == TOULOUSE);
    assert (history[1] == BARCELONA);
    assert (history[2] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == SEA_UNKNOWN);
    assert (history[1] == CITY_UNKNOWN);
    assert (history[2] == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    //check 4 trail
    char *trail5 = "GLO.... SAM.... HST.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HFR.... MTO.... DS?T... "
                    "GIR.... SLI.... HCO.... MMR.... DD2T... "
                    "GLV.... SHA.... ";
    hv = newHunterView(trail5, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == LIVERPOOL);
    assert (history[1] == IRISH_SEA);
    assert (history[2] == SWANSEA);
    assert (history[3] == LONDON);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == HAMBURG);
    assert (history[1] == LEIPZIG);
    assert (history[2] == COLOGNE);
    assert (history[3] == AMSTERDAM);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == COLOGNE);
    assert (history[1] == FRANKFURT);
    assert (history[2] == STRASBOURG);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == MARSEILLES);
    assert (history[1] == TOULOUSE);
    assert (history[2] == BARCELONA);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == DOUBLE_BACK_2);
    assert (history[1] == SEA_UNKNOWN);
    assert (history[2] == CITY_UNKNOWN);
    assert (history[3] == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    char *trail6 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HST.... MTO.... DHIT... "
                    "GIR.... SLI.... HFR.... MMR.... DC?T.. "
                    "GLV.... SHA.... HNU.... MCF.... DS?T... "
                    "GMN.... SBR.... HLI.... ";
    hv = newHunterView(trail6, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == MANCHESTER);
    assert (history[1] == LIVERPOOL);
    assert (history[2] == IRISH_SEA);
    assert (history[3] == SWANSEA);
    assert (history[4] == LONDON);
    assert (history[5] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == BERLIN);
    assert (history[1] == HAMBURG);
    assert (history[2] == LEIPZIG);
    assert (history[3] == COLOGNE);
    assert (history[4] == AMSTERDAM);
    assert (history[5] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == LEIPZIG);
    assert (history[1] == NUREMBURG);
    assert (history[2] == FRANKFURT);
    assert (history[3] == STRASBOURG);
    assert (history[4] == GENEVA);
    assert (history[5] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == CLERMONT_FERRAND);
    assert (history[1] == MARSEILLES);
    assert (history[2] == TOULOUSE);
    assert (history[3] == BARCELONA);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == SEA_UNKNOWN);
    assert (history[1] == CITY_UNKNOWN);
    assert (history[2] == HIDE);
    assert (history[3] == CITY_UNKNOWN);
    assert (history[4] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    //check 6 trail

    char *trail7 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HST.... MTO.... DHIT... "
                    "GIR.... SLI.... HFR.... MMR.... DC?T... "
                    "GLV.... SHA.... HNU.... MCF.... DS?T... "
                    "GMN.... SBR.... HLI.... MBO.... DC?T... "
                    "GED.... SPR.... HBR.... ";
    hv = newHunterView(trail7, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == EDINBURGH);
    assert (history[1] == MANCHESTER);
    assert (history[2] == LIVERPOOL);
    assert (history[3] == IRISH_SEA);
    assert (history[4] == SWANSEA);
    assert (history[5] == LONDON);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == PRAGUE);
    assert (history[1] == BERLIN);
    assert (history[2] == HAMBURG);
    assert (history[3] == LEIPZIG);
    assert (history[4] == COLOGNE);
    assert (history[5] == AMSTERDAM);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == BERLIN);
    assert (history[1] == LEIPZIG);
    assert (history[2] == NUREMBURG);
    assert (history[3] == FRANKFURT);
    assert (history[4] == STRASBOURG);
    assert (history[5] == GENEVA);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == BORDEAUX);
    assert (history[1] == CLERMONT_FERRAND);
    assert (history[2] == MARSEILLES);
    assert (history[3] == TOULOUSE);
    assert (history[4] == BARCELONA);
    assert (history[5] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == CITY_UNKNOWN);
    assert (history[1] == SEA_UNKNOWN);
    assert (history[2] == CITY_UNKNOWN);
    assert (history[3] == HIDE);
    assert (history[4] == CITY_UNKNOWN);
    assert (history[5] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    //check all trails have dropped off
    char *trail8 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HST.... MTO.... DHIT... "
                    "GIR.... SLI.... HFR.... MMR.... DC?T... "
                    "GLV.... SHA.... HNU.... MCF.... DS?T... "
                    "GMN.... SBR.... HLI.... MBO.... DS?T... "
                    "GED.... SPR.... HBR.... MNA.... DC?T... "
                    "GNS.... SVI.... HPR.... ";
    hv = newHunterView(trail8, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == NORTH_SEA);
    assert (history[1] == EDINBURGH);
    assert (history[2] == MANCHESTER);
    assert (history[3] == LIVERPOOL);
    assert (history[4] == IRISH_SEA);
    assert (history[5] == SWANSEA);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == VIENNA);
    assert (history[1] == PRAGUE);
    assert (history[2] == BERLIN);
    assert (history[3] == HAMBURG);
    assert (history[4] == LEIPZIG);
    assert (history[5] == COLOGNE);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == PRAGUE);
    assert (history[1] == BERLIN);
    assert (history[2] == LEIPZIG);
    assert (history[3] == NUREMBURG);
    assert (history[4] == FRANKFURT);
    assert (history[5] == STRASBOURG);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == NANTES);
    assert (history[1] == BORDEAUX);
    assert (history[2] == CLERMONT_FERRAND);
    assert (history[3] == MARSEILLES);
    assert (history[4] == TOULOUSE);
    assert (history[5] == BARCELONA);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == CITY_UNKNOWN);
    assert (history[1] == SEA_UNKNOWN);
    assert (history[2] == SEA_UNKNOWN);
    assert (history[3] == CITY_UNKNOWN);
    assert (history[4] == HIDE);
    assert (history[5] == CITY_UNKNOWN);
    disposeHunterView(hv);

    return 1;
}



/*
* Test their Trails can resolve locations (Double Back)
*/
static int bbTest14(void) {
    //Check before resolved location
    char *trail = "GLO.... SAM.... HST.... MBA.... DC?.V.. "
                    "GSW.... SCO.... HFR.... MTO.... DS?T... "
                    "GIR.... SLI.... HCO.... MMR.... DD2T... "
                    "GLV.... SHA.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    LocationID history[TRAIL_SIZE];
   giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
   assert (history[0] == LIVERPOOL);
   assert (history[1] == IRISH_SEA);
   assert (history[2] == SWANSEA);
   assert (history[3] == LONDON);
   assert (history[4] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
   assert (history[0] == HAMBURG);
   assert (history[1] == LEIPZIG);
   assert (history[2] == COLOGNE);
   assert (history[3] == AMSTERDAM);
   assert (history[4] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
   assert (history[0] == COLOGNE);
   assert (history[1] == FRANKFURT);
   assert (history[2] == STRASBOURG);
   assert (history[3] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
   assert (history[0] == MARSEILLES);
   assert (history[1] == TOULOUSE);
   assert (history[2] == BARCELONA);
   assert (history[3] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_DRACULA, history);
   assert (history[0] == DOUBLE_BACK_2);
   assert (history[1] == SEA_UNKNOWN);
   assert (history[2] == CITY_UNKNOWN);
   assert (history[3] == UNKNOWN_LOCATION);
   disposeHunterView(hv);


    //check history revealing the Double Back 2 as the city we had an encounter in
    char *trail2 = "GLO.... SAM.... HST.... MBA.... DAM.V.. "
                    "GSW.... SCO.... HFR.... MTO.... DNST... "
                    "GIR.... SLI.... HCO.... MMR.... DD2T... "
                    "GLV.... SHA.... HAMTVD. ";
   hv = newHunterView(trail2, messages);
   giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
   assert (history[0] == LIVERPOOL);
   assert (history[1] == IRISH_SEA);
   assert (history[2] == SWANSEA);
   assert (history[3] == LONDON);
   assert (history[4] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
   assert (history[0] == HAMBURG);
   assert (history[1] == LEIPZIG);
   assert (history[2] == COLOGNE);
   assert (history[3] == AMSTERDAM);
   assert (history[4] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
   assert (history[0] == AMSTERDAM);
   assert (history[1] == COLOGNE);
   assert (history[2] == FRANKFURT);
   assert (history[3] == STRASBOURG);
   assert (history[4] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
   assert (history[0] == MARSEILLES);
   assert (history[1] == TOULOUSE);
   assert (history[2] == BARCELONA);
   assert (history[3] == UNKNOWN_LOCATION);
   giveMeTheTrail (hv, PLAYER_DRACULA, history);
   assert (history[0] == DOUBLE_BACK_2);
   assert (history[1] == NORTH_SEA);
   assert (history[2] == AMSTERDAM);
   assert (history[3] == UNKNOWN_LOCATION);
   disposeHunterView(hv);


   //Dracula doubles back a bit further
   char *trail7 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                   "GSW.... SCO.... HST.... MTO.... DHIT... "
                   "GIR.... SLI.... HFR.... MMR.... DC?T... "
                   "GLV.... SHA.... HNU.... MCF.... DC?T... "
                   "GMN.... SBR.... HLI.... MBO.... DC?T... "
                   "GED.... SPR.... HBR.... MNA.... DD5T... "
                   "GNS.... SVI.... HPR....  MNA.... DC?T.V. "
                   "GNS.... SBD.... HPR....  MNA.... DC?T... ";
   hv = newHunterView(trail7, messages);
   giveMeTheTrail(hv, PLAYER_DRACULA, history);
   assert(history[0] == CITY_UNKNOWN);
   assert(history[1] == CITY_UNKNOWN);
   assert(history[2] == DOUBLE_BACK_5);
   assert(history[3] == CITY_UNKNOWN);
   assert(history[4] == CITY_UNKNOWN);
   assert(history[5] == CITY_UNKNOWN);
   disposeHunterView(hv);


   //double back 5 test
   char *trail8 = "GLO.... SAM.... HGE.... MBA.... DC?.V.. "
                   "GLO.... SCO.... HST.... MTO.... DMAT... "
                   "GEC.... SLI.... HFR.... MMR.... DC?T... "
                   "GLE.... SHA.... HNU.... MCF.... DC?T... "
                   "GNA.... SBR.... HLI.... MBO.... DC?T... "
                   "GBB.... SPR.... HBR.... MNA.... DC?T... "
                   "GSN.... SVI.... HPR....  MNA.... DD5T.V. "
                   "GMADT.. ";
   hv = newHunterView(trail8, messages);
   giveMeTheTrail(hv, PLAYER_DRACULA, history);
   assert(history[0] == DOUBLE_BACK_5);
   assert(history[1] == CITY_UNKNOWN);
   assert(history[2] == CITY_UNKNOWN);
   assert(history[3] == CITY_UNKNOWN);
   assert(history[4] == CITY_UNKNOWN);
   assert(history[5] == MADRID);
   disposeHunterView(hv);


   //after ending turn at castle dracula
   char *trail9 = "GLO.... SBU.... HST.... MBA.... DC?.V.. "
                  "GSW.... SST.... HFR.... MTO.... DCDT... "
                  "GIR.... SZU.... HCO.... MMR.... DD1T... "
                  "GLV.... SMU.... HCO.... ";
   hv = newHunterView(trail9, messages);
   giveMeTheTrail (hv, PLAYER_DRACULA, history);
   assert (history[0] == DOUBLE_BACK_1);
   assert (history[1] == CASTLE_DRACULA);
   assert (history[2] == CITY_UNKNOWN);
   assert (history[3] == UNKNOWN_LOCATION);
   disposeHunterView(hv);

   return 1;
}


/*
* Test their Trails can resolve locations (HIDE)
*/
static int bbTest15(void) {
    //BEFORE WITH HIDE in trail
    char *trail3 = "GLO.... SBU.... HST.... MBA.... DS?.V.. "
                    "GSW.... SST.... HFR.... MTO.... DC?T... "
                    "GIR.... SZU.... HCO.... MMR.... DHIT... "
                    "GLV.... SMU.... ";
    PlayerMessage messages[] = {""};
    LocationID history[TRAIL_SIZE];
    HunterView hv = newHunterView(trail3, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == LIVERPOOL);
    assert (history[1] == IRISH_SEA);
    assert (history[2] == SWANSEA);
    assert (history[3] == LONDON);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == MUNICH);
    assert (history[1] == ZURICH);
    assert (history[2] == STRASBOURG);
    assert (history[3] == BRUSSELS);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == COLOGNE);
    assert (history[1] == FRANKFURT);
    assert (history[2] == STRASBOURG);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == MARSEILLES);
    assert (history[1] == TOULOUSE);
    assert (history[2] == BARCELONA);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == HIDE);//Cannot resolve the HIDE
    assert (history[1] == CITY_UNKNOWN);
    assert (history[2] == SEA_UNKNOWN);
    assert (history[3] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    //after encounter
    char *trail4 = "GLO.... SBU.... HST.... MBA.... DNS.V.. "
                   "GSW.... SST.... HFR.... MTO.... DAMT... "
                   "GIR.... SZU.... HCO.... MMR.... DHIT... "
                   "GLV.... SMU.... HAMTVD. ";
    hv = newHunterView(trail4, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == LIVERPOOL);
    assert (history[1] == IRISH_SEA);
    assert (history[2] == SWANSEA);
    assert (history[3] == LONDON);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DR_SEWARD, history);
    assert (history[0] == MUNICH);
    assert (history[1] == ZURICH);
    assert (history[2] == STRASBOURG);
    assert (history[3] == BRUSSELS);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_VAN_HELSING, history);
    assert (history[0] == AMSTERDAM);
    assert (history[1] == COLOGNE);
    assert (history[2] == FRANKFURT);
    assert (history[3] == STRASBOURG);
    assert (history[4] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_MINA_HARKER, history);
    assert (history[0] == MARSEILLES);
    assert (history[1] == TOULOUSE);
    assert (history[2] == BARCELONA);
    assert (history[3] == UNKNOWN_LOCATION);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == HIDE); //Resolves the HIDE
    assert (history[1] == AMSTERDAM);
    assert (history[2] == NORTH_SEA);
    assert (history[3] == UNKNOWN_LOCATION);
    disposeHunterView(hv);


    //after ending turn at castle dracula
    char *trail5 = "GLO.... SBU.... HST.... MBA.... DC?.V.. "
                   "GSW.... SST.... HFR.... MTO.... DCDT... "
                   "GIR.... SZU.... HCO.... MMR.... DHIT... "
                   "GLV.... SMU.... HCO.... ";
    hv = newHunterView(trail5, messages);
    giveMeTheTrail (hv, PLAYER_DRACULA, history);
    assert (history[0] == HIDE); //Resolves the HIDE without an encounter
    assert (history[1] == CASTLE_DRACULA);
    assert (history[2] == CITY_UNKNOWN);
    assert (history[3] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    return 1;
}


/*
* Test their Trails can resolve locations (TELEPORT)
*/
static int bbTest16(void) {
    //teleport hunter to the hospital
    char *trail5 = "GLO.... SMR.... HCD.... MAM.... DSW.V.. "
                    "GSWT... SCF.... HGA.... MCO.... DMNT... "
                    "GLV.... SMR.... HCD.... MAM.... DHIT... "
                    "GMNTTD. SCF.... HGA.... ";
    PlayerMessage messages[] = {""};
    LocationID history[TRAIL_SIZE];
    HunterView hv = newHunterView(trail5, messages);
    giveMeTheTrail (hv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == MANCHESTER); //FROM MANCHESTER
    assert (history[2] == SWANSEA);
    assert (history[1] == LIVERPOOL);
    assert (history[3] == LONDON);
    assert (history[4] == UNKNOWN_LOCATION);
    assert (history[5] == UNKNOWN_LOCATION);
    disposeHunterView(hv);

    //Dracula teleports to Castle DRACULA,
    char *trail6 = "GMN.... SGE.... HMU.... MBU.... DBD.V.. "
                    "GLO.... SMR.... HZU.... MAM.... DTPT... "
                    "GSWT.... SCF.... HST.... ";
    hv = newHunterView(trail6, messages);
    giveMeTheTrail(hv, PLAYER_DRACULA, history);
    assert(history[0] == TELEPORT);
    assert(whereIs(hv,PLAYER_DRACULA) == CASTLE_DRACULA);
    assert(history[1] == BUDAPEST);
    assert(history[2] == UNKNOWN_LOCATION);
    assert(history[3] == UNKNOWN_LOCATION);
    assert(history[4] == UNKNOWN_LOCATION);
    assert(history[5] == UNKNOWN_LOCATION);
    disposeHunterView(hv);
    return 1;
}


/*
 * Test 17: whereCanIgo before first move
 */
static int bbTest17(void) {
    //for Godalming
    char *trail = "";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);

    int size = 0;
    LocationID *edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeHunterView(hv);

    //for Seward
    char *trail2 = "GLO.... ";
    hv = newHunterView(trail2, messages);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);

    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeHunterView(hv);

    //for Helsing
    char *trail3 = "GLO.... SZU.... ";
    hv = newHunterView(trail3, messages);
    assert(whoAmI(hv) == PLAYER_VAN_HELSING);

    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeHunterView(hv);

    //for Mina
    char *trail4 = "GLO.... SZU.... HMR.... ";
    hv = newHunterView(trail4, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);

    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeHunterView(hv);

    return 1;
}

/*
 * Test 18: whereCanIgo (road only)
 */
static int bbTest18(void) {
    //for Godalming
    char *trail = "GLO.... SMR.... HAO.... MGA.... DC?.V.. ";

    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);

    int size = 0;
    LocationID *edges = whereCanIgo(hv, &size, true, false, false);
    assert(size == 4);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[LONDON]);
    assert(seen[SWANSEA]);
    assert(seen[MANCHESTER]);
    assert(seen[PLYMOUTH]);

    free(edges);
    disposeHunterView(hv);

    //for Seward
    char *trail2 ="GLO.... SMR.... HAO.... MGA.... DC?.V.. GMN.... ";
    hv = newHunterView(trail2, messages);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);

    edges = whereCanIgo(hv, &size, true, false, false);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[MARSEILLES]);
    assert(seen2[TOULOUSE]);
    assert(seen2[CLERMONT_FERRAND]);
    assert(seen2[GENEVA]);
    assert(seen2[ZURICH]);
    assert(seen2[MILAN]);
    assert(seen2[GENOA]);

    free(edges);
    disposeHunterView(hv);

    //for Helsing
    char *trail3 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... SGE.... ";
    hv = newHunterView(trail3, messages);
    assert(whoAmI(hv) == PLAYER_VAN_HELSING);

    edges = whereCanIgo(hv, &size, true, false, false);
    assert(size == 1);
    free(edges);
    disposeHunterView(hv);

    //for Mina
    char *trail4 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... SGE.... HAO.... ";
    hv = newHunterView(trail4, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);

    edges = whereCanIgo(hv, &size, true, false, false);
    assert(size == 5);

    bool seen4[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen4[edges[i]] = true;

    assert(seen4[GALATZ]);
    assert(seen4[CASTLE_DRACULA]);
    assert(seen4[CONSTANTA]);
    assert(seen4[BUCHAREST]);
    assert(seen4[KLAUSENBURG]);

    free(edges);
    disposeHunterView(hv);

    return 1;
}

/*
 * Test 19: whereCanIgo (sea only)
 */
static int bbTest19(void) {

    //for Godalming
    char *trail = "GAT.... SAO.... HZU.... MCG.... DC?.V.. ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanIgo(hv, &size, false, false, true);
    assert(size == 2);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[ATHENS]);
    assert(seen[IONIAN_SEA]);

    free(edges);
    disposeHunterView(hv);

    //for Seward
    char *trail2 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. GIO.... ";
    hv = newHunterView(trail2, messages);

    edges = whereCanIgo(hv, &size, false, false, true);
    assert(size == 9);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[ATLANTIC_OCEAN]);
    assert(seen2[NORTH_SEA]);
    assert(seen2[GALWAY]);
    assert(seen2[IRISH_SEA]);
    assert(seen2[ENGLISH_CHANNEL]);
    assert(seen2[BAY_OF_BISCAY]);
    assert(seen2[LISBON]);
    assert(seen2[CADIZ]);
    assert(seen2[MEDITERRANEAN_SEA]);

    free(edges);
    disposeHunterView(hv);

    //for Helsing
    char *trail3 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. "
                    "GIO.... SGW.... ";
    hv = newHunterView(trail3, messages);

    edges = whereCanIgo(hv, &size, false, false, true);
    assert(size == 1);

    free(edges);
    disposeHunterView(hv);

    //for Mina
    char *trail4 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. "
                    "GIO.... SGW.... HZU.... ";
    hv = newHunterView(trail4, messages);

    edges = whereCanIgo(hv, &size, false, false, true);
    assert(size == 3);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[CAGLIARI]);
    assert(seen3[MEDITERRANEAN_SEA]);
    assert(seen3[TYRRHENIAN_SEA]);

    free(edges);
    disposeHunterView(hv);

    return 1;
}

/*
 * Test 20: whereCanIgo (rail only)
 */
static int bbTest20(void) {

    //1 rail hop allowed
    char *trail = "GPA.... SPA.... HPA.... MPA.... DC?.V.. ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanIgo(hv, &size, false, true, false);
    assert(size == 5);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[PARIS]);
    assert(seen[MARSEILLES]);
    assert(seen[BORDEAUX]);
    assert(seen[LE_HAVRE]);
    assert(seen[BRUSSELS]);

    free(edges);
    disposeHunterView(hv);

    //2 rail hops allowed
    char *trail2 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. GMR.... ";
    hv = newHunterView(trail2, messages);
    edges = whereCanIgo(hv, &size, false, true, false);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[PARIS]);
    assert(seen2[LE_HAVRE]);
    assert(seen2[BORDEAUX]);
    assert(seen2[MARSEILLES]);
    assert(seen2[BRUSSELS]);
    assert(seen2[COLOGNE]);
    assert(seen2[SARAGOSSA]);

    free(edges);
    disposeHunterView(hv);

    //3 rail hops allowed
    char *trail3 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. "
                    "GMR.... SCO.... ";
    hv = newHunterView(trail3, messages);
    edges = whereCanIgo(hv, &size, false, true, false);
    assert(size == 10);
    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[PARIS]);
    assert(seen3[LE_HAVRE]);
    assert(seen3[BORDEAUX]);
    assert(seen3[MARSEILLES]);
    assert(seen3[BRUSSELS]);
    assert(seen3[COLOGNE]);
    assert(seen3[SARAGOSSA]);
    assert(seen3[MADRID]);
    assert(seen3[BARCELONA]);
    assert(seen3[FRANKFURT]);

    free(edges);
    disposeHunterView(hv);

    //No rail hops allowed
    char *trail4 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. "
                    "GMR.... SCO.... HFR.... ";
    hv = newHunterView(trail4, messages);
    edges = whereCanIgo(hv, &size, false, true, false);
    assert(size == 1);

    free(edges);
    disposeHunterView(hv);

    return 1;
}

/*
 * Test 21: whereCanIgo (combination of all)
 */
static int bbTest21(void) {

    char *trail = "GHA.... SIO.... HST.... MMA.... DKL.V.. ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    int size = 0;
    //1 rail hop allowed
    LocationID *edges = whereCanIgo(hv, &size, true, true, true);
    assert(whereIs(hv, PLAYER_LORD_GODALMING) == HAMBURG);
    assert(size == 5);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(seen[HAMBURG]);
    assert(seen[BERLIN]);
    assert(seen[LEIPZIG]);
    assert(seen[COLOGNE]);
    assert(seen[NORTH_SEA]);

    free(edges);
    disposeHunterView(hv);

    //2 rail hops allowed
    char *trail2 = "GHA.... SIO.... HST.... MMA.... DKL.V.. GLI.... ";
    hv = newHunterView(trail2, messages);
    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[IONIAN_SEA]);
    assert(seen2[TYRRHENIAN_SEA]);
    assert(seen2[ADRIATIC_SEA]);
    assert(seen2[VALONA]);
    assert(seen2[ATHENS]);
    assert(seen2[SALONICA]);
    assert(seen2[BLACK_SEA]);

    free(edges);
    disposeHunterView(hv);

    //3 rail hops allowed
    char *trail3 = "GHA.... SIO.... HST.... MMA.... DKL.V.. "
                    "GLI.... SAS.... ";
    hv = newHunterView(trail3, messages);
    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == 14);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[STRASBOURG]);
    assert(seen3[GENEVA]);
    assert(seen3[PARIS]);
    assert(seen3[BRUSSELS]);
    assert(seen3[COLOGNE]);
    assert(seen3[FRANKFURT]);
    assert(seen3[NUREMBURG]);
    assert(seen3[MUNICH]);
    assert(seen3[ZURICH]);
    assert(seen3[MILAN]);
    assert(seen3[GENOA]);
    assert(seen3[FLORENCE]);
    assert(seen3[LEIPZIG]);
    assert(seen3[BERLIN]);

    free(edges);
    disposeHunterView(hv);

    //No rail hops
    char *trail4 = "GHA.... SIO.... HST.... MMA.... DKL.V.. "
                    "GLI.... SAS.... HBR.... ";
    hv = newHunterView(trail4, messages);
    edges = whereCanIgo(hv, &size, true, true, true);
    assert(size == 7);

    bool seen4[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen4[edges[i]] = true;

    assert(seen4[MADRID]);
    assert(seen4[LISBON]);
    assert(seen4[CADIZ]);
    assert(seen4[GRANADA]);
    assert(seen4[ALICANTE]);
    assert(seen4[SARAGOSSA]);
    assert(seen4[SANTANDER]);

    free(edges);
    disposeHunterView(hv);

    return 1;
}

/*
* Where can they go first move
*/
static int bbTest22(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);

    int size = 0;
    LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_MINA_HARKER, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, false, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    disposeHunterView(hv);

    //for Seward looking at Godalming
    char *trail2 = "GLO.... ";
    hv = newHunterView(trail2, messages);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);

    edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, true, true, true);
    assert(size == 5);
    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[LONDON]);
    assert(seen[PLYMOUTH]);
    assert(seen[SWANSEA]);
    assert(seen[MANCHESTER]);
    assert(seen[ENGLISH_CHANNEL]);
    free(edges);
    disposeHunterView(hv);

    //for Helsing looking at godalming (ensure same as seward)
    char *trail3 = "GLO.... SZU.... ";
    hv = newHunterView(trail3, messages);
    assert(whoAmI(hv) == PLAYER_VAN_HELSING);
    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    free(edges);
    edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, true, true, true);
    assert(size == 5);
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[LONDON]);
    assert(seen[PLYMOUTH]);
    assert(seen[SWANSEA]);
    assert(seen[MANCHESTER]);
    assert(seen[ENGLISH_CHANNEL]);
    free(edges);
    disposeHunterView(hv);

    return 1;
}



/*
* Where can they go (Road only)
*/
static int bbTest23(void) {
    char *trail = "GLO.... SMR.... HAO.... MGA.... DC?.V.. GLO.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);

    int size = 0;
    LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, true, false, false);
    //this should list london, swansea, manchester and plymouth as roads, but they dont show up...
    //it thinks its already in the list?
    assert(size == 4);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(seen[LONDON]);
    assert(seen[SWANSEA]);
    assert(seen[MANCHESTER]);
    assert(seen[PLYMOUTH]);

    free(edges);
    disposeHunterView(hv);

    //for Seward
    char *trail2 ="GLO.... SMR.... HAO.... MGA.... DC?.V.. ";
    hv = newHunterView(trail2, messages);
    assert(whoAmI(hv) == PLAYER_LORD_GODALMING);

    edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD, true, false, false);
    assert(size == 7);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[MARSEILLES]);
    assert(seen2[TOULOUSE]);
    assert(seen2[CLERMONT_FERRAND]);
    assert(seen2[GENEVA]);
    assert(seen2[ZURICH]);
    assert(seen2[MILAN]);
    assert(seen2[GENOA]);

    free(edges);
    disposeHunterView(hv);

    //for Helsing
    char *trail3 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... ";
    hv = newHunterView(trail3, messages);
    assert(whoAmI(hv) == PLAYER_DR_SEWARD);

    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING,true, false, false);
    assert(size == 1);
    free(edges);
    disposeHunterView(hv);

    //for Mina
    char *trail4 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... SGE.... ";
    hv = newHunterView(trail4, messages);
    assert(whoAmI(hv) == PLAYER_VAN_HELSING);

    edges = whereCanTheyGo(hv, &size,PLAYER_MINA_HARKER, true, false, false);
    assert(size == 5);

    bool seen4[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen4[edges[i]] = true;

    assert(seen4[GALATZ]);
    assert(seen4[CASTLE_DRACULA]);
    assert(seen4[CONSTANTA]);
    assert(seen4[BUCHAREST]);
    assert(seen4[KLAUSENBURG]);

    free(edges);
    disposeHunterView(hv);

    //for Dracula (from city unknown)
    char *trail5 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... SGE.... MCD....";
    hv = newHunterView(trail5, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);

    //we dont know where he can go as his location is unknown
    edges = whereCanTheyGo(hv, &size,PLAYER_DRACULA, true, false, false);
    assert(size == 0);
    disposeHunterView(hv);


    //for Dracula (from sea unknown)
    char *trail6 = "GLO.... SMR.... HAO.... MGA.... DS?.V.. "
                    "GMN.... SGE.... HBB.... ";
    hv = newHunterView(trail6, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);
    //nowhere to go - at sea, but cannot travel by boat...
    edges = whereCanTheyGo(hv, &size,PLAYER_DRACULA, true, false, false);
    assert(size == 0);
    assert(edges == NULL);
    disposeHunterView(hv);

    //for Dracula (from known city)
    char *trail7 = "GLO.... SMR.... HAO.... MGA.... DMN.V.. "
                    "GMN.... SGE.... HBB.... ";
    hv = newHunterView(trail7, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, false, false);
    assert(size == 4);

    bool seen5[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen5[edges[i]] = true;

    assert(seen[MANCHESTER]);
    assert(seen5[LIVERPOOL]);
    assert(seen5[LONDON]);
    assert(seen5[EDINBURGH]);

    free(edges);
    disposeHunterView(hv);

    //for Dracula (from known city)
    char *trail8 = "GLO.... SMR.... HAO.... MGA.... DBB.V.. "
                    "GMN.... SGE.... HBB.... ";
    hv = newHunterView(trail8, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, false, false);
    assert(size == 1);
    assert(edges[0] == BAY_OF_BISCAY);

    free(edges);
    disposeHunterView(hv);

    return 1;
}


/*
* Where can they go (Sea only)
*/
static int bbTest24(void) {
    //for Godalming
    char *trail = "GAT.... SAO.... HZU.... ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);
    int size = 0;
    LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, false, false, true);
    assert(size == 2);
    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[ATHENS]);
    assert(seen[IONIAN_SEA]);
    free(edges);
    disposeHunterView(hv);

    //for Seward
    char *trail2 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. ";
    hv = newHunterView(trail2, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD,false, false, true);
    assert(size == 9);
    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[ATLANTIC_OCEAN]);
    assert(seen2[NORTH_SEA]);
    assert(seen2[GALWAY]);
    assert(seen2[IRISH_SEA]);
    assert(seen2[ENGLISH_CHANNEL]);
    assert(seen2[BAY_OF_BISCAY]);
    assert(seen2[LISBON]);
    assert(seen2[CADIZ]);
    assert(seen2[MEDITERRANEAN_SEA]);
    free(edges);
    disposeHunterView(hv);

    //for Helsing
    char *trail3 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. "
                    "GIO.... ";
    hv = newHunterView(trail3, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING,false, false, true);
    assert(size == 1);
    free(edges);
    disposeHunterView(hv);

    //for Mina
    char *trail4 = "GAT.... SAO.... HZU.... MCG.... DC?.V.. "
                    "GIO.... SGW.... ";
    hv = newHunterView(trail4, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_MINA_HARKER,false, false, true);
    assert(size == 3);
    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[CAGLIARI]);
    assert(seen3[MEDITERRANEAN_SEA]);
    assert(seen3[TYRRHENIAN_SEA]);
    free(edges);
    disposeHunterView(hv);


    //for Dracula (from city unknown)
    char *trail5 = "GLO.... SMR.... HAO.... MGA.... DC?.V.. "
                    "GMN.... SGE.... MCD....";
    hv = newHunterView(trail5, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);
    //he can only go to oceans because he is already on land
    edges = whereCanTheyGo(hv, &size,PLAYER_DRACULA, false, false, true);
    assert(size == 0);assert(edges == NULL);
    disposeHunterView(hv);


    //for Dracula (from sea unknown)
    char *trail6 = "GLO.... SMR.... HAO.... MGA.... DS?.V.. "
                    "GMN.... SGE.... MCD....";
    hv = newHunterView(trail6, messages);
    assert(whoAmI(hv) == PLAYER_MINA_HARKER);
    edges = whereCanTheyGo(hv, &size,PLAYER_DRACULA, false, false, true);
    assert(size == 0);
    assert(edges == NULL);
    disposeHunterView(hv);

    return 1;
}



/*
 * Where can they go (Rail only)
 */
static int bbTest25(void) {

        //1 rail hop allowed
        char *trail = "GPA.... SPA.... HPA.... ";
        PlayerMessage messages[] = {""};
        HunterView hv = newHunterView(trail, messages);
        int size = 0;
        LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_LORD_GODALMING, false, true, false);
        assert(size == 5);

        bool seen[NUM_MAP_LOCATIONS] = {false};
        for(int i = 0; i < size; i++)
            seen[edges[i]] = true;

        assert(seen[PARIS]);
        assert(seen[MARSEILLES]);
        assert(seen[BORDEAUX]);
        assert(seen[LE_HAVRE]);
        assert(seen[BRUSSELS]);
        free(edges);
        disposeHunterView(hv);

        //2 rail hops allowed
        char *trail2 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. ";
        hv = newHunterView(trail2, messages);
        edges = whereCanTheyGo(hv, &size,PLAYER_DR_SEWARD, false, true, false);
        assert(size == 7);

        bool seen2[NUM_MAP_LOCATIONS] = {false};
        for(int i = 0; i < size; i++)
            seen2[edges[i]] = true;

        assert(seen2[PARIS]);
        assert(seen2[LE_HAVRE]);
        assert(seen2[BORDEAUX]);
        assert(seen2[MARSEILLES]);
        assert(seen2[BRUSSELS]);
        assert(seen2[COLOGNE]);
        assert(seen2[SARAGOSSA]);
        free(edges);
        disposeHunterView(hv);

        //3 rail hops allowed
        char *trail3 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. "
                        "GMR.... ";
        hv = newHunterView(trail3, messages);
        edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING, false, true, false);
        assert(size == 10);
        bool seen3[NUM_MAP_LOCATIONS] = {false};
        for(int i = 0; i < size; i++)
            seen3[edges[i]] = true;

        assert(seen3[PARIS]);
        assert(seen3[LE_HAVRE]);
        assert(seen3[BORDEAUX]);
        assert(seen3[MARSEILLES]);
        assert(seen3[BRUSSELS]);
        assert(seen3[COLOGNE]);
        assert(seen3[SARAGOSSA]);
        assert(seen3[MADRID]);
        assert(seen3[BARCELONA]);
        assert(seen3[FRANKFURT]);

        free(edges);
        disposeHunterView(hv);

        //No rail hops allowed
        char *trail4 = "GPA.... SPA.... HPA.... MPA.... DC?.V.. "
                        "GMR.... SCO.... ";
        hv = newHunterView(trail4, messages);
        edges = whereCanTheyGo(hv, &size, PLAYER_MINA_HARKER, false, true, false);
        assert(size == 1);

        free(edges);
        disposeHunterView(hv);

    return 1;
}

/*
 * Test 26: whereCanTheyGo (combination of all)
 */
static int bbTest26(void) {

    char *trail = "GPA.... SRO.... HMI.... MAO.... DC?.V.. ";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    //2 rail hops allowed
    int size = 0;
    LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(whereIs(hv, PLAYER_DR_SEWARD) == ROME);
    assert(size == 6);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[ROME]);
    assert(seen[NAPLES]);
    assert(seen[BARI]);
    assert(seen[FLORENCE]);
    assert(seen[MILAN]);
    assert(seen[TYRRHENIAN_SEA]);

    free(edges);

    //3 rail hops allowed
    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING, true, true, true);
    assert(whereIs(hv, PLAYER_VAN_HELSING) == MILAN);
    assert(size == 12);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[GENOA]);
    assert(seen2[MARSEILLES]);
    assert(seen2[GENEVA]);
    assert(seen2[ZURICH]);
    assert(seen2[STRASBOURG]);
    assert(seen2[FRANKFURT]);
    assert(seen2[MUNICH]);
    assert(seen2[VENICE]);
    assert(seen2[FLORENCE]);
    assert(seen2[ROME]);
    assert(seen2[MILAN]);
    assert(seen2[NAPLES]);

    free(edges);
    disposeHunterView(hv);

    //3 rail hops allowed
    char *trail2 = "GPA.... SRO.... HMI.... MAO.... DC?.V.. "
                    "GCF.... STS.... HFR.... ";
    hv = newHunterView(trail2, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DR_SEWARD, true, true, true);
    assert(size == 7);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[TYRRHENIAN_SEA]);
    assert(seen3[MEDITERRANEAN_SEA]);
    assert(seen3[IONIAN_SEA]);
    assert(seen3[CAGLIARI]);
    assert(seen3[ROME]);
    assert(seen3[NAPLES]);
    assert(seen3[GENOA]);

    free(edges);

    //No rail hops allowed
    edges = whereCanTheyGo(hv, &size, PLAYER_VAN_HELSING, true, true, true);
    int size2 = 0;
    LocationID *edges2 = whereCanTheyGo(hv, &size2, PLAYER_VAN_HELSING, true, false, false);
    assert(size == size2);
    for(int i = 0; i < size; i++)
        assert(edges[i] == edges2[i]);
        //ie, for no rail hops and inland, same as jsut travelling by road

    free(edges);
    free(edges2);
    disposeHunterView(hv);

    return 1;
}

/*
 * Test 27: Where can dracula go? (special cases)
 */
static int bbTest27(void) {

    // whereCanTheyGo(dracula) returns empty array if location is unknown
    //     and round is not 0
    char *trail = "";
    PlayerMessage messages[] = {""};
    HunterView hv = newHunterView(trail, messages);

    int size = 0;
    LocationID *edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == NUM_MAP_LOCATIONS);
    assert(edges != NULL);

    free(edges);
    disposeHunterView(hv);

    char *trail2 = "GLO.... SIO.... HSO.... MFR.... DC?.V.. ";
    hv = newHunterView(trail2, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == 0);
    assert(edges == NULL);

    disposeHunterView(hv);

    //checking location is determined from double backs
    char *trail3 = "GSW.... SIO.... HSO.... MFR.... DLV.V.. "
                    "GLVD... SAT.... HBC.... MLI.... DC?T... "
                    "GMN.... SVA.... HSZ.... MBR.... DD2T.... ";
    hv = newHunterView(trail3, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == 4);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen[edges[i]] = true;

    assert(seen[LIVERPOOL]);
    assert(seen[SWANSEA]);
    assert(seen[MANCHESTER]);
    assert(seen[IRISH_SEA]);

    free(edges);
    disposeHunterView(hv);

    //check location is determined from hides
    char *trail4 = "GSW.... SIO.... HSO.... MFR.... DC?.V.. "
                    "GLVD... SAT.... HBC.... MLI.... DMNT... "
                    "GMN.... SVA.... HSZ.... MBR.... DHIT.... ";
    hv = newHunterView(trail4, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == 4);

    bool seen2[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen2[edges[i]] = true;

    assert(seen2[MANCHESTER]);
    assert(seen2[LIVERPOOL]);
    assert(seen2[EDINBURGH]);
    assert(seen2[LONDON]);

    free(edges);
    disposeHunterView(hv);

    //check teleport is converted to Castle Dracula
    char *trail5 = "GSW.... SIO.... HSO.... MFR.... DTP.V.. ";
    hv = newHunterView(trail5, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == 3);

    bool seen3[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen3[edges[i]] = true;

    assert(seen3[CASTLE_DRACULA]);
    assert(seen3[GALATZ]);
    assert(seen3[KLAUSENBURG]);

    free(edges);
    disposeHunterView(hv);

    //check that teleport (castle dracula) is listed as an option if no other options are possible
    char *trail6 = "GMN.... SGE.... HBA.... MSO.... DAO.V.. "
                   "GLV.... SZU.... HAL.... MVR.... DGWT... "
                   "GIR.... SMU.... HMA.... MCN.... DHIT... "
                   "GAO.... SVI.... HCA.... MBC.... DDUT... "
                   "GEC.... SZA.... HLS.... MKL.... DD3T... "
                   "GLE.... SJM.... HAO.... MCD.... ";
    hv = newHunterView(trail6, messages);
    edges = whereCanTheyGo(hv, &size, PLAYER_DRACULA, true, true, true);
    assert(size == 2);

    bool seen4[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++)
        seen4[edges[i]] = true;

    assert(seen4[GALWAY]);
    assert(seen4[CASTLE_DRACULA]);

    free(edges);
    disposeHunterView(hv);

    return 1;
}
