////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// testGameView.c: test the GameView ADT
//
// 2018-01-19 -> Team Blade
// Keiran Sampson (Z5168147)
// Daniel Woolnough (Z5116128)
////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "GameView.h"

#define COLOR_RED     "\x1b[91m"
#define COLOR_GREEN   "\x1b[92m"
#define COLOR_CYAN    "\x1b[96m"
#define COLOR_RESET   "\x1b[0m"
#define COLOR_BLUE    "\x1b[94m"
#define LINE "***************************************************************************\n"
#define PRINT_SUCC printf(COLOR_GREEN"COMPLETED!"COLOR_RESET"\n");
#define PRINT_FAIL printf(COLOR_RED"INCOMPLETE!"COLOR_RESET"\n");


static void blackBoxTests(void);
static int bbTest1(void);
static int bbTest2(void);
static int bbTest3(void);
static int bbTest4(void);
static int bbTest5(void);
static int bbTest6(void);
static int bbTest7(void);
static int bbTest8(void);
static int bbTest9(void);
static int bbTest10(void);
static int bbTest11(void);
static int bbTest12(void);
static int bbTest13(void);
static int bbTest14(void);
static int bbTest15(void);
static int bbTest16(void);
static int bbTest17(void);
static int bbTest18(void);
static int bbTest19(void);
static int bbTest20(void);
static int bbTest21(void);
static int bbTest22(void);

int main (int argc, char *ardv[]) {

    setbuf(stdout,NULL);
    /***********************
    *   FUNCTION TESTING  *
    ************************/
    printf(LINE);
    printf(LINE);
    printf(COLOR_CYAN"Running GameView Tests!"COLOR_RESET"\n");
    printf(LINE);
    printf(LINE);

    /***********************
    *   BLACKBOX TESTING  *
    ************************/
    printf(COLOR_CYAN"Black Box tests:"COLOR_RESET"\n");
    printf(LINE);
    blackBoxTests();
}

static void bb_tests(int (*testFunction) (void),char *msg)
{
    printf("%s",msg);
    if(testFunction()){
        PRINT_SUCC
    }else{
        PRINT_FAIL
    }
}

static void blackBoxTests(void) {
        bb_tests(bbTest1,"Test 1: Basic initialisation\t\t\t\t\t");
        bb_tests(bbTest2,"Test 2: Basic Functions\t\t\t\t\t\t");
        bb_tests(bbTest3,"Test 3: Extended Play(no health changes)\t\t\t");
        bb_tests(bbTest4,"Test 4: After One Move\t\t\t\t\t\t");
        bb_tests(bbTest5,"Test 5: Dracula encounter\t\t\t\t\t");
        bb_tests(bbTest6,"Test 6: Trap encounter\t\t\t\t\t\t");
        bb_tests(bbTest7,"Test 7: Vampire encounter\t\t\t\t\t");
        bb_tests(bbTest8,"Test 8: Hunter loses all health (and gets it back)\t\t");
        bb_tests(bbTest9,"Test 9: Dracula loses health at sea\t\t\t\t");
        bb_tests(bbTest10,"Test 10: Dracula regains health\t\t\t\t\t");
        bb_tests(bbTest11,"Test 11: Hunter gains health\t\t\t\t\t");
        bb_tests(bbTest12,"Test 12: Game History\t\t\t\t\t\t");
        bb_tests(bbTest13,"Test 13: Dracula double back at sea\t\t\t\t");
        bb_tests(bbTest14,"Test 14: Vampire Matures\t\t\t\t\t");
        bb_tests(bbTest15,"Test 15: Basic Road Connections\t\t\t\t\t");
        bb_tests(bbTest16,"Test 16: Basic Boat connections\t\t\t\t\t");
        bb_tests(bbTest17,"Test 17: Basic Rail connections (0 hops allowed)\t\t");
        bb_tests(bbTest18,"Test 18: Basic Rail connections (1 hop allowed)\t\t\t");
        bb_tests(bbTest19,"Test 19: Basic Rail connections (2 hops allowed)\t\t");
        bb_tests(bbTest20,"Test 20: Basic Rail connections (3 hops allowed)\t\t");
        bb_tests(bbTest21,"Test 21: All three connections\t\t\t\t\t");
        bb_tests(bbTest22,"Test 22: getLocation after Dracula Teleport\t\t\t");

        printf(LINE);
        printf(LINE);
        printf(COLOR_CYAN"All tests completed!"COLOR_RESET"\n");
        printf(LINE);
        printf(LINE);
    }

/*
* Basic Initialisation Tests
*/
static int bbTest1(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    assert(getScore(gv) == GAME_START_SCORE);
    assert(getRound (gv) == 0);
    assert(getCurrentPlayer (gv) == PLAYER_LORD_GODALMING);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth (gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth(gv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth(gv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(getLocation (gv, PLAYER_LORD_GODALMING) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_DR_SEWARD) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_VAN_HELSING) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_MINA_HARKER) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_DRACULA) == UNKNOWN_LOCATION);

    disposeGameView (gv);
    return 1;
}


/*
* Basic Function Tests
*/
static int bbTest2(void) {
    char *trail = "GST.... SAO.... HZU.... MBB.... DC?....";
    PlayerMessage messages[] = {"Hello", "Rubbish", "Stuff", "", "Mwahahah"};
    GameView gv = newGameView (trail, messages);

    assert (getCurrentPlayer (gv) == PLAYER_LORD_GODALMING);
    assert (getRound (gv) == 1);
    assert (getLocation (gv, PLAYER_LORD_GODALMING) == STRASBOURG);
    assert (getLocation (gv, PLAYER_DR_SEWARD) == ATLANTIC_OCEAN);
    assert (getLocation (gv, PLAYER_VAN_HELSING) == ZURICH);
    assert (getLocation (gv, PLAYER_MINA_HARKER) == BAY_OF_BISCAY);
    assert (getLocation (gv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));

    disposeGameView (gv);
    return 1;
}


/*
* Moving around the game Board (no health changes)
*/
static int bbTest3(void) {
    char *trail = "GHA.... SLO.... HLS.... MCF.... DC?.... GBR.... SSW.... \
                     HMA.... MTO.... DC?.... GLI.... SLV.... HCA.... MMR.... \
                     DC?.... GHA.... SMN.... HLS.... MCF.... DC?.... ";
    PlayerMessage messages[20];
    for (int i = 0; i < 20; i++) {
        strcpy(messages[i], "random");
    }
    GameView gv = newGameView(trail, messages);

    assert (getRound (gv) == 4);
    assert (getCurrentPlayer (gv) == PLAYER_LORD_GODALMING);
    assert (getLocation (gv, PLAYER_LORD_GODALMING) == HAMBURG);
    assert (getLocation (gv, PLAYER_DR_SEWARD) == MANCHESTER);
    assert (getLocation (gv, PLAYER_VAN_HELSING) == LISBON);
    assert (getLocation (gv, PLAYER_MINA_HARKER) == CLERMONT_FERRAND);
    assert (getLocation (gv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getHealth (gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (getHealth (gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert (getHealth (gv, PLAYER_VAN_HELSING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (getHealth (gv, PLAYER_MINA_HARKER) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));

    disposeGameView(gv);
    return 1;
}


/*
* After a single move test each player
*/
static int bbTest4(void) {
    char *trail = "GLO.... ";
    PlayerMessage messages[] = {"random"};
    GameView gv = newGameView(trail, messages);

    assert(getCurrentPlayer(gv) == PLAYER_DR_SEWARD);
    assert(getRound(gv) == 0);
    assert(getLocation(gv, PLAYER_LORD_GODALMING) == LONDON);
    assert(getLocation(gv, PLAYER_DR_SEWARD) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_VAN_HELSING) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_MINA_HARKER) == UNKNOWN_LOCATION);
    assert(getLocation(gv, PLAYER_DRACULA) == UNKNOWN_LOCATION);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));

    disposeGameView(gv);
    return 1;
}


/*
* A dracula encounter
*/
static int bbTest5(void) {
    char *trail = "GST.... SAO.... HCD.... MAO.... DGE.... "
                    "GGED...";
    PlayerMessage messages[] = {
        "Hello", "Rubbish",  "Stuff", "", "Mwahahah", "Aha!"};
    GameView gv = newGameView (trail, messages);

    assert (getLocation (gv, PLAYER_DRACULA) == GENEVA);
    assert (getHealth (gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_DRACULA_ENCOUNTER);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert (getLocation (gv, PLAYER_LORD_GODALMING) == GENEVA);

    disposeGameView (gv);

    //check dracula encounters are only read from string
    //not hardcoded in (even if string is invalid play);
    char *trail2 = "GST.... SAO.... HCD.... MAO.... DGE.... "
                    "GGE....";
    gv = newGameView (trail2, messages);

    assert (getLocation (gv, PLAYER_DRACULA) == GENEVA);
    assert (getHealth (gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert (getLocation (gv, PLAYER_LORD_GODALMING) == GENEVA);

    disposeGameView (gv);
    return 1;
}


/*
* A trap encounter
*/
static int bbTest6(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DC?T... GLVT... ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    GameView gv = newGameView(trail, messages);
    assert(getLocation(gv, PLAYER_DRACULA) == CITY_UNKNOWN);
    assert(getLocation(gv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(getHealth(gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) ==
        GAME_START_HUNTER_LIFE_POINTS - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeGameView(gv);

    //check trap encounters are only read from string
    //not hardcoded in (even if string is invalid play);
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DLVT... GLV.... ";
    gv = newGameView (trail2, messages);
    assert(getLocation(gv, PLAYER_DRACULA) == LIVERPOOL);
    assert(getLocation(gv, PLAYER_LORD_GODALMING) == LIVERPOOL);
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(getHealth(gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeGameView (gv);

    return 1;
}


/*
* A vampire encounter
*/
static int bbTest7(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DC?.V.. GLV.V.. ";
    PlayerMessage messages[] = {"", "", "", "", "", ""};
    GameView gv = newGameView(trail, messages);
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS);
    assert(getHealth(gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    disposeGameView(gv);

    return 1;
}


/*
* A hunter loses all health, then gains it all back next move (after automatic tp)
*/
static int bbTest8(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);

    //checking a hunter successfully dies
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == 0);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER);
    assert(getHealth(gv, PLAYER_DR_SEWARD) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getLocation(gv, PLAYER_DRACULA) == MANCHESTER);
    assert(getLocation(gv, PLAYER_LORD_GODALMING) == ST_JOSEPH_AND_ST_MARYS);
    assert(getLocation(gv, PLAYER_DR_SEWARD) == MARSEILLES);
    assert(getLocation(gv, PLAYER_VAN_HELSING) == CASTLE_DRACULA);
    assert(getLocation(gv, PLAYER_MINA_HARKER) == AMSTERDAM);
    disposeGameView(gv);

    //lets now resurrect out Hunter
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
            "SCF.... HGA.... MCO.... DMNT.... GLV.... SMR.... "
            "HCD.... MAM.... DMN.... GMNTTD. SMR.... HCD.... MAM.... "
            "DSW.... ";
    gv = newGameView(trail2, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv) - SCORE_LOSS_HUNTER_HOSPITAL);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    assert(getLocation(gv, PLAYER_LORD_GODALMING) == ST_JOSEPH_AND_ST_MARYS);
    disposeGameView(gv);
    return 1;
}


/*
* Dracula loses health at sea
*/
static int bbTest9(void) {
    char *trail = "GLO.... SMR.... HCD.... MAM.... DAS.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);
    //Dracula loses 2 blood points if ending his turn at sea
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS -
        LIFE_LOSS_SEA);
    assert(getLocation(gv, PLAYER_DRACULA) == ADRIATIC_SEA);
    disposeGameView(gv);
    return 1;
}


/*
* Dracula regains his health
*/
static int bbTest10(void) {
    //before Dracula regains his health
    char *trail = "GMR.... SAO.... HCD.... MAO.... DGO.... GGOD... "
                    "SAO.... HCD.... MAO.... DTS.... GMR.... SAO.... "
                    "HCD.... MAO.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA);
    assert(getLocation(gv, PLAYER_DRACULA) == TYRRHENIAN_SEA);
    disposeGameView(gv);

    //Then gains health at Castle Dracula
    char *trail2 = "GMR.... SAO.... HCD.... MAO.... DGO.... GGOD... "
                    "SAO.... HCD.... MAO.... DTS.... GMR.... SAO.... "
                    "HZU.... MAO.... DCD.... GMR.... SAO.... "
                    "HZU.... MAO.... DCD.... ";
    gv = newGameView(trail2, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA + LIFE_GAIN_CASTLE_DRACULA
        + LIFE_GAIN_CASTLE_DRACULA);
    assert(getHealth(gv, PLAYER_DRACULA) >= GAME_START_BLOOD_POINTS);
    assert(getLocation(gv, PLAYER_DRACULA) == CASTLE_DRACULA);
    disposeGameView(gv);

    //check dracula encounters are only read from string
    //not detected (even if string is invalid)
    char *trail3 =   "GMR.... SAO.... HCD.... MAO.... DGO.... GGOD... "
                    "SAO.... HCD.... MAO.... DTS.... GMR.... SAO.... "
                    "HCD.... MAO.... DCD.... GMR.... SAO.... "
                    "HCD.... MAO.... DCD.... ";
    gv = newGameView(trail3, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    assert(getHealth(gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS
        - LIFE_LOSS_HUNTER_ENCOUNTER - LIFE_LOSS_SEA + LIFE_GAIN_CASTLE_DRACULA
        + LIFE_GAIN_CASTLE_DRACULA);
    assert(getHealth(gv, PLAYER_DRACULA) >= GAME_START_BLOOD_POINTS);
    assert(getLocation(gv, PLAYER_DRACULA) == CASTLE_DRACULA);
    disposeGameView(gv);

    return 1;
}


/*
* A hunter gains his health
*/
static int bbTest11(void) {
    //Godwalding has encounter with a trap
    char *trail = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeGameView(gv);

    //Godwalding has encounter with another trap
    char *trail2 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... ";
    gv = newGameView(trail2, messages);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER);
    disposeGameView(gv);

    //godwalding has a rest day
    char *trail3 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... ";
    gv = newGameView(trail3, messages);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS
        - LIFE_LOSS_TRAP_ENCOUNTER - LIFE_LOSS_TRAP_ENCOUNTER + LIFE_GAIN_REST);
    disposeGameView(gv);

    //godwalding has another rest but he cannot exceed max hunter health
    char *trail4 = "GLO.... SMR.... HCD.... MAM.... DSWT... GSWT.... "
                    "SMR.... HCD.... MAM.... DMNT... GMNT.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... "
                    "SCF.... HGA.... MCO.... DSWT.... GMN.... ";
    gv = newGameView(trail4, messages);
    assert(getHealth(gv, PLAYER_LORD_GODALMING) == GAME_START_HUNTER_LIFE_POINTS);
    disposeGameView(gv);

    return 1;
}


/*
* Testing Players History
*/
static int bbTest12(void) {
    char *trail = "GST.... SAO.... HCD.... MAO.... DGE.... GGED... ";
    PlayerMessage messages[] = {"Hello", "Rubbish",  "Stuff", "", "Mwahahah"};
    GameView gv = newGameView(trail, messages);

    LocationID history[TRAIL_SIZE];
    getHistory (gv, PLAYER_DRACULA, history);
    assert (history[0] == GENEVA);
    assert (history[1] == UNKNOWN_LOCATION);

    getHistory (gv, PLAYER_LORD_GODALMING, history);
    assert (history[0] == GENEVA);
    assert (history[1] == STRASBOURG);
    assert (history[2] == UNKNOWN_LOCATION);

    getHistory (gv, PLAYER_DR_SEWARD, history);
    assert (history[0] == ATLANTIC_OCEAN);
    assert (history[1] == UNKNOWN_LOCATION);
    disposeGameView(gv);

    return 1;
}


/*
* Test Dracula Double back at sea (from hunter and dracula views)
*/
static int bbTest13(void) {
    //hunter view
    char *trail =
        "GGE.... SGE.... HGE.... MGE.... DS?.... "
        "GST.... SST.... HST.... MST.... DD1....";
    PlayerMessage messages[] = {
        "Hello", "Rubbish", "Stuff", "", "Mwahahah",
        "Aha!", "", "", "", "Back I go"};
    GameView gv = newGameView (trail, messages);
    assert (getCurrentPlayer (gv) == PLAYER_LORD_GODALMING);
    assert (getHealth (gv, PLAYER_DRACULA) == GAME_START_BLOOD_POINTS -
        2 * LIFE_LOSS_SEA);
    assert (getLocation (gv, PLAYER_DRACULA) == DOUBLE_BACK_1);

    LocationID history[TRAIL_SIZE];
    getHistory (gv, PLAYER_DRACULA, history);
    assert (history[0] == DOUBLE_BACK_1);
    assert (history[1] == SEA_UNKNOWN);
    disposeGameView (gv);

    //dracula view
    char *trail2 =
       "GGE.... SGE.... HGE.... MGE.... DEC.... "
       "GST.... SST.... HST.... MST.... DD1....";
    PlayerMessage messages2[] = {
       "Hello", "Rubbish", "Stuff", "", "Mwahahah",
       "Aha!", "", "", "", "Back I go"};
    gv = newGameView (trail2, messages2);

    assert (getCurrentPlayer (gv) == PLAYER_LORD_GODALMING);
    assert (getHealth (gv, PLAYER_DRACULA) ==
        GAME_START_BLOOD_POINTS - 2 * LIFE_LOSS_SEA);
    assert (getLocation (gv, PLAYER_DRACULA) == DOUBLE_BACK_1);

    getHistory (gv, PLAYER_DRACULA, history);
    assert (history[0] == DOUBLE_BACK_1);
    assert (history[1] == ENGLISH_CHANNEL);
    disposeGameView (gv);

    return 1;
}


/*
* A vampire matures
*/
static int bbTest14(void) {
    char *trail = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));

    disposeGameView(gv);

    //now the vampire matures...
    char *trail2 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED..V. ";
    gv = newGameView(trail2, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv)
        - SCORE_LOSS_VAMPIRE_MATURES);

    disposeGameView(gv);

    //check vampire maturing is only read from string
    //and not hardcoded in (even if invalid play)
    char *trail3 = "GGE.... SGE.... HGE.... MGE.... DED.V.. "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... "
                    "GGE.... SGE.... HGE.... MGE.... DED.... ";
    gv = newGameView(trail3, messages);
    assert(getScore(gv) == GAME_START_SCORE - getRound(gv));
    disposeGameView(gv);

    return 1;
}


/*
* Basic Road Connections
*/
static int bbTest15(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Galatz road connections
    int size, *edges = connectedLocations (gv, &size, GALATZ,
        PLAYER_LORD_GODALMING, getRound(gv), true, false, false);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++) {
       seen[edges[i]] = true;
    }

    assert(size == 5);
    assert(seen[GALATZ]);
    assert(seen[CONSTANTA]);
    assert(seen[BUCHAREST]);
    assert(seen[KLAUSENBURG]);
    assert(seen[CASTLE_DRACULA]);

    free(edges);
    disposeGameView(gv);
    return 1;
}


/*
* Basic Boat Connections
*/
static int bbTest16(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Ionian Sea sea connections
    int size, *edges = connectedLocations (gv, &size, IONIAN_SEA,
        PLAYER_LORD_GODALMING, getRound(gv), false, false, true);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for (int i = 0; i < size; i++) {
       seen[edges[i]] = true;
    }

    assert(size == 7);
    assert(seen[IONIAN_SEA]);
    assert(seen[BLACK_SEA]);
    assert(seen[ADRIATIC_SEA]);
    assert(seen[TYRRHENIAN_SEA]);
    assert(seen[ATHENS]);
    assert(seen[VALONA]);
    assert(seen[SALONICA]);
    free(edges);
    disposeGameView(gv);
    return 1;
}


/*
* Basic Rail connections (0 hops allowed)
*/
static int bbTest17(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Le Havre rail connections (should be none except itself)
    int size, *edges = connectedLocations (gv, &size, LE_HAVRE,
        PLAYER_LORD_GODALMING, getRound(gv), false, true, false);

    assert(size == 1);
    assert(edges[0] == LE_HAVRE);
    free(edges);

    //dracula doesn't move by rail (should be none except itself)
    edges = connectedLocations(gv, &size, MADRID, PLAYER_DRACULA,
        getRound(gv), false, true, false);

    assert(size == 1);
    assert(edges[0] == MADRID);
    free(edges);
    disposeGameView(gv);

    return 1;
}


/*
* Basic Rail connections (1 hop allowed)
*/
static int bbTest18(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Sofia rail connections
    int size, *edges = connectedLocations (gv, &size,SOFIA,
        PLAYER_DR_SEWARD, getRound(gv), false, true, false);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(size == 4);
    assert(seen[SOFIA]);
    assert(seen[BELGRADE]);
    assert(seen[VARNA]);
    assert(seen[SALONICA]);
    free(edges);
    disposeGameView(gv);

    return 1;
}


/*
* Basic Rail connections (2 hops allowed)
*/
static int bbTest19(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Milan rail connections
    int size, *edges = connectedLocations (gv, &size, MILAN,
        PLAYER_VAN_HELSING, getRound(gv), false, true, false);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(size == 7);
    assert(seen[MILAN]);
    assert(seen[GENOA]);
    assert(seen[GENEVA]);
    assert(seen[ZURICH]);
    assert(seen[FLORENCE]);
    assert(seen[STRASBOURG]);
    assert(seen[ROME]);
    free(edges);
    disposeGameView(gv);

    return 1;
}


/*
* Basic Rail connections (3 hops allowed)
*/
static int bbTest20(void) {
    char *trail = "";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView (trail, messages);

    //check Berlin rail connections for up to 3
    int size, *edges = connectedLocations(gv, &size,
         BERLIN, PLAYER_MINA_HARKER, getRound(gv), false, true, false);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(size == 12);
    assert(seen[BERLIN]);
    assert(seen[HAMBURG]);
    assert(seen[LEIPZIG]);
    assert(seen[PRAGUE]);
    assert(seen[FRANKFURT]);
    assert(seen[NUREMBURG]);
    assert(seen[VIENNA]);
    assert(seen[COLOGNE]);
    assert(seen[STRASBOURG]);
    assert(seen[MUNICH]);
    assert(seen[VENICE]);
    assert(seen[BUDAPEST]);
    free(edges);
    disposeGameView (gv);

    return 1;
}


/*
* Test all three types of connections
*/
static int bbTest21(void) {
    char *trail = "GLO.... SMR.... HBA.... MAS.... DSW.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);

    //all connections from Barcelona with up to 3 rail hops
    int size, *edges = connectedLocations(gv, &size,
        BARCELONA, PLAYER_VAN_HELSING, getRound(gv), true, true, true);

    bool seen[NUM_MAP_LOCATIONS] = {false};
    for(int i = 0; i < size; i++) {
        seen[edges[i]] = true;
    }

    assert(size == 10);
    assert(seen[BARCELONA]);
    assert(seen[ALICANTE]);
    assert(seen[SARAGOSSA]);
    assert(seen[TOULOUSE]);
    assert(seen[MEDITERRANEAN_SEA]);
    assert(seen[BORDEAUX]);
    assert(seen[SANTANDER]);
    assert(seen[MADRID]);
    assert(seen[LISBON]);
    assert(seen[PARIS]);
    free(edges);
    disposeGameView(gv);

    return 1;
}


/*
* Test Dracula Teleport
*/
static int bbTest22(void) {
    char *trail = "GMN.... SGE.... HMU.... MBU.... DBD.... GLO.... "
            "SMR.... HZU.... MAM.... DTPT... GSWT.... SCF.... "
            "HST.... MCO.... ";
    PlayerMessage messages[] = {""};
    GameView gv = newGameView(trail, messages);

    //Dracula teleports to Castle DRACULA,

    assert( getLocation(gv, PLAYER_DRACULA) == TELEPORT);


    disposeGameView(gv);

    return 1;
}
