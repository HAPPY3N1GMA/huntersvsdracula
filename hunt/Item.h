//an Item ADT, storing a value and a layer
// Written by John Shepherd, March 2013

#ifndef ITEM_H
#define ITEM_H

typedef struct item *Item;

Item newItem(int, int);
int getLayer(Item item);
int getValue(Item item);
void updateLayer(Item item, int layer);

#endif
